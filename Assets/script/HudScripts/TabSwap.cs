﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TabSwap : MonoBehaviour {
	
	public GameObject A;
	public GameObject B;
	
	private bool On = true;

	void Start(){
	}


	public void OnClick () {
		if (On == true) {
			On = false;
		} else {
			On = true;
		}


		if (On == true) { 
			A.SetActive(true);
			B.SetActive(false);
		} else {
			A.SetActive(false);
			B.SetActive(true);    
		}
	}

}