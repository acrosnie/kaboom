﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TabSelect2 : MonoBehaviour
{
	public Transform panelAtt;
	public Transform panelDef;
	public Transform panelBuild;
	public Transform buttonAtt;
	public Transform buttonDef;
	public Transform buttonBuild;
	public Sprite buttonSpriteSelect;
	public Sprite buttonSpriteUnselect;




	public void buttonAttEvent()
	{
		panelAtt.gameObject.SetActive(true);
		panelDef.gameObject.SetActive(false);
		panelBuild.gameObject.SetActive(false);
		buttonAtt.GetComponent<Image>().sprite = buttonSpriteSelect;
		buttonDef.GetComponent<Image>().sprite = buttonSpriteUnselect;
		buttonBuild.GetComponent<Image>().sprite = buttonSpriteUnselect;
	}
	
	public void buttonDefEvent()
	{
		panelAtt.gameObject.SetActive(false);
		panelDef.gameObject.SetActive(true);
		panelBuild.gameObject.SetActive(false);
		buttonAtt.GetComponent<Image>().sprite = buttonSpriteUnselect;
		buttonDef.GetComponent<Image>().sprite = buttonSpriteSelect;
		buttonBuild.GetComponent<Image>().sprite = buttonSpriteUnselect;
	}
	
	public void buttonBuildEvent()
	{
		panelAtt.gameObject.SetActive(false);
		panelDef.gameObject.SetActive(false);
		panelBuild.gameObject.SetActive(true);
		buttonAtt.GetComponent<Image>().sprite = buttonSpriteUnselect;
		buttonDef.GetComponent<Image>().sprite = buttonSpriteUnselect;
		buttonBuild.GetComponent<Image>().sprite = buttonSpriteSelect;
	}
}