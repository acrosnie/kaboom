using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FillBuildingPanelLevel : MonoBehaviour
{
	private int nn;
	private int nr;
	public Texture[] iconeRessource;

	void displayUpgradeOrNot(Transform buildingInfo, Transform b, Transform actualBuilding)
	{
		if (b == null)
		{
			foreach (Transform child in buildingInfo)
			{
				if (child.name != "upgradeTo" && child.name != "buildingNameAndLevel" && child.name != "buildingImage")
					child.gameObject.SetActive(false);
			}
			//actualBuilding.GetComponent<BuildingPlacementProd>().canvasBuilding.Find("levelUpIcon").gameObject.SetActive(false);
			return ;
		}
		else
		{
			foreach (Transform child in buildingInfo)
			{
				if (child.name != "line1" && child.name != "priceRessource")
					child.gameObject.SetActive(true);
			}
			//actualBuilding.GetComponent<BuildingPlacementProd>().canvasBuilding.Find("levelUpIcon").gameObject.SetActive(true);
		}
	}

	void colorRedOrWhiteRessource(Text t, int costRessource, int userRessource)
	{
		t.color = Color.white;
		if (costRessource > userRessource)
			t.color = Color.red;
	}

	void fillLine(Transform panel, string text, float values, float nextValue)
	{
		var l = Instantiate(panel.Find("line1"), panel.Find("line1").position, panel.Find("line1").rotation) as Transform;
		l.gameObject.name = "lineClone";
		l.parent = panel;
		l.localScale = panel.Find("line1").localScale;
		l.position = panel.Find("line1").position;
		l.localPosition -= new Vector3(0, nn * 30, 0);
		l.Find("text").GetComponent<Text>().text = text;
		l.Find("value").GetComponent<Text>().text = "" + values;
		if (nextValue == 0)
			l.Find("nextValue").GetComponent<Text>().text = "+0";
		else
			l.Find("nextValue").GetComponent<Text>().text = "+" + (nextValue - values);
		l.gameObject.SetActive(true);
		nn++;
	}

	Texture getIcone(string str)
	{
		if (str == "r1")
			return (iconeRessource[0]);
		else if (str == "r2")
			return (iconeRessource[1]);
		else if (str == "r3")
			return (iconeRessource[2]);
		else
			return (iconeRessource[3]);
		return (null);
	}

	void fillPriceRessource(Transform panel, float values, float userRessource, string str)
	{
		var l = Instantiate(panel.Find("priceRessource"), panel.Find("priceRessource").position, panel.Find("priceRessource").rotation) as Transform;
		l.gameObject.name = "priceRessourceClone";
		l.parent = panel;
		l.localScale = panel.Find("priceRessource").localScale;
		l.position = panel.Find("priceRessource").position;
		l.localPosition += new Vector3(0, nr * 80, 0);
		l.Find("value").GetComponent<Text>().text = "" + values;
		l.Find("img").GetComponent<RawImage>().texture = getIcone(str);
		l.gameObject.SetActive(true);
		colorRedOrWhiteRessource(l.Find("value").GetComponent<Text>(), (int)values, (int)userRessource);
		nn++;
		nr++;
	}

	void fillItemTownHall(Transform item)
	{
		var ni = 0;
		var prod = GameObject.Find("script").GetComponent<GenerateProd>();
		var townHallLevel = prod.levelTownHall;
		var r = prod.mainScript.GetComponent<GenerateResource>().prefabBuildingListProd;
		foreach (var val in r)
		{
			if (val != null && val.GetComponent<BuildingInfo>().level == 1)
			{
				var level = prod.getNextLevelTownHallForBuyBuilding(val.GetComponent<BuildingInfo>().name);
				if (level == townHallLevel + 1)
				{
					item.gameObject.SetActive(true);
					var l = Instantiate(item.Find("NewItem"), item.Find("NewItem").position, item.Find("NewItem").rotation) as Transform;
					l.parent = item;
					l.localScale = item.Find("NewItem").localScale;
					l.position = item.Find("NewItem").position;
					l.localPosition += new Vector3(ni * 100, 0, 0);

					foreach (var vv in r)
					{
						if (vv.GetComponent<BuildingInfo>().level == townHallLevel + 1 && vv.GetComponent<BuildingInfo>().name == val.GetComponent<BuildingInfo>().name)
						{
							l.Find("img").GetComponent<RawImage>().texture = val.GetComponent<BuildingInfo>().texture;
							break ;
						}
					}
					ni++;
				}
			}
		}
	}

	public void fillUpgradeGeneral(BuildingInfo info, Transform[] building, Transform actualBuilding)
	{
		nn = 0;
		nr = 0;
		Transform b = null;

		for (int i = 0; i < building.Length; i++)
		{
			if (info && building[i] != null && building[i].GetComponent<BuildingInfo>().name == info.name && building[i].GetComponent<BuildingInfo>().level == info.level + 1)
			{
				b = building[i];
				break ;
			}
		}
		BuildingInfo infoNext = (b == null) ? null : b.GetComponent<BuildingInfo>();
		BuildingInfo infoActual = actualBuilding.GetComponent<BuildingInfo>();
		var buildingPanel = GameObject.Find("Canvas").transform.Find("BuildingUpgradePanel").transform.Find("Panel").transform;
		foreach (Transform child in buildingPanel)
		{
			if (child.name == "lineClone" || child.name == "priceRessourceClone")
				Destroy(child.gameObject);
		}
		buildingPanel.Find("buildingNameAndLevel").GetComponent<Text>().text = info.name + " - " + info.level;
		buildingPanel.Find("upgradeTo").GetComponent<Text>().text = ((b == null) ? "Can't level up !" : ("Upgrade to lvl " + (info.level + 1)));
		buildingPanel.Find("buildingImage").GetComponent<RawImage>().texture = info.texture; //texture
		//------------------------ info placement
		fillLine(buildingPanel, "Health :", (float)infoActual.health, (infoNext == null) ? 0 : (float)infoNext.health); // Health
		if (infoActual.healthShield > 0)
			fillLine(buildingPanel, "Health Shield:", (float)infoActual.healthShield, (infoNext == null) ? 0 : (float)infoNext.healthShield); // healthShield
		if (infoActual.capacityRessource1 > 0)
			fillLine(buildingPanel, "Capacity Ressource 1 :", (float)infoActual.capacityRessource1, (infoNext == null) ? 0 : (float)infoNext.capacityRessource1); // capacityRessource1
		if (infoActual.capacityRessource2 > 0)
			fillLine(buildingPanel, "Capacity Ressource 2 :", (float)infoActual.capacityRessource2, (infoNext == null) ? 0 : (float)infoNext.capacityRessource2); // capacityRessource2
		if (infoActual.capacityRessource3 > 0)
			fillLine(buildingPanel, "Capacity Ressource 3 :", (float)infoActual.capacityRessource3, (infoNext == null) ? 0 : (float)infoNext.capacityRessource3); // capacityRessource3
		if (infoActual.stockRessource1 > 0)
			fillLine(buildingPanel, "Stockage Ressource 1 :", (float)infoActual.stockRessource1, (infoNext == null) ? 0 : (float)infoNext.stockRessource1); // stockRessource1
		if (infoActual.stockRessource2 > 0)
			fillLine(buildingPanel, "Stockage Ressource 2 :", (float)infoActual.stockRessource2, (infoNext == null) ? 0 : (float)infoNext.stockRessource2); // stockRessource2
		if (infoActual.stockRessource3 > 0)
			fillLine(buildingPanel, "Stockage Ressource 3 :", (float)infoActual.stockRessource3, (infoNext == null) ? 0 : (float)infoNext.stockRessource3); // stockRessource3
		if (infoActual.ratioRessource1 > 0)
			fillLine(buildingPanel, "Ratio Ressource 1 :", (float)infoActual.ratioRessource1 * 3600, (infoNext == null) ? 0 : (float)infoNext.ratioRessource1 * 3600); // ratioRessource1
		if (infoActual.ratioRessource2 > 0)
			fillLine(buildingPanel, "Ratio Ressource 2 :", (float)infoActual.ratioRessource2 * 3600, (infoNext == null) ? 0 : (float)infoNext.ratioRessource2 * 3600); // ratioRessource2
		if (infoActual.ratioRessource3 > 0)
			fillLine(buildingPanel, "Ratio Ressource 3 :", (float)infoActual.ratioRessource3 * 3600, (infoNext == null) ? 0 : (float)infoNext.ratioRessource3 * 3600); // ratioRessource3
		//----------xp placement
		nn++;
		buildingPanel.Find("xp").Find("text").GetComponent<Text>().text = "Experience :"; //xp
		buildingPanel.Find("xp").Find("nextValue").GetComponent<Text>().text = "+" + info.xp; //xp
		buildingPanel.Find("xp").localScale = buildingPanel.Find("line1").localScale;
		buildingPanel.Find("xp").position = buildingPanel.Find("line1").position;
		buildingPanel.Find("xp").localPosition -= new Vector3(0, nn * 30, 0);
		buildingPanel.Find("xp").gameObject.SetActive(true);
		nn -= 1;
		buildingPanel.Find("item").gameObject.SetActive(false);
		if (info.name == "Town Hall")
			fillItemTownHall(buildingPanel.Find("item"));
		//---------------- button upgrade
		if (info.levelRessource1 > 0)
			fillPriceRessource(buildingPanel, info.levelRessource1, GameObject.Find("script").GetComponent<InfoUser>().GetComponent<InfoUser>().nbrTotalR1, "r1");
		if (info.levelRessource2 > 0)
			fillPriceRessource(buildingPanel, info.levelRessource2, GameObject.Find("script").GetComponent<InfoUser>().GetComponent<InfoUser>().nbrTotalR2, "r2");
		if (info.levelRessource3 > 0)
			fillPriceRessource(buildingPanel, info.levelRessource3, GameObject.Find("script").GetComponent<InfoUser>().GetComponent<InfoUser>().nbrTotalR3, "r3");
		if (info.levelRessource4 > 0)
			fillPriceRessource(buildingPanel, info.levelRessource4, GameObject.Find("script").GetComponent<InfoUser>().GetComponent<InfoUser>().nbrTotalR4, "r4");
		//buildingPanel.Find("Upgrade").position = new Vector3(buildingPanel.Find("Upgrade").position.x, buildingPanel.Find("line1").position.y, 0) - new Vector3(0, ((nn - nr) * 19), 0);
		buildingPanel.Find("Upgrade").Find("Text").GetComponent<Text>().text = "Upgrade : " + (info.constructionTime / 3600).ToString("0.0") + "h";
		buildingPanel.Find("Upgrade").GetComponent<Button>().onClick.RemoveAllListeners();
		buildingPanel.Find("Upgrade").GetComponent<Button>().onClick.AddListener(delegate{ actualBuilding.GetComponent<BuildingAction>().levelUp(); });
		//------------- button Instant
		nn += 2;
		//buildingPanel.Find("priceRessourceInstant").position = new Vector3(buildingPanel.Find("priceRessourceInstant").position.x, buildingPanel.Find("line1").position.y, 0) - new Vector3(0, nn * 19, 0);
		buildingPanel.Find("priceRessourceInstant").Find("value").GetComponent<Text>().text = "" + info.totalInstant;
		colorRedOrWhiteRessource(buildingPanel.Find("priceRessourceInstant").Find("value").GetComponent<Text>(), (int)info.totalInstant, (int)GameObject.Find("script").GetComponent<InfoUser>().GetComponent<InfoUser>().nbrTotalR4);
		buildingPanel.Find("priceRessourceInstant").gameObject.SetActive(true);
		buildingPanel.Find("priceRessourceInstant").Find("img").GetComponent<RawImage>().texture = getIcone("r4");
		var cost = info.totalInstant;
		buildingPanel.Find("Instant").GetComponent<Button>().onClick.RemoveAllListeners();
		buildingPanel.Find("Instant").GetComponent<Button>().onClick.AddListener(delegate{ GameObject.Find("GameController").GetComponent<FinishUpgradeWithGlobes>().finishDisplay(true); });
		displayUpgradeOrNot(buildingPanel, b, actualBuilding);
	}

	public void fillInfoGeneral(BuildingInfo info, Transform[] building, Transform actualBuilding)
	{
		var buildingInfo = GameObject.Find("Canvas").transform.Find("BuildingInfoRayManta").transform.Find("Panel").transform;
		buildingInfo.Find("buildingNameAndLevel").GetComponent<Text>().text = info.name + " - " + info.level; //name
		buildingInfo.Find("buildingDescription").GetComponent<Text>().text = info.description; //description
		buildingInfo.Find("buildingImage").GetComponent<RawImage>().texture = info.texture; //texture
	}
}