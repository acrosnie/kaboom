﻿using UnityEngine;
using System.Collections;

public class ClickOnBuildingOnOff : MonoBehaviour
{	
	public GameObject A;
	public GameObject B;
	public GameObject C;
	private GameObject tempGameObject;


	public void active(int a)
	{
		A.SetActive (false);
		B.SetActive (false);
		C.SetActive (false);
		if (a == 1)
		{
			tempGameObject = A;
			A.SetActive (true);
		}
		else if (a == 2)
		{
			tempGameObject = B;
			B.SetActive (true);
		}
		else if (a == 3)
		{
			tempGameObject = C;
			C.SetActive (true);
		}

	}

	public void desactiveTemp()
	{
		if (tempGameObject != null)
			tempGameObject.SetActive (false);
	}

	public void reactiveTemp()
	{
		if (tempGameObject != null)
			tempGameObject.SetActive (true);
	}

	public void desactive()
	{
		tempGameObject = null;
		A.SetActive (false);
		B.SetActive (false);
		C.SetActive (false);
		GameObject.Find("GameController").GetComponent<FinishUpgradeWithGlobes>().finishDisplayClose();
	}
}