﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BuildTabSelect : MonoBehaviour
{
	public Transform panelDef;
	public Transform panelCity;
	public Transform buttonDef;
	public Transform buttonCity;
	public Texture2D buttonTextureSelect;
	public Texture2D buttonTextureUnselect;
	private Vector2 startButtonDefEvent;
	private Vector2 startbuttonCityEvent;

	public void Start()
	{
		startbuttonCityEvent = (Vector2)panelCity.Find("ScrollPanel").position;
		startButtonDefEvent = (Vector2)panelDef.Find("ScrollPanel").position;
	}

	public void buttonDefEvent()
	{
		panelDef.gameObject.SetActive(true);
		panelCity.gameObject.SetActive(false);
		buttonDef.GetComponent<RawImage>().texture = buttonTextureSelect;
		buttonCity.GetComponent<RawImage>().texture = buttonTextureUnselect;
		panelCity.Find("ScrollPanel").position = startbuttonCityEvent;
		panelDef.Find("ScrollPanel").position = startButtonDefEvent;
	}

	public void buttonCityEvent()
	{
		panelDef.gameObject.SetActive(false);
		panelCity.gameObject.SetActive(true);
		buttonDef.GetComponent<RawImage>().texture = buttonTextureUnselect;
		buttonCity.GetComponent<RawImage>().texture = buttonTextureSelect;
		panelCity.Find("ScrollPanel").position = startbuttonCityEvent;
		panelDef.Find("ScrollPanel").position = startButtonDefEvent;
	}
}