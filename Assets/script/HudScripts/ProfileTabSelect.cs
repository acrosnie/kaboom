﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ProfileTabSelect : MonoBehaviour
{
	public Transform panelAchievements;
	public Transform panelStandings;
	public Transform buttonAchievements;
	public Transform buttonStandings;
	public Texture2D buttonTextureSelect;
	public Texture2D buttonTextureUnselect;


	
	public void buttonAchievementsEvent()
	{
		panelAchievements.gameObject.SetActive(true);
		panelStandings.gameObject.SetActive(false);
		buttonAchievements.GetComponent<RawImage>().texture = buttonTextureSelect;
		buttonStandings.GetComponent<RawImage>().texture = buttonTextureUnselect;
	}
	
	public void buttonStandingsEvent()
	{
		panelAchievements.gameObject.SetActive(false);
		panelStandings.gameObject.SetActive(true);
		buttonAchievements.GetComponent<RawImage>().texture = buttonTextureUnselect;
		buttonStandings.GetComponent<RawImage>().texture = buttonTextureSelect;
	}

}