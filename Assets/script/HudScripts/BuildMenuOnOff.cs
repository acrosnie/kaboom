﻿using UnityEngine;
using System.Collections;

public class BuildMenuOnOff : MonoBehaviour
{
	public GameObject buildMenu;
	public GameObject mouseCollider;
	public GameObject cameraTarget;

	public void active()
	{
			mouseCollider.GetComponent<MouseSensitive>().unselectAll();
			mouseCollider.GetComponent<MouseSensitive>().unactiveSensitive = true;
			cameraTarget.GetComponent<Move>().block = true;
			buildMenu.SetActive (true);
			GetComponent<BuildTabSelect>().buttonCityEvent();
	}

	public void desactive()
	{
		mouseCollider.GetComponent<MouseSensitive>().unactiveSensitive = false;
		cameraTarget.GetComponent<Move>().block = false;
		buildMenu.SetActive (false);
	}
}