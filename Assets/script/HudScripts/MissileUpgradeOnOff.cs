﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MissileUpgradeOnOff : MonoBehaviour
{	
	public GameObject A;
	//public GameObject B;
	private bool state; 

	void Start()
	{
		state = false;
	}

	public void active(InfoMissile infoMissileOld, InfoMissile infoMissileNew)
	{
		print ("click");
		GameObject.Find("script").GetComponent<upgradeMissile>().fillInfo(A, infoMissileOld, infoMissileNew);
		A.SetActive (true);
		state = true;
	}

	public void desactive()
	{
		A.SetActive (false);
		state = false;
	}
}