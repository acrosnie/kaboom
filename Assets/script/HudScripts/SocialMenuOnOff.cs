﻿using UnityEngine;
using System.Collections;

public class SocialMenuOnOff : MonoBehaviour {
	
	public GameObject A;
	//public GameObject B;
	private bool state; 

	void Start() {
		state = false;
	}

	public void OnClick()
	{
		if (state == false) {
			A.SetActive (true);
			state = true;
		} else {
			A.SetActive (false);
			state = false;
		}
		//B.SetActive (false);
	}


}