﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ConvertGlobes : MonoBehaviour
{	
	public GameObject A;

	public void active()
	{
		A.SetActive (true);
	}

	public void desactive()
	{
		A.SetActive (false);
	}

	public void displayMissile(InfoMissile bbI)
	{
		var infoUser = GameObject.Find("script").GetComponent<InfoUser>();
		var missR2 = bbI.ressource2Cost - infoUser.nbrTotalR2;
		missR2 = (missR2 < 0) ? 0 : missR2;
		float r2 = (bbI.intantR2Prod == 0 || bbI.ressource2Cost == 0) ? 0 : Mathf.CeilToInt(missR2 * (bbI.intantR2Prod / bbI.ressource2Cost));
		int total = (int)r2;
		if (total < 1)
			total = 1;
		A.transform.Find("Panel").Find("missingRessource1").Find("Text").GetComponent<Text>().text = "0";
		A.transform.Find("Panel").Find("missingRessource2").Find("Text").GetComponent<Text>().text = "" + missR2;
		A.transform.Find("Panel").Find("missingRessource3").Find("Text").GetComponent<Text>().text = "0";
		A.transform.Find("Panel").Find("ConversionClick").Find("GlobesNeeded").GetComponent<Text>().text = "" + total;
		var missileLevelListId = bbI.missileLevelListId;
		int rr1 = 0;
		int rr2 = (int)(bbI.ressource2Cost - missR2);
		int rr3 = 0;
		int rr4 = total;
		A.transform.Find("Panel").Find("ConversionClick").GetComponent<Button>().onClick.RemoveAllListeners();
		if (infoUser.nbrTotalR4 >= rr4)
		{
			A.transform.Find("Panel").Find("ConversionClick").GetComponent<Button>().onClick.AddListener(delegate{ GameObject.Find("script").GetComponent<MissileProdScript>().constructMissileGlobes(missileLevelListId, rr1, rr2, rr3, rr4); });
			A.transform.Find("Panel").Find("ConversionClick").GetComponent<Button>().onClick.AddListener(delegate{ desactive(); });
		}
		active();
	}

	public void displayMissileUpgrade(InfoMissile bbI, InfoMissile bbNew)
	{
		var infoUser = GameObject.Find("script").GetComponent<InfoUser>();
		var missR2 = bbI.ressource2CostUpgrade - infoUser.nbrTotalR2;
		missR2 = (missR2 < 0) ? 0 : missR2;
		float r2 = (bbI.intantR2Upgrade == 0 || bbI.ressource2CostUpgrade == 0) ? 0 : Mathf.CeilToInt(missR2 * (bbI.intantR2Upgrade / bbI.ressource2CostUpgrade));
		int total = (int)r2;
		if (total < 1)
			total = 1;
		A.transform.Find("Panel").Find("missingRessource1").Find("Text").GetComponent<Text>().text = "0";
		A.transform.Find("Panel").Find("missingRessource2").Find("Text").GetComponent<Text>().text = "" + missR2;
		A.transform.Find("Panel").Find("missingRessource3").Find("Text").GetComponent<Text>().text = "0";
		A.transform.Find("Panel").Find("ConversionClick").Find("GlobesNeeded").GetComponent<Text>().text = "" + total;
		var missileLevelListId = bbI.missileLevelListId;
		int rr1 = 0;
		int rr2 = (int)(bbI.ressource2CostUpgrade - missR2);
		int rr3 = 0;
		int rr4 = total;
		var iOld = bbI;
		var iNew = bbNew;
		A.transform.Find("Panel").Find("ConversionClick").GetComponent<Button>().onClick.RemoveAllListeners();
		if (infoUser.nbrTotalR4 >= rr4)
		{
			A.transform.Find("Panel").Find("ConversionClick").GetComponent<Button>().onClick.AddListener(delegate{ GameObject.Find("script").GetComponent<upgradeMissile>().upForced(iOld, iNew, rr1, rr2, rr3, rr4); });
			A.transform.Find("Panel").Find("ConversionClick").GetComponent<Button>().onClick.AddListener(delegate{ desactive(); });
		}
		active();
	}

	public void displayBuildingUpgrade(Transform tr)
	{
		var bbI = tr.GetComponent<BuildingInfo>();
		var infoUser = GameObject.Find("script").GetComponent<InfoUser>();
		var missR1 = bbI.levelRessource1 - infoUser.nbrTotalR1;
		var missR2 = bbI.levelRessource2 - infoUser.nbrTotalR2;
		var missR3 = bbI.levelRessource3 - infoUser.nbrTotalR3;
		missR1 = (missR1 < 0) ? 0 : missR1;
		missR2 = (missR2 < 0) ? 0 : missR2;
		missR3 = (missR3 < 0) ? 0 : missR3;
		float r1 = (bbI.instantR1 == 0 || bbI.levelRessource1 == 0) ? 0 : Mathf.CeilToInt(missR1 * (bbI.instantR1 / bbI.levelRessource1));
		float r2 = (bbI.instantR2 == 0 || bbI.levelRessource2 == 0) ? 0 : Mathf.CeilToInt(missR2 * (bbI.instantR2 / bbI.levelRessource2));
		float r3 = (bbI.instantR3 == 0 || bbI.levelRessource3 == 0) ? 0 : Mathf.CeilToInt(missR3 * (bbI.instantR3 / bbI.levelRessource3));
		int total = (int)(r1 + r2 + r3);
		if (total < 1)
			total = 1;
		A.transform.Find("Panel").Find("missingRessource1").Find("Text").GetComponent<Text>().text = "" + missR1;
		A.transform.Find("Panel").Find("missingRessource2").Find("Text").GetComponent<Text>().text = "" + missR2;
		A.transform.Find("Panel").Find("missingRessource3").Find("Text").GetComponent<Text>().text = "" + missR3;
		A.transform.Find("Panel").Find("ConversionClick").Find("GlobesNeeded").GetComponent<Text>().text = "" + total;
		var name = bbI.name;
		var level = bbI.level;
		int rr1 = (int)(bbI.levelRessource1 - missR1);
		int rr2 = (int)(bbI.levelRessource2 - missR2);
		int rr3 = (int)(bbI.levelRessource3 - missR3);
		int rr4 = total;
		A.transform.Find("Panel").Find("ConversionClick").GetComponent<Button>().onClick.RemoveAllListeners();
		if (infoUser.nbrTotalR4 >= rr4)
		{
			A.transform.Find("Panel").Find("ConversionClick").GetComponent<Button>().onClick.AddListener(delegate{ tr.GetComponent<BuildingAction>().levelUpForced(rr1, rr2, rr3, rr4); });
			A.transform.Find("Panel").Find("ConversionClick").GetComponent<Button>().onClick.AddListener(delegate{ desactive(); });
		}
		active();
	}

	public void display(BuildingInfo bbI)
	{
		var infoUser = GameObject.Find("script").GetComponent<InfoUser>();
		var missR1 = bbI.costBuyRessource1 - infoUser.nbrTotalR1;
		var missR2 = bbI.costBuyRessource2 - infoUser.nbrTotalR2;
		var missR3 = bbI.costBuyRessource3 - infoUser.nbrTotalR3;
		missR1 = (missR1 < 0) ? 0 : missR1;
		missR2 = (missR2 < 0) ? 0 : missR2;
		missR3 = (missR3 < 0) ? 0 : missR3;
		float r1 = (bbI.instant_build_cost_r1 == 0 || bbI.costBuyRessource1 == 0) ? 0 : Mathf.CeilToInt(missR1 * (bbI.instant_build_cost_r1 / bbI.costBuyRessource1));
		float r2 = (bbI.instant_build_cost_r2 == 0 || bbI.costBuyRessource2 == 0) ? 0 : Mathf.CeilToInt(missR2 * (bbI.instant_build_cost_r2 / bbI.costBuyRessource2));
		float r3 = (bbI.instant_build_cost_r3 == 0 || bbI.costBuyRessource3 == 0) ? 0 : Mathf.CeilToInt(missR3 * (bbI.instant_build_cost_r3 / bbI.costBuyRessource3));
		int total = (int)(r1 + r2 + r3);
		if (total < 1)
			total = 1;
		A.transform.Find("Panel").Find("missingRessource1").Find("Text").GetComponent<Text>().text = "" + missR1;
		A.transform.Find("Panel").Find("missingRessource2").Find("Text").GetComponent<Text>().text = "" + missR2;
		A.transform.Find("Panel").Find("missingRessource3").Find("Text").GetComponent<Text>().text = "" + missR3;
		A.transform.Find("Panel").Find("ConversionClick").Find("GlobesNeeded").GetComponent<Text>().text = "" + total;
		var name = bbI.name;
		var level = bbI.level;
		int rr1 = (int)(bbI.costBuyRessource1 - missR1);
		int rr2 = (int)(bbI.costBuyRessource2 - missR2);
		int rr3 = (int)(bbI.costBuyRessource3 - missR3);
		int rr4 = total;
		A.transform.Find("Panel").Find("ConversionClick").GetComponent<Button>().onClick.RemoveAllListeners();
		if (infoUser.nbrTotalR4 >= rr4)
		{
			A.transform.Find("Panel").Find("ConversionClick").GetComponent<Button>().onClick.AddListener(delegate{ GameObject.Find("GameController").GetComponent<ButtonSelectBuilding>().addBuildingGlobes(name, level, rr1, rr2, rr3, rr4); });
			A.transform.Find("Panel").Find("ConversionClick").GetComponent<Button>().onClick.AddListener(delegate{ desactive(); });
		}
		active();
	}
}