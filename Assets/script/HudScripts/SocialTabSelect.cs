﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SocialTabSelect : MonoBehaviour
{
	public Transform panelChat;
	public Transform panelNews;
	public Transform buttonChat;
	public Transform buttonNews;
	public Texture2D buttonSpriteSelect;
	public Texture2D buttonSpriteUnselect;



	public void buttonChatEvent()
	{
		panelChat.gameObject.SetActive(true);
		panelNews.gameObject.SetActive(false);
		buttonChat.GetComponent<RawImage>().texture = buttonSpriteSelect;
		buttonNews.GetComponent<RawImage>().texture = buttonSpriteUnselect;
	}
	
	public void buttonNewsEvent()
	{
		panelChat.gameObject.SetActive(false);
		panelNews.gameObject.SetActive(true);
		buttonChat.GetComponent<RawImage>().texture = buttonSpriteUnselect;
		buttonNews.GetComponent<RawImage>().texture = buttonSpriteSelect;
	}
}