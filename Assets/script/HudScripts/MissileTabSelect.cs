﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MissileTabSelect : MonoBehaviour
{
	public Transform panelAtt;
	public Transform panelDef;
	public Transform panelBuild;
	public Transform buttonAttSelect;
	public Transform buttonDefSelect;
	public Transform buttonBuildSelect;
	public Transform buttonAttUnselect;
	public Transform buttonDefUnselect;
	public Transform buttonBuildUnselect;
	private Vector2 startbuttonAttEvent;
	private Vector2 startbuttonDefEvent;
	private Vector2 startbuttonAttEvent2;
	private Vector2 startbuttonDefEvent2;
	private Vector2 startbuttonBuildEvent;

	public void Start()
	{
		startbuttonAttEvent = (Vector2)panelAtt.Find("AttackLineScroll").Find("Selection").position;
		startbuttonDefEvent = (Vector2)panelDef.Find("DefenceLineScroll").Find("Selection").position;
		startbuttonAttEvent2 = (Vector2)panelAtt.Find("LineEquipedScroll").Find("Selection").position;
		startbuttonDefEvent2 = (Vector2)panelDef.Find("LineEquipedScroll").Find("Selection").position;
		startbuttonBuildEvent = (Vector2)panelBuild.Find("MissileFactoryScroll").Find("Selection").position;
	}

	public void buttonAttEvent()
	{
		panelAtt.gameObject.SetActive (true);
		panelDef.gameObject.SetActive (false);
		panelBuild.gameObject.SetActive (false);
		buttonAttSelect.gameObject.SetActive(true);
		buttonAttUnselect.gameObject.SetActive(false);
		buttonDefSelect.gameObject.SetActive(false);
		buttonDefUnselect.gameObject.SetActive(true);
		buttonBuildSelect.gameObject.SetActive(false);
		buttonBuildUnselect.gameObject.SetActive(true);
		panelAtt.Find("AttackLineScroll").Find("Selection").position = startbuttonAttEvent;
		panelDef.Find("DefenceLineScroll").Find("Selection").position = startbuttonDefEvent;
		panelAtt.Find("LineEquipedScroll").Find("Selection").position = startbuttonAttEvent2;
		panelDef.Find("LineEquipedScroll").Find("Selection").position = startbuttonDefEvent2;
		panelBuild.Find("MissileFactoryScroll").Find("Selection").position = startbuttonBuildEvent;
	}
	
	public void buttonDefEvent()
	{
		panelAtt.gameObject.SetActive(false);
		panelDef.gameObject.SetActive(true);
		panelBuild.gameObject.SetActive(false);
		buttonAttSelect.gameObject.SetActive(false);
		buttonAttUnselect.gameObject.SetActive(true);
		buttonDefSelect.gameObject.SetActive(true);
		buttonDefUnselect.gameObject.SetActive(false);
		buttonBuildSelect.gameObject.SetActive(false);
		buttonBuildUnselect.gameObject.SetActive(true);
		panelAtt.Find("AttackLineScroll").Find("Selection").position = startbuttonAttEvent;
		panelDef.Find("DefenceLineScroll").Find("Selection").position = startbuttonDefEvent;
		panelAtt.Find("LineEquipedScroll").Find("Selection").position = startbuttonAttEvent2;
		panelDef.Find("LineEquipedScroll").Find("Selection").position = startbuttonDefEvent2;
		panelBuild.Find("MissileFactoryScroll").Find("Selection").position = startbuttonBuildEvent;
	}

	public void buttonBuildEvent()
	{
		panelAtt.gameObject.SetActive(false);
		panelDef.gameObject.SetActive(false);
		panelBuild.gameObject.SetActive(true);
		buttonAttSelect.gameObject.SetActive(false);
		buttonAttUnselect.gameObject.SetActive(true);
		buttonDefSelect.gameObject.SetActive(false);
		buttonDefUnselect.gameObject.SetActive(true);
		buttonBuildSelect.gameObject.SetActive(true);
		buttonBuildUnselect.gameObject.SetActive(false);
		panelAtt.Find("AttackLineScroll").Find("Selection").position = startbuttonAttEvent;
		panelDef.Find("DefenceLineScroll").Find("Selection").position = startbuttonDefEvent;
		panelAtt.Find("LineEquipedScroll").Find("Selection").position = startbuttonAttEvent2;
		panelDef.Find("LineEquipedScroll").Find("Selection").position = startbuttonDefEvent2;
		panelBuild.Find("MissileFactoryScroll").Find("Selection").position = startbuttonBuildEvent;
		GameObject.Find("script").GetComponent<MissileProdScript>().majCostTotalInstantBuildMissile();
	}
}
