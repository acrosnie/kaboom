﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScrollRectOpti : MonoBehaviour {

	public Transform panelParent; //le panel qui contient l'inertie a activer et  desactiver
	public RectTransform panel;
	public Button[] bttn;
	public RectTransform limitLeft;
	public RectTransform limitRight;
	
	private float[] distanceLeft; //distance du bouton à la limite gauche
	private float[] distanceRight;
	private bool dragging = false;
	private int bttnDistance; // distance entre les boutons
	private int minButtonNumLeft; // distance la plus courte par rapport à la limite gauche
	private int minButtonNumRight;
	
	void Start () {
		int bttnLength = bttn.Length;
		distanceLeft = new float[bttnLength];
		distanceRight = new float[bttnLength];
		
		bttnDistance = (int)Mathf.Abs (bttn [1].GetComponent<RectTransform> ().anchoredPosition.x - bttn [0].GetComponent<RectTransform> ().anchoredPosition.x);
		
	}
	
	void Update () {
		for (int i=0; i < bttn.Length; i++) {
			distanceLeft [i] = Mathf.Abs (limitLeft.transform.position.x - bttn [i].transform.position.x);
			distanceRight [i] = Mathf.Abs (limitRight.transform.position.x - bttn [i].transform.position.x);
		}
		
		float minDistanceLeft = Mathf.Min (distanceLeft);
		float minDistanceRight = Mathf.Min (distanceRight);
		
		for (int a = 0; a < bttn.Length; a++) {
			if (minDistanceLeft == distanceLeft [a]) {
				minButtonNumLeft = a;
			}
			if (minDistanceRight == distanceRight [a]) {
				minButtonNumRight = a;
			}
		}

		if (!dragging 
			    && minButtonNumLeft == 0 
			    && (limitLeft.transform.position.x - bttn [0].transform.position.x) < 0) // Calcul limite gauche
		{
			//panelParent.GetComponent<ScrollRect>() = false;
			Debug.Log (true);

			//var posFinal = new Vector2(limitLeft.anchoredPosition.x, limitLeft.anchoredPosition.y);
			//var panelPosition = new Vector2(panel.anchoredPosition.x, panel.anchoredPosition.y);
			//panel.anchoredPosition = Vector2.Lerp(panelPosition, posFinal, Time.deltaTime * 1f);
			float newX = Mathf.Lerp(panel.anchoredPosition.x, limitLeft.anchoredPosition.x, Time.deltaTime * 2f);
			Vector2 newPosition = new Vector2 (newX,panel.anchoredPosition.y);
			panel.anchoredPosition = newPosition;

			//panelParent.GetComponent<ScrollRect>() = true;
		}

		
		if (!dragging 
			    && minButtonNumRight == bttn.Length - 1 
			    && (limitRight.transform.position.x - bttn [bttn.Length - 1].transform.position.x) > 0) // Calcul limite droite
		{
			//panelParent.GetComponent<ScrollRect>() = false;
			Debug.Log (true);

			//var posFinal = new Vector2(limitLeft.anchoredPosition.x, limitLeft.anchoredPosition.y);
			//var panelPosition = new Vector2(panel.anchoredPosition.x, panel.anchoredPosition.y);
			//panel.anchoredPosition = Vector2.Lerp(panelPosition, posFinal, Time.deltaTime * 1f);
			float newX = Mathf.Lerp(panel.anchoredPosition.x, limitLeft.anchoredPosition.x, Time.deltaTime * 2f);
			Vector2 newPosition = new Vector2 (newX,panel.anchoredPosition.y);
			panel.anchoredPosition = newPosition;

			//panelParent.GetComponent<ScrollRect>() = true;
		}
	
		
	}
	
	void LerpToBttn(int position){
		float newX = Mathf.Lerp (panel.anchoredPosition.x, position, Time.deltaTime *10f);
		Vector2 newPosition = new Vector2 (newX, panel.anchoredPosition.y);
		
		panel.anchoredPosition = newPosition;
	}
	
	public void StartDrag(){
		dragging = true;
	}
	
	public void EndDrag(){
		dragging = false;
	}
}
