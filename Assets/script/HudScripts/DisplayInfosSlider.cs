﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DisplayInfosSlider : MonoBehaviour
{	
	public GameObject A;
	public GameObject B;
	public GameObject C;

	void Start()
	{

	}

	public void active(int i)
	{
		desactive();
		if (i == 1)
		{
			A.SetActive (true);
			A.transform.Find("MaxCapacityNumber").GetComponent<Text>().text = "" + GameObject.Find("script").GetComponent<InfoUser>().nbrMaxR1;
			A.transform.Find("ProdRateNumber").GetComponent<Text>().text = "" + GameObject.Find("script").GetComponent<InfoUser>().prodRateNumberRessource1;
		}
		else if (i == 2)
		{
			B.SetActive (true);
			B.transform.Find("MaxCapacityNumber").GetComponent<Text>().text = "" + GameObject.Find("script").GetComponent<InfoUser>().nbrMaxR2;
			B.transform.Find("ProdRateNumber").GetComponent<Text>().text = "" + GameObject.Find("script").GetComponent<InfoUser>().prodRateNumberRessource2;
		}
		else if (i == 3)
		{
			C.SetActive (true);
			C.transform.Find("MaxCapacityNumber").GetComponent<Text>().text = "" + GameObject.Find("script").GetComponent<InfoUser>().nbrMaxR3;
			C.transform.Find("ProdRateNumber").GetComponent<Text>().text = "" + GameObject.Find("script").GetComponent<InfoUser>().prodRateNumberRessource3;
		}

	}

	public void desactive()
	{
		A.SetActive (false);
		B.SetActive (false);
		C.SetActive (false);
	}
}