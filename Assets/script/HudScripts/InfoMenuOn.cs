﻿using UnityEngine;
using System.Collections;

public class InfoMenuOn : MonoBehaviour
{
	public GameObject[] A;
	public GameObject[] B;
	public GameObject[] C;

	void Start()
	{

	}

	public void desactive ()
	{
		foreach (var d in A)
		{
			if (d != null)
				d.SetActive(false);
		}
		foreach (var d in B)
		{
			if (d != null)
				d.SetActive(false);
		}
		GameObject.Find("mouseCollider").GetComponent<MouseSensitive>().unactiveSensitive = false;
	}

	public void activeUpgrade ()
	{
		var t = GameObject.FindGameObjectsWithTag("building");

		foreach (var t2 in t)
		{
			if (t2 != null && t2.GetComponent<BuildingPlacementProd>() && t2.GetComponent<BuildingPlacementProd>().selected == true) // if selected
			{
				if (t2.GetComponent<BuildingInfo>().name == "Town Hall")
					A[1].SetActive(true);
				else
					A[1].SetActive(true);
				GameObject.Find("mouseCollider").GetComponent<MouseSensitive>().unactiveSensitive = true;
				break ;
			}
		}
	}

	public void activeInfo ()
	{
		var t = GameObject.FindGameObjectsWithTag("building");

		foreach (var t2 in t)
		{
			if (t2 != null && t2.GetComponent<BuildingPlacementProd>() && t2.GetComponent<BuildingPlacementProd>().selected == true) // if selected
			{
				B[0].SetActive(true);
				break ;
			}
		}
	}

	public void activeEnterMenu ()
	{
		var t = GameObject.FindGameObjectsWithTag("building");

		foreach (var t2 in t)
		{
			if (t2 != null && t2.GetComponent<BuildingPlacementProd>() && t2.GetComponent<BuildingPlacementProd>().selected == true) // if selected
			{

				if (t2.GetComponent<BuildingInfo>().name == "Missile Launcher")
				{
					GetComponent<MissileMenuOnOff>().active();
					GetComponent<MissileTabSelect>().buttonDefEvent();
				}
				else if (t2.GetComponent<BuildingInfo>().name == "Attack Whale")
				{
					GetComponent<MissileMenuOnOff>().active();
					GetComponent<MissileTabSelect>().buttonAttEvent();
				}
				else if (t2.GetComponent<BuildingInfo>().name == "Missile Factory")
				{
					GetComponent<MissileMenuOnOff>().active();
					GetComponent<MissileTabSelect>().buttonBuildEvent();
				}
				else if (t2.GetComponent<BuildingInfo>().name == "Geo's Lab")
					C[1].SetActive(true);
				GameObject.Find("mouseCollider").GetComponent<MouseSensitive>().unactiveSensitive = true;
				break ;
			}
		}
	}
}