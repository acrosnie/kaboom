﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FinishUpgradeWithGlobes : MonoBehaviour
{
	public Transform finishDisplayTransform;
	public Transform building;
	public Transform constructionLine;
	public bool missileConstruct;
	public bool missile;
	public bool instant;
	public InfoMissile infoMissileOld;
	public InfoMissile infoMissileNew;
	public int ressource4;

	public void finish()
	{

		if (missileConstruct)
		{
			foreach (Transform child in constructionLine)
			{
				child.GetComponent<InConstructionLine>().timeMax = 0;
			}
			GameObject.Find("script").GetComponent<GenerateProd>().spendGlobes(ressource4);
		}
		else
		{
			if (building == null)
			{
				print ("error");
				return ;
			}
			else if (!missile)
				building.GetComponent<BuildingAction>().finishLevelUpGlobes();
			else if (!missile && instant)
				building.GetComponent<BuildingAction>().finishLevelUpGlobes(building.GetComponent<BuildingInfo>().totalInstant);
			else if (missile && instant)
				GameObject.Find("script").GetComponent<upgradeMissile>().upButtonInstant(infoMissileOld, infoMissileNew);
			else if (missile)
				building.GetComponent<BuildingAction>().finishLevelUpGlobesMissile();
		}
		finishDisplayTransform.gameObject.SetActive(false);
		instant = false;
		missile = false;
		missileConstruct = false;
	}

	public void finishDisplaySimple()
	{
		finishDisplay();
	}

	public void finishDisplayBuildMissile()
	{
		var ressourceLeft = 0;
		int i = 0;
		foreach (Transform child in constructionLine)
		{
			var m = PlayerPrefs.GetInt("missileConstructMissileLevelListId" + i);
			if (m > 0)
			{
				var missileInfo = GameObject.Find("script").GetComponent<MissileProdScript>().getMissileInfo(m);
				ressourceLeft += (int)(missileInfo.intantR2Prod + missileInfo.intantTimeProd);
			}
			i++;
		}
		missileConstruct = true;
		ressource4 = ressourceLeft;
		finishDisplayTransform.Find("Panel").Find("title").GetComponent<Text>().text = "TERMINER LA CONSTRUCTION DE MISSILES !";
		finishDisplayTransform.Find("Panel").Find("description").GetComponent<Text>().text = "Construction de missiles en cours. Les terminer en echange de " + ressourceLeft + " Ressource4 ?";
		finishDisplayTransform.Find("Panel").Find("Upgrade").Find("value").GetComponent<Text>().text = "" + ressourceLeft;
		finishDisplayTransform.Find("Panel").Find("Upgrade").gameObject.SetActive(true);
		finishDisplayTransform.gameObject.SetActive(true);
	}

	public void finishDisplay(bool ins = false, InfoMissile iOld = null, InfoMissile iNew = null)
	{
		instant = ins;
		if (building)
		{
			if (missile && !instant)
			{
				var name = building.GetComponent<BuildingInfo>().missileName;
				var ressourceLeft = building.GetComponent<BuildingInfo>().costRessource4UpgradeLeftMissile;
				finishDisplayTransform.Find("Panel").Find("title").GetComponent<Text>().text = "TERMINER L'AMELIORATION !";
				finishDisplayTransform.Find("Panel").Find("description").GetComponent<Text>().text = name + " : amelioration en cours. La terminer en echange de " + ressourceLeft + " Ressource4 ?";
				finishDisplayTransform.Find("Panel").Find("Upgrade").Find("value").GetComponent<Text>().text = "" + ressourceLeft;
				finishDisplayTransform.Find("Panel").Find("Upgrade").gameObject.SetActive(true);
			}
			else if (missile && instant)
			{
				infoMissileOld = iOld;
				infoMissileNew = iNew;
				var name = infoMissileOld.name;
				var ressourceLeft = infoMissileOld.intantR2Upgrade + infoMissileOld.intantTimeUpgrade;
				finishDisplayTransform.Find("Panel").Find("title").GetComponent<Text>().text = "AMELIORATION IMMEDIATE !";
				finishDisplayTransform.Find("Panel").Find("description").GetComponent<Text>().text = "Ameliorer instantanement " + name + " en echange de " + ressourceLeft + " Ressource4 ?";
				finishDisplayTransform.Find("Panel").Find("Upgrade").Find("value").GetComponent<Text>().text = "" + ressourceLeft;
				finishDisplayTransform.Find("Panel").Find("Upgrade").gameObject.SetActive(true);
			}
			else if (!missile && instant)
			{
				var name = building.GetComponent<BuildingInfo>().name;
				var ressourceLeft = building.GetComponent<BuildingInfo>().totalInstant;
				finishDisplayTransform.Find("Panel").Find("title").GetComponent<Text>().text = "AMELIORATION IMMEDIATE !";
				finishDisplayTransform.Find("Panel").Find("description").GetComponent<Text>().text = "Ameliorer instantanement " + name + " en echange de " + ressourceLeft + " Ressource4 ?";
				finishDisplayTransform.Find("Panel").Find("Upgrade").Find("value").GetComponent<Text>().text = "" + ressourceLeft;
				finishDisplayTransform.Find("Panel").Find("Upgrade").gameObject.SetActive(true);
			}
			else
			{
				var name = building.GetComponent<BuildingInfo>().name;
				var ressourceLeft = building.GetComponent<BuildingInfo>().costRessource4UpgradeLeft;
				finishDisplayTransform.Find("Panel").Find("title").GetComponent<Text>().text = "TERMINER L'UPGRADE !";
				finishDisplayTransform.Find("Panel").Find("description").GetComponent<Text>().text = name + " : upgrade en cours. La terminer en echange de " + ressourceLeft + " Ressource4 ?";
				finishDisplayTransform.Find("Panel").Find("Upgrade").Find("value").GetComponent<Text>().text = "" + ressourceLeft;
				finishDisplayTransform.Find("Panel").Find("Upgrade").gameObject.SetActive(true);
			}
		}
		else
		{
			finishDisplayTransform.Find("Panel").Find("title").GetComponent<Text>().text = "Error !";
			finishDisplayTransform.Find("Panel").Find("description").GetComponent<Text>().text = "";
			finishDisplayTransform.Find("Panel").Find("Upgrade").gameObject.SetActive(false);
		}
		finishDisplayTransform.gameObject.SetActive(true);
	}

	public void finishDisplayClose()
	{
		finishDisplayTransform.gameObject.SetActive(false);
		missile = false;
		missileConstruct = false;
	}

	public void cancel()
	{
		finishDisplayTransform.gameObject.SetActive(false);
		if (building == null)
		{
			print ("error");
			return ;
		}
		if (!missile)
			building.GetComponent<BuildingAction>().cancelUpgrade();
		else
			building.GetComponent<BuildingAction>().cancelUpgradeMissile();
	}
}