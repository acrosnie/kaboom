﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SettingsButtonsOnOff : MonoBehaviour {
	
	public GameObject A;
	public GameObject B; 
	public GameObject C;
	public GameObject D;
	public Texture2D textureSelected;
	public Texture2D textureUnselected;

	private bool stateA;
	private bool stateB;
	private bool stateC;
	private bool stateD;


	void Start() {
		stateA = false;
		stateB = false;
		stateC = false;
		stateD = false;
	}

	public void OnClickA()
	{
		if (stateA == false) {
			A.GetComponent<RawImage> ().texture = textureSelected;
			stateA = true;
		} else {
			A.GetComponent<RawImage> ().texture = textureUnselected;
			stateA = false;
		}

	}

	public void OnClickB()
	{
		if (stateB == false) {
			B.GetComponent<RawImage> ().texture = textureSelected;
			stateB = true;
		} else {
			B.GetComponent<RawImage> ().texture = textureUnselected;
			stateB = false;
		}

	}

	public void OnClickC()
	{
		if (stateC == false) {
			C.GetComponent<RawImage> ().texture = textureSelected;
			stateC = true;
		} else {
			C.GetComponent<RawImage> ().texture = textureUnselected;
			stateC = false;
		}

	}

	public void OnClickD()
	{
		if (stateD == false) {
			D.GetComponent<RawImage> ().texture = textureSelected;
			stateD = true;
		} else {
			D.GetComponent<RawImage> ().texture = textureUnselected;
			stateD = false;
		}

	}



}