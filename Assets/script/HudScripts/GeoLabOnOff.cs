﻿using UnityEngine;
using System.Collections;

public class GeoLabOnOff : MonoBehaviour
{	
	public GameObject A;

	public void active()
	{
		A.SetActive (true);
	}

	public void desactive()
	{
		A.SetActive (false);
	}
}