﻿using UnityEngine;
using System.Collections;

public class MissileMenuOnOff : MonoBehaviour {
	
	public GameObject A;
	//public GameObject B;
	private bool state; 
	public GameObject mouseCollider;
	public GameObject cameraTarget;

	void Start() {
		state = false;
	}

	public void active(bool forceAttack = false)
	{
		mouseCollider.GetComponent<MouseSensitive>().unselectAll();
		mouseCollider.GetComponent<MouseSensitive>().unactiveSensitive = true;
		cameraTarget.GetComponent<Move>().block = true;
		A.SetActive (true);
		if (forceAttack)
			GetComponent<MissileTabSelect>().buttonAttEvent();
	}

	public void unActive()
	{
		mouseCollider.GetComponent<MouseSensitive>().unselectAll();
		mouseCollider.GetComponent<MouseSensitive>().unactiveSensitive = false;
		cameraTarget.GetComponent<Move>().block = false;
		A.SetActive (false);
	}
	// public void OnClick()
	// {
	// 	if (state == false)
	// 	{
	// 		mouseCollider.GetComponent<MouseSensitive>().unselectAll();
	// 		mouseCollider.GetComponent<MouseSensitive>().unactiveSensitive = true;
	// 		cameraTarget.GetComponent<Move>().block = true;
	// 		A.SetActive (true);
	// 		state = true;
	// 		GetComponent<MissileTabSelect>().buttonAttEvent();
	// 	}
	// 	else
	// 	{
	// 		mouseCollider.GetComponent<MouseSensitive>().unactiveSensitive = false;
	// 		cameraTarget.GetComponent<Move>().block = false;
	// 		A.SetActive (false);
	// 		state = false;
	// 	}
	// 	//B.SetActive (false);
	// }


}