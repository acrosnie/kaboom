﻿using UnityEngine;
using System.Collections;

public class AnimSprite : MonoBehaviour
{
	public Sprite[] spriteListArray1;
	public Sprite[] spriteListArray2;
	public float[] framePerSecond;
	private float t;
	private int n;
	private int i;
	private Sprite[] selected;

	void Start ()
	{
		selected = spriteListArray1;
		n = Random.Range(0, selected.Length);
		t = 0;
	}
	
	void algoRandomAnim()
	{
		var r = Random.Range(0, 100);
		if (r <= 60)
			i = 0;
		else if (spriteListArray2 != null && spriteListArray2.Length > 0)
			i = Random.Range(1, 2);
		else
			i = 0;
		n = 0;
		if (i == 0)
			selected = spriteListArray1;
		else
			selected = spriteListArray2;
	}

	void anim()
	{
		if (spriteListArray1 == null && spriteListArray2 == null)
			return ;
		t += Time.deltaTime;
		float f = 2.0f / framePerSecond[i];
		if (t >= f)
		{
			t = 0;
			n++;
			if (n >= selected.Length)
				algoRandomAnim();
			GetComponent<SpriteRenderer>().sprite = selected[n];
		}
	}

	void Update ()
	{
		anim();
	}
}
