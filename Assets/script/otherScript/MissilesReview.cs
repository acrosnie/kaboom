using UnityEngine;
 
[System.Serializable]
public class MissilesReview : System.Object
{
	public Transform missile;
	public int missilePlayerid;
	public int nbr;
	public bool equipedAtt;
	public bool equipedDef;

	public void construct(Transform c_missile, int c_missilePlayerid, int c_nbr, bool c_equipedAtt, bool c_equipedDef) 
	{
		missile = c_missile;
		missilePlayerid = c_missilePlayerid;
		nbr = c_nbr;
		equipedAtt = c_equipedAtt;
		equipedDef = c_equipedDef;
	}
}