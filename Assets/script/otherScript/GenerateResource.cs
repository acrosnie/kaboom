﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using UnityEngine.UI;
using System.IO;
using System;

public class GenerateResource : MonoBehaviour
{
	public Texture[] textureIcon; 
	public Transform textureBuildingPrefab;
	public GameObject canvasObject;
	public GameObject loadingObject;
	public Transform[] prefabBuildingListProd;
	public Transform[] prefabBuildingListBattle;
	public Transform[] prefabMissileList;
	public Transform missileBase;
	public Transform buildingBattleBase;
	public Texture2D[] textureList;
	public Transform spriteFloorPrefab;
	public Transform canvasBuildingPrefab;
	public JSONNode buildingLevelListJson;
	public JSONNode buildingPlayerListJson;
	public JSONNode playerInfoJson;
	public JSONNode missileLevelListJson;
	public JSONNode missilePlayerListJson;
	public JSONNode missilePlayerConstructJson;
	public BuildingInfoNbr[] buildingInfoNbrObject;
	public MissilesReview[] missilesReview;
	public int idPlayer;
	public long timestamp;
	private string localPathWindows;
	private int totalNumber;
	private int nbr;
	private int spriteNumber;
	private int prefabListNumber;
	private int textureCount;
	private bool generateSpriteBool;
	private bool boolD;
	private bool finish;
	private bool doneGenerate;

	void Awake()
	{
		DontDestroyOnLoad(transform.gameObject);
	}

	void Start ()
	{
		PlayerPrefs.SetInt("idPlayer", -1);
		localPathWindows = Application.persistentDataPath;
		canvasObject.SetActive(false);
		loadingObject.SetActive(true);
		startGenerate();
	}

	IEnumerator fixedCoroutine(int waitTime)
	{
		while (true)
		{
			yield return new WaitForSeconds(waitTime * 2);
			timestamp += (int)waitTime;
		}
	}

	public void startGenerate(bool f = false)
	{
		if (PlayerPrefs.GetInt("idPlayer") > 0)
		{
			idPlayer = PlayerPrefs.GetInt("idPlayer");
			StartCoroutine(getPlayer(f));
		}
		else
			StartCoroutine(addPlayer(f));
	}

	string generateRandomPseudo()
	{
		int[] tabInt = new int[6];
		string pseudo = "";
		for (int i = 0; i < 6; i++)
		{
			tabInt[i] = UnityEngine.Random.Range(0, 5);
		}
		for (int i = 0; i < 6; i++)
		{
			if (tabInt[i] == 0)
				pseudo = pseudo + "a";
			if (tabInt[i] == 1)
				pseudo = pseudo + "b";
			if (tabInt[i] == 3)
				pseudo = pseudo + "c";
			if (tabInt[i] == 4)
				pseudo = pseudo + "d";
			if (tabInt[i] == 5)
				pseudo = pseudo + "e";
		}
		return (pseudo);
	}

	IEnumerator addPlayer(bool f)
	{
		var pseudo = generateRandomPseudo();
		print (pseudo);
		var url = "http://5.196.68.62/api/addPlayer/" + pseudo;
		WWW www = new WWW(url);
		yield return (www);
		if (www.text == "")
			yield return (null);
		PlayerPrefs.SetInt("idPlayer", int.Parse(www.text));
		PlayerPrefs.SetInt("idPreConstructed", (int)UnityEngine.Random.Range(10000, 20000));
		idPlayer = PlayerPrefs.GetInt("idPlayer");
		//buildingPlayerListJson = JSON.Parse(www.text); ??
		StartCoroutine(getPlayer(f));
	}

	IEnumerator getPlayer(bool f)
	{
		var url = "http://5.196.68.62/api/getPlayerInfo/" + idPlayer;
		WWW www = new WWW(url);
		yield return (www);
		if (www.text == "")
			yield return (null);
		playerInfoJson = JSON.Parse(www.text);
		if (loadingObject != null)
			loadingObject.GetComponent<Text>().text = "Welcome " + playerInfoJson["pseudo"];
		timestamp = playerInfoJson["timestamp"].AsInt;
		StartCoroutine(fixedCoroutine(1));
		StartCoroutine(updatePlayer(f));
	}

	IEnumerator updatePlayer(bool f)
	{
		var url = "http://5.196.68.62/api/updatePlayerInfo/" + idPlayer;
		WWW www = new WWW(url);
		yield return (www);
		if (www.text == "")
			yield return (null);
		StartCoroutine(getBuildingPlayerList(f));
	}

	IEnumerator getBuildingPlayerList(bool f)
	{
		var url = "http://5.196.68.62/api/getBuildingPlayerList/" + idPlayer;
		WWW www = new WWW(url);
		yield return (www);
		if (www.text == "")
			yield return (null);
		buildingPlayerListJson = JSON.Parse(www.text);
		StartCoroutine(getBuildingLevelList(f));
	}

	IEnumerator getBuildingLevelList(bool f)
	{
		var url = "http://5.196.68.62/api/getBuildingLevelList";
		WWW www = new WWW(url);
		yield return (www);
		if (www.text == "")
			yield return (null);
		buildingLevelListJson = JSON.Parse(www.text);
		StartCoroutine(getMissileConstructPlayer(f));
	}

	IEnumerator getMissileConstructPlayer(bool f)
	{
		var url = "http://5.196.68.62/api/missilePlayerConstruct/get/" + idPlayer;
		WWW www = new WWW(url);
		yield return (www);
		missilePlayerConstructJson = JSON.Parse(www.text);
		StartCoroutine(getMissileLevelList(f));
	}

	IEnumerator getMissileLevelList(bool f)
	{
		var url = "http://5.196.68.62/api/getMissileLevelList";
		WWW www = new WWW(url);
		yield return (www);
		if (www.text == "")
			yield return (null);
		missileLevelListJson = JSON.Parse(www.text);
		StartCoroutine(getMissilePlayerList(f));
	}

	IEnumerator getMissilePlayerList(bool f)
	{
		var url = "http://5.196.68.62/api/getMissilePlayerList/" + idPlayer;
		WWW www = new WWW(url);
		yield return (www);
		if (www.text == "")
			yield return (null);
		missilePlayerListJson = JSON.Parse(www.text);
		if (!f)
			downloadImg();
		else
			GameObject.Find("script").GetComponent<GenerateBattleMode>().start = true;
	}

	string findFolder(string type, string buildingName, string level)
	{
		var folder = "SPRITE_" + type + "_" + buildingName + "_" + level;
		return (folder);
	}

	public int getNbrImgInJson(JSONNode jsonn, string champImage)
	{
		int k = 0;
		for (int i = 0; i < jsonn.Count; i++)
		{
			for (int j = 0; j < jsonn[i][champImage].Count; j++)
			{
				k++;
			}
		}
		return (k);
	}

	void addInBuildingInfoNbrObject(string name)
	{
		bool found = false;
		for (int i = 0; i < buildingInfoNbrObject.Length; i++)
		{
			if (buildingInfoNbrObject[i] == null)
				buildingInfoNbrObject[i] = new BuildingInfoNbr();
			if ((string)buildingInfoNbrObject[i].name == name)
				found = true;
		}
		if (!found)
		{
			for (int i = 0; i < buildingInfoNbrObject.Length; i++)
			{
				if ((string)buildingInfoNbrObject[i].name == "" || (string)buildingInfoNbrObject[i].name == null)
				{
					int ii = 0;
					buildingInfoNbrObject[i].name = name;
					buildingInfoNbrObject[i].nbrMax = -2;
					for (int j = 0; j < buildingLevelListJson.Count; j++) // building sprites PROD
					{
						if ((string)buildingLevelListJson[j]["building_list"]["name"] == name)
						{
							var levelNbr = (string)buildingLevelListJson[j]["building_list"]["level_nbr"];
							var split = levelNbr.Split(',');
							buildingInfoNbrObject[i].level = new int[split.Length];
							buildingInfoNbrObject[i].nbrForLevel = new int[split.Length];
							foreach (var a in split)
							{
								string[] split2 = a.Split(':');
								buildingInfoNbrObject[i].level[ii] = Int32.Parse(split2[0]);
								buildingInfoNbrObject[i].nbrForLevel[ii] = Int32.Parse(split2[1]);
								ii++;
							}
							return ;
						}
					}
				}
			}
		}
	}

	void setMissileReview()
	{
		int ii = 0;
		Transform t = null;
		missilesReview = new MissilesReview[prefabMissileList.Length];
		for (int i = 0; i < missileLevelListJson.Count; i++)
		{
			t = null;
			foreach (var p  in prefabMissileList)
			{
				if (p.GetComponent<InfoMissile>().missileLevelListId == missileLevelListJson[i]["id"].AsInt)
				{
					t = p;
					break ;
				}
			}
			if (t == null)
				continue ;
			missilesReview[ii] = new MissilesReview();
			missilesReview[ii].missile = t;
			if (t.GetComponent<InfoMissile>().name == "Stealth")
				print(t.GetComponent<InfoMissile>().missileLevelListId);
			ii++;
		}
		for (int i = 0; i < missilePlayerListJson.Count; i++)
		{
			foreach (var m in missilesReview)
			{
				if (m != null && m.missile != null && m.missile.GetComponent<InfoMissile>().missileLevelListId == missilePlayerListJson[i]["missile_level_list"]["id"].AsInt)
				{
					m.missilePlayerid = missilePlayerListJson[i]["id"].AsInt;
					m.nbr = missilePlayerListJson[i]["nbr"].AsInt;
					m.equipedAtt = (missilePlayerListJson[i]["nbr_equiped_att"].AsInt > 0) ? true : false;
					m.equipedDef = (missilePlayerListJson[i]["nbr_equiped_def"].AsInt > 0) ? true : false;
				}
			}
		}
	}

	void downloadImg()
	{
		if (buildingLevelListJson == null || missileLevelListJson == null)
		{
			loadingObject.GetComponent<Text>().text = "Connection error !";
			return ;
		}
		buildingInfoNbrObject = new BuildingInfoNbr[buildingLevelListJson.Count];
		prefabBuildingListProd = new Transform[getNbrImgInJson(buildingLevelListJson, "img_building_prod")];
		prefabBuildingListBattle = new Transform[getNbrImgInJson(buildingLevelListJson, "img_building_battle")];
		prefabMissileList = new Transform[getNbrImgInJson(missileLevelListJson, "img_missile")];
		textureList = new Texture2D[getNbrImgInJson(buildingLevelListJson, "img_building_prod") + getNbrImgInJson(buildingLevelListJson, "img_building_battle") + getNbrImgInJson(missileLevelListJson, "img_missile")];
		for (int i = 0; i < buildingLevelListJson.Count; i++) // building sprites PROD
		{
			addInBuildingInfoNbrObject(buildingLevelListJson[i]["building_list"]["name"]);
			for (int j = 0; j < buildingLevelListJson[i]["img_building_prod"].Count; j++)
			{
				string folder = findFolder("BUILDINGPROD", buildingLevelListJson[i]["building_list"]["name"], buildingLevelListJson[i]["level"]);
				string filename = buildingLevelListJson[i]["img_building_prod"][j]["random_id"] + "_" + buildingLevelListJson[i]["img_building_prod"][j]["name"];
				if (!System.IO.File.Exists(localPathWindows + "/" + folder + "/" + filename)) // si l'image n'existe pas deja, on la download
				{
					totalNumber++;
					StartCoroutine(getImgInServer("http://5.196.68.62/sprite/" + folder + "/" + buildingLevelListJson[i]["img_building_prod"][j]["name"], folder, filename));
				}
			}	
		}
		for (int i = 0; i < buildingLevelListJson.Count; i++) // building sprites BATTLE
		{
			for (int j = 0; j < buildingLevelListJson[i]["img_building_battle"].Count; j++)
			{
				string folder = findFolder("BUILDINGBATTLE", buildingLevelListJson[i]["building_list"]["name"], buildingLevelListJson[i]["level"]);
				string filename = buildingLevelListJson[i]["img_building_battle"][j]["random_id"] + "_" + buildingLevelListJson[i]["img_building_battle"][j]["name"];
				if (!System.IO.File.Exists(localPathWindows + "/" + folder + "/" + filename)) // si l'image n'existe pas deja, on la download
				{
					totalNumber++;
					StartCoroutine(getImgInServer("http://5.196.68.62/sprite/" + folder + "/" + buildingLevelListJson[i]["img_building_battle"][j]["name"], folder, filename));
				}
			}	
		}
		for (int i = 0; i < missileLevelListJson.Count; i++) // missile sprites
		{
			for (int j = 0; j < missileLevelListJson[i]["img_missile"].Count; j++)
			{
				string folder = findFolder("MISSILE", missileLevelListJson[i]["missile_list"]["name"], missileLevelListJson[i]["missile_level"]);
				string filename = missileLevelListJson[i]["img_missile"][j]["random_id"] + "_" + missileLevelListJson[i]["img_missile"][j]["name"];
				if (!System.IO.File.Exists(localPathWindows + "/" + folder + "/" + filename)) // si l'image n'existe pas deja, on la download
				{
					totalNumber++;
					StartCoroutine(getImgInServer("http://5.196.68.62/sprite/" + folder + "/" + missileLevelListJson[i]["img_missile"][j]["name"], folder, filename));
				}
			}	
		}
		for (int i = 0; i < missileLevelListJson.Count; i++) // missile icone
		{
			for (int j = 0; j < missileLevelListJson[i]["img_icone"].Count; j++)
			{
				string folder = findFolder("MISSILEICONE", missileLevelListJson[i]["missile_list"]["name"], missileLevelListJson[i]["missile_level"]);
				string filename = missileLevelListJson[i]["img_icone"][j]["random_id"] + "_" + missileLevelListJson[i]["img_icone"][j]["name"];
				if (!System.IO.File.Exists(localPathWindows + "/" + folder + "/" + filename)) // si l'image n'existe pas deja, on la download
				{
					totalNumber++;
					StartCoroutine(getImgInServer("http://5.196.68.62/sprite/" + folder + "/" + missileLevelListJson[i]["img_icone"][j]["name"], folder, filename));
				}
			}	
		}
		boolD = true;
	}

	IEnumerator getImgInServer(string url, string folder, string nameTexture)
	{
		loadingObject.GetComponent<Text>().text = "Downloading Img, please wait...";
		url = url.Replace(" ", "%20");
		Texture2D texture = new Texture2D(2, 2, TextureFormat.Alpha8, true);
		WWW www = new WWW(url);
		print (url);
		yield return www;
		www.LoadImageIntoTexture(texture);
		loadingObject.GetComponent<Text>().text = "Save Img as folder, please wait...";
		SaveTextureToFile(texture, folder, nameTexture);
		nbr++;
	}

	void deleteFile(string path, string fileName)
	{
		var split = fileName.Split('_');
		var s1 = split[3] + split[4] + split[5];
		var info = new DirectoryInfo(path);
		var fileInfo = info.GetFiles();
		foreach (var file in fileInfo)
		{
			var split2 = file.Name.Split('_');
			var s2 = split2[3] + split2[4] + split2[5];
			if (s1 == s2)
				file.Delete();
		}
	}

	void emptyFolder(string path)
	{
		var info = new DirectoryInfo(path);
		var fileInfo = info.GetFiles();
		foreach (var file in fileInfo)
			file.Delete();
	}

	public void SaveTextureToFile (Texture2D texture, string folder, string imageName)
	{
		var file = localPathWindows + "/" + folder + "/" + imageName;
		Directory.CreateDirectory(localPathWindows + "/" + folder); // create folder
		deleteFile(localPathWindows + "/" + folder, imageName);
		File.WriteAllBytes (file, texture.EncodeToPNG()); // write texture file
		print ("file write to" + file);
	}

	// public int findIdLevelBuildingJson(string f)
	// {
	// 		var ff = f.Split('_');
	// 		var str = ff[1] + "_" + ff[2] + "_" + ff[3] + "_" + ff[4] + "_" + ff[5];
	// 		for (int i = 0; i < buildingLevelListJson.Count; i++)
	// 		{
	// 			for (int j = 0; j < buildingLevelListJson[i]["img_building_prod"].Count; j++)
	// 			{
	// 				var str2 = (string)buildingLevelListJson[i]["img_building_prod"][j]["name"];
	// 				if (str2 == str)
	// 					return (i);
	// 			}
	// 		}
	// 		return (-1);
	// }

	// int prepareSprite()
	// {
	// 	int totalImg = 0;
	// 	for (int i = 0; i < buildingLevelListJson.Count; i++)
	// 	{
			
	// 	}
	// 	return (totalImg);
	// }

	public void generateSprite()
	{
		float startTime = Time.time;
		int total = getNbrImgInJson(buildingLevelListJson, "img_building_prod") + getNbrImgInJson(buildingLevelListJson, "img_building_battle") + getNbrImgInJson(missileLevelListJson, "img_missile");
		loadingObject.GetComponent<Text>().text = "Making coffee...";
		StartCoroutine(getImgInFolder(buildingLevelListJson, "BUILDINGPROD", total, startTime));
		generateSpriteBool = true;
	}

	IEnumerator getImgInFolderIcone(int id)
	{
		var folder = localPathWindows + "/" + findFolder("MISSILEICONE", missileLevelListJson[id]["missile_list"]["name"], missileLevelListJson[id]["missile_level"]);
		var url = "file://" + folder + "/" + missileLevelListJson[id]["img_icone"][0]["random_id"] + "_" + missileLevelListJson[id]["img_icone"][0]["name"];
		Texture2D texture = new Texture2D(1, 1);
		WWW www = new WWW(url);
		yield return www;
		if (www.text == "")
			yield return (false);
		texture = www.texture;
		var spriteIcone = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f), texture.width, 0, SpriteMeshType.FullRect, Vector4.zero);
		prefabMissileList[prefabListNumber - 1].GetComponent<InfoMissile>().icone = spriteIcone;
	}

	IEnumerator getImgInFolder(JSONNode jsonn, string type, int total, float t)
	{	
		prefabListNumber = 0;
		string folder = "";
		string url = "";
		string name = "";
		string level = "";
		string imgChampName = "img";
		if (type == "BUILDINGPROD")
			imgChampName = "img_building_prod";
		else if (type == "BUILDINGBATTLE")
			imgChampName = "img_building_battle";
		else if (type == "MISSILE")
			imgChampName = "img_missile";
		for (int i = 0; i < jsonn.Count; i++)
		{
			var totalImg = jsonn[i][imgChampName].Count;
			for (int j = 0; j < jsonn[i][imgChampName].Count; j++)
			{
				if (type == "BUILDINGPROD" || type == "BUILDINGBATTLE")
				{
					name = jsonn[i]["building_list"]["name"];
					level = jsonn[i]["level"];
				}
				else if (type == "MISSILE")
				{
					name = jsonn[i]["missile_list"]["name"];
					level = jsonn[i]["missile_level"];
				}
				folder = localPathWindows + "/" + findFolder(type, name, level);
				url = "file://" + folder + "/" + jsonn[i][imgChampName][j]["random_id"] + "_" + jsonn[i][imgChampName][j]["name"];
				Sprite[] newSprite = new Sprite[jsonn[i][imgChampName][j]["nbr_img_x"].AsInt * jsonn[i][imgChampName][j]["nbr_img_y"].AsInt];
				url = url.Replace(" ", "%20");
				url = url.Replace("\\", "/");
				Texture2D texture = new Texture2D(1, 1);
				WWW www = new WWW(url);
				yield return www;
				texture = www.texture;
				if (type == "BUILDINGBATTLE" || type == "MISSILE")
				{
					var w =  (float)((float)texture.width / (float)jsonn[i][imgChampName][j]["nbr_img_x"].AsInt);
					var h =  (float)((float)texture.height / (float)jsonn[i][imgChampName][j]["nbr_img_y"].AsInt);
					var trueH = w / 1.41436464088f; // trueh
					var ii = 0;
					for (int y = 0; y < jsonn[i][imgChampName][j]["nbr_img_y"].AsInt; y++)
					{
						for (int x = 0; x < jsonn[i][imgChampName][j]["nbr_img_x"].AsInt; x++)
						{
							var r = ((float)trueH / 2.0f) / (float)h;
							var posX = (float)(int)(w * x);
							var posY = (h * (float)jsonn[i][imgChampName][j]["nbr_img_y"].AsInt) - (h * (y + 1));
							if (type == "BUILDINGPROD" || type == "BUILDINGBATTLE")
								newSprite[ii] = Sprite.Create(texture, new Rect(posX, posY, w, h), new Vector2(0.5f, r), w / (float)jsonn[i][imgChampName][j]["nbr_case"].AsInt, 0, SpriteMeshType.FullRect, Vector4.zero);
							else if (type == "MISSILE")
								newSprite[ii] = Sprite.Create(texture, new Rect(posX, posY, w, h), new Vector2(0.5f, 0.5f), w / (float)(jsonn[i][imgChampName][j]["nbr_case"].AsInt / 1.5f), 0, SpriteMeshType.FullRect, Vector4.zero);
							newSprite[ii].name = name + "_" + j;
							ii++;
							spriteNumber++;
						}		
					}
				}
				if (type == "BUILDINGPROD" && newSprite.Length > 0)
				{
					if (j == 0)
						generateBuildingModelGameObjectProd(i, j, texture, totalImg, imgChampName);
					else
					{
						//prefabBuildingListProd[prefabListNumber - 1].Find("spriteBuilding").GetComponent<AnimSprite>().spriteListArray2 = newSprite;
						//prefabBuildingListProd[prefabListNumber - 1].Find("spriteBuilding").GetComponent<AnimSprite>().framePerSecond[j] = buildingLevelListJson[i][imgChampName][j]["speed"].AsFloat;
					}
				}
				else if (type == "BUILDINGBATTLE" && newSprite.Length > 0)
				{
					if (j == 0)
						generateBuildingModelGameObjectBattle(i, j, newSprite, totalImg, imgChampName);
				}
				else if (type == "MISSILE" && newSprite.Length > 0)
				{
					if (j == 0)
					{
						generateMissileModelGameObject(i, j, newSprite, totalImg);
						StartCoroutine(getImgInFolderIcone(i));
					}
					else
					{
						prefabMissileList[prefabListNumber - 1].GetComponent<AnimSprite>().spriteListArray2 = newSprite;
						prefabMissileList[prefabListNumber - 1].GetComponent<AnimSprite>().framePerSecond[j] = missileLevelListJson[i][imgChampName][j]["speed"].AsFloat;
					}
				}
				textureCount++;
				var p = (int)((float)textureCount / (float)total * 100.0f);
				loadingObject.GetComponent<Text>().text = "Making " + type + "... (" + p + "%) | (" + textureCount + " / " + total + ")" + " | spriteNumber : " + spriteNumber + " | TIME : " + (Time.time - t) + " Sec.";
				if (total == textureCount)
				{
					if (type == "MISSILE")
					{
						setMissileReview();
						finish = true;
						float endTime = Time.time;
						float timeElapsed = (endTime - t);
					}
					if (finish)
						StartCoroutine(loadLevel((1)));
				}
			}
		}
		if (type == "BUILDINGPROD")
			StartCoroutine(getImgInFolder(jsonn, "BUILDINGBATTLE", total, t));
		else if (type == "BUILDINGBATTLE")
			StartCoroutine(getImgInFolder(missileLevelListJson, "MISSILE", total, t));
	}

	IEnumerator loadLevel(int level)
	{
		AsyncOperation async = Application.LoadLevelAsync(level);
		yield return (async);
	}

	void fillInfoMissile(Transform b, int id, int j)
	{
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().name = missileLevelListJson[id]["missile_list"]["name"];
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().description = missileLevelListJson[id]["missile_list"]["name"];
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().missileLevelListId = missileLevelListJson[id]["id"].AsInt;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().missileListId = missileLevelListJson[id]["missile_list"]["id"].AsInt;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().damageMissile = missileLevelListJson[id]["damage_missile"].AsInt;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().damageBuilding = missileLevelListJson[id]["damage_building"].AsInt;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().health = missileLevelListJson[id]["health"].AsInt;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().missileLevel = missileLevelListJson[id]["missile_level"].AsInt;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().labUnlockLevel = missileLevelListJson[id]["lab_unlock_level"].AsInt;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().ressource1Cost = 0;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().ressource2Cost = missileLevelListJson[id]["ressource2_cost_build"].AsInt;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().ressource3Cost = 0;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().ressource2CostUpgrade = missileLevelListJson[id]["ressource2_cost_upgrade"].AsInt;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().ressource4CostUpgrade = 0;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().area = missileLevelListJson[id]["area"].AsFloat;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().speed = missileLevelListJson[id]["speed"].AsFloat;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().coolDown = missileLevelListJson[id]["cool_down"].AsFloat;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().draggable = (missileLevelListJson[id]["draggable"].AsInt > 0) ? true : false;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().freeze = (missileLevelListJson[id]["freeze"].AsInt > 0) ? true : false;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().magnet = (missileLevelListJson[id]["magnet"].AsInt > 0) ? true : false;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().magnet_radius = missileLevelListJson[id]["attraction_radius"].AsFloat;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().timeExplosion = missileLevelListJson[id]["time_explosion"].AsFloat;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().prodTime = missileLevelListJson[id]["prod_time"].AsFloat;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().upgradeTime = missileLevelListJson[id]["upgrade_time"].AsFloat;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().sprite = prefabMissileList[prefabListNumber].GetComponent<SpriteRenderer>().sprite;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().explodeInPoint = true;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().coolDown = missileLevelListJson[id]["cool_down"].AsFloat;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().freeze_speed_reduc = missileLevelListJson[id]["speed_reduc"].AsFloat;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().frag = (missileLevelListJson[id]["frag"].AsInt > 0) ? true : false;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().nb_frag	= missileLevelListJson[id]["nb_frag"].AsInt;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().dmg_missiles_frag = missileLevelListJson[id]["dmg_missiles_frag"].AsInt;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().dmg_buildings_frag = missileLevelListJson[id]["dmg_buildings_frag"].AsInt;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().health_frag	= missileLevelListJson[id]["health_frag"].AsFloat;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().speed_frag = missileLevelListJson[id]["speed_frag"].AsFloat;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().area_frag = missileLevelListJson[id]["area_frag"].AsFloat;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().att = (missileLevelListJson[id]["missile_list"]["att"].AsInt > 0) ? true : false;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().def = (missileLevelListJson[id]["missile_list"]["def"].AsInt > 0) ? true : false;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().missileFactLevel = missileLevelListJson[id]["missile_list"]["lvl_missile_fact"].AsInt;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().intantR2Prod = missileLevelListJson[id]["instant_r2_prod"].AsInt;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().intantTimeProd = missileLevelListJson[id]["instant_time_prod"].AsInt;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().intantR2Upgrade = missileLevelListJson[id]["instant_r2_upgrade"].AsInt;
		prefabMissileList[prefabListNumber].GetComponent<InfoMissile>().intantTimeUpgrade = missileLevelListJson[id]["instant_time_upgrade"].AsInt;
	}

	void generateMissileModelGameObject(int id, int j, Sprite[] newSprite, int totalImg)
	{
		var b = Instantiate(missileBase, missileBase.position, missileBase.rotation) as Transform;
		b.name = missileLevelListJson[id]["missile_list"]["name"] + " Level " + missileLevelListJson[id]["missile_level"];
		b.tag = "missile";
		b.gameObject.AddComponent<SpriteRenderer>();
		b.GetComponent<SpriteRenderer>().sprite = newSprite[0];
		b.GetComponent<SpriteRenderer>().sortingOrder = 0;
		b.gameObject.AddComponent<EffectMissile>();
		b.gameObject.AddComponent<Rigidbody2D>();
		b.GetComponent<Rigidbody2D>().isKinematic = true;
		var c = new GameObject();
		c.name = "missileColliderObject";
		c.gameObject.AddComponent<BoxCollider2D>();
		c.GetComponent<BoxCollider2D>().isTrigger = true;
		c.GetComponent<BoxCollider2D>().size = new Vector2(b.GetComponent<SpriteRenderer>().bounds.size.x / 1.4f, b.GetComponent<SpriteRenderer>().bounds.size.y / 1.4f);
		c.tag = "missileCollider";
		c.transform.parent = b;
		b.gameObject.AddComponent<AnimSprite>();
		b.GetComponent<AnimSprite>().spriteListArray1 = newSprite;
		b.GetComponent<AnimSprite>().framePerSecond = new float[totalImg];
		b.GetComponent<AnimSprite>().framePerSecond[0] = missileLevelListJson[id]["img_missile"][0]["speed"].AsFloat;
		b.transform.parent = transform;
		b.gameObject.SetActive(false);
		prefabMissileList[prefabListNumber] = b.transform;
		fillInfoMissile(prefabMissileList[prefabListNumber], id, j);
		prefabListNumber++;
	}


	void generateBuildingModelGameObjectBattle(int id, int j, Sprite[] newSprite, int totalImg, string imgChampName)
	{
		var b = Instantiate(buildingBattleBase, buildingBattleBase.position, buildingBattleBase.rotation) as Transform;
		b.name = buildingLevelListJson[id]["building_list"]["name"] + " Level " + buildingLevelListJson[id]["level"] + "_battle";
		b.tag = "building";
		b.GetComponent<SpriteRenderer>().sprite = newSprite[0];
		b.gameObject.AddComponent<BoxCollider2D>();
		b.GetComponent<BoxCollider2D>().isTrigger = true;
		b.transform.parent = transform;
		b.gameObject.SetActive(false);
		prefabBuildingListBattle[prefabListNumber] = b.transform;
		DontDestroyOnLoad(prefabBuildingListBattle[prefabListNumber]);
		prefabBuildingListBattle[prefabListNumber].GetComponent<BuildingBattle>().buildingLevelListId = buildingLevelListJson[id]["id"].AsInt;
		prefabListNumber++;
	}

	void generateBuildingModelGameObjectProd(int id, int j, Texture2D newTexture, int totalImg, string imgChampName)
	{
		var b = new GameObject();
		b.name = buildingLevelListJson[id]["building_list"]["name"] + " Level " + buildingLevelListJson[id]["level"];
		b.tag = "building";
		b.AddComponent<BuildingPlacementProd>();
		b.AddComponent<BuildingInfo>();
		b.AddComponent<BuildingAction>();
		b.AddComponent<ButtonActionBuilding>();
		b.AddComponent<Rigidbody2D>();
		b.GetComponent<Rigidbody2D>().isKinematic = true;
		Transform floor = Instantiate(spriteFloorPrefab, spriteFloorPrefab.position, spriteFloorPrefab.rotation) as Transform;
		floor.name = "spriteFloor";
		floor.localScale = new Vector2(buildingLevelListJson[id][imgChampName][j]["nbr_case"].AsInt, buildingLevelListJson[id][imgChampName][j]["nbr_case"].AsInt);
		floor.parent = b.transform;
		floor.Find("arrowGroupe").gameObject.SetActive(false);
		floor.GetComponent<SpriteRenderer>().enabled = false;
		var collider = new GameObject();
		collider.name = "collider";
		collider.transform.localScale = new Vector3(buildingLevelListJson[id][imgChampName][j]["nbr_case"].AsInt * 0.62f, buildingLevelListJson[id][imgChampName][j]["nbr_case"].AsInt * 0.62f, 1);
		collider.transform.parent = b.transform;
		collider.transform.rotation =  Quaternion.Euler(new Vector3(45, 0, 45));
		collider.AddComponent<BoxCollider2D>();
		collider.GetComponent<BoxCollider2D>().isTrigger = true;
		collider.tag = "buildingColliderPlacement";
		var spriteBuilding = Instantiate(textureBuildingPrefab, textureBuildingPrefab.position, textureBuildingPrefab.rotation) as Transform;
		spriteBuilding.name = "spriteBuilding";
		float ratioScaleY = ((newTexture.height / buildingLevelListJson[id][imgChampName][0]["nbr_img_y"].AsFloat) / (newTexture.width / buildingLevelListJson[id][imgChampName][0]["nbr_img_x"].AsFloat));
		float scaleY = ratioScaleY * floor.localScale.x;
		spriteBuilding.transform.localScale = new Vector3(floor.localScale.x, scaleY, floor.localScale.z);
		spriteBuilding.GetComponent<AnimTexture>().nbrCaseX = buildingLevelListJson[id][imgChampName][0]["nbr_img_x"].AsInt;
		spriteBuilding.GetComponent<AnimTexture>().nbrCaseY = buildingLevelListJson[id][imgChampName][0]["nbr_img_y"].AsInt;
		spriteBuilding.GetComponent<AnimTexture>().framePerSecond = buildingLevelListJson[id][imgChampName][0]["speed"].AsFloat;
		spriteBuilding.GetComponent<AnimTexture>().texture = newTexture;
		spriteBuilding.position += new Vector3(0, (spriteBuilding.GetComponent<Renderer>().bounds.extents.y - floor.GetComponent<Renderer>().bounds.extents.y), 0);
		spriteBuilding.transform.parent = b.transform;
		Color[] pix = newTexture.GetPixels(0, 0, newTexture.width / buildingLevelListJson[id][imgChampName][0]["nbr_img_x"].AsInt, newTexture.height / buildingLevelListJson[id][imgChampName][0]["nbr_img_y"].AsInt);
		Texture2D texture = new Texture2D(newTexture.width / buildingLevelListJson[id][imgChampName][0]["nbr_img_x"].AsInt, newTexture.height / buildingLevelListJson[id][imgChampName][0]["nbr_img_y"].AsInt);
		texture.SetPixels(pix);
		texture.Apply();
		fillInfoPrefab(b, id, j, imgChampName, texture);
		prefabBuildingListProd[prefabListNumber] = b.transform;
		DontDestroyOnLoad(prefabBuildingListProd[prefabListNumber]);
		b.transform.parent = transform;
		b.transform.position = new Vector3(0, 0, -11);
		var canvasInfo = Instantiate(canvasBuildingPrefab, canvasBuildingPrefab.position, canvasBuildingPrefab.rotation) as Transform;
		if (buildingLevelListJson[id]["prod_rate_ressource1"].AsFloat > 0)
			canvasInfo.Find("getRessource").GetComponent<RawImage>().texture = textureIcon[0];
		else if (buildingLevelListJson[id]["prod_rate_ressource2"].AsFloat > 0)
			canvasInfo.Find("getRessource").GetComponent<RawImage>().texture = textureIcon[1];
		else if (buildingLevelListJson[id]["prod_rate_ressource3"].AsFloat > 0)
			canvasInfo.Find("getRessource").GetComponent<RawImage>().texture = textureIcon[2];	
		canvasInfo.name = "CanvasBuilding";
		canvasInfo.SetParent(b.transform);
		canvasInfo.Find("inSelect").Find("nameBuilding").GetComponent<Text>().text = (string)buildingLevelListJson[id]["building_list"]["name"];
		canvasInfo.Find("inSelect").Find("levelBuilding").GetComponent<Text>().text = "Level " + buildingLevelListJson[id]["level"].AsInt;
		canvasInfo.Find("inSelect").gameObject.SetActive(false);
		b.SetActive(false);
		prefabListNumber++;
	}

	void fillInfoPrefab(GameObject b, int id, int j, string imgChampName, Texture2D newTexture)
	{
		b.GetComponent<BuildingInfo>().name = buildingLevelListJson[id]["building_list"]["name"];
		b.GetComponent<BuildingInfo>().description = buildingLevelListJson[id]["building_list"]["name"];
		b.GetComponent<BuildingInfo>().level = buildingLevelListJson[id]["level"].AsInt;
		b.GetComponent<BuildingInfo>().health = buildingLevelListJson[id]["health"].AsFloat;
		b.GetComponent<BuildingInfo>().costBuyRessource1 = buildingLevelListJson[id]["_cost_ressource1_build"].AsInt;
		b.GetComponent<BuildingInfo>().costBuyRessource2 = buildingLevelListJson[id]["_cost_ressource2_build"].AsInt;
		b.GetComponent<BuildingInfo>().costBuyRessource3 = buildingLevelListJson[id]["_cost_ressource3_build"].AsInt;
		b.GetComponent<BuildingInfo>().levelRessource1 = buildingLevelListJson[id]["_cost_ressource1_upgrade"].AsInt;
		b.GetComponent<BuildingInfo>().levelRessource2 = buildingLevelListJson[id]["_cost_ressource2_upgrade"].AsInt;
		b.GetComponent<BuildingInfo>().levelRessource3 = buildingLevelListJson[id]["_cost_ressource3_upgrade"].AsInt;
		b.GetComponent<BuildingInfo>().capacityRessource1 = buildingLevelListJson[id]["capacity_ressource1"].AsInt;
		b.GetComponent<BuildingInfo>().capacityRessource2 = buildingLevelListJson[id]["capacity_ressource2"].AsInt;
		b.GetComponent<BuildingInfo>().capacityRessource3 = buildingLevelListJson[id]["capacity_ressource3"].AsInt;
		b.GetComponent<BuildingInfo>().stockRessource1 = buildingLevelListJson[id]["stock_ressource1"].AsInt;
		b.GetComponent<BuildingInfo>().stockRessource2 = buildingLevelListJson[id]["stock_ressource2"].AsInt;
		b.GetComponent<BuildingInfo>().stockRessource3 = buildingLevelListJson[id]["stock_ressource3"].AsInt;
		b.GetComponent<BuildingInfo>().ratioRessource1 = buildingLevelListJson[id]["prod_rate_ressource1"].AsFloat;
		b.GetComponent<BuildingInfo>().ratioRessource2 = buildingLevelListJson[id]["prod_rate_ressource2"].AsFloat;
		b.GetComponent<BuildingInfo>().ratioRessource3 = buildingLevelListJson[id]["prod_rate_ressource3"].AsFloat;
		b.GetComponent<BuildingInfo>().buyable = (buildingLevelListJson[id]["buyable"].AsInt == 1) ? true : false;
		b.GetComponent<BuildingInfo>().preConstructed = (buildingLevelListJson[id]["preConstructed"].AsInt == 1) ? true : false;
		b.GetComponent<BuildingInfo>().att = (buildingLevelListJson[id]["def_line"].AsInt == 1) ? false : true;
		b.GetComponent<BuildingInfo>().def = !b.GetComponent<BuildingInfo>().att;
		b.GetComponent<BuildingInfo>().constructionTime = buildingLevelListJson[id]["time"].AsFloat;
		b.GetComponent<BuildingInfo>().texture = newTexture;
		b.GetComponent<BuildingInfo>().isProd = true;
		b.GetComponent<BuildingInfo>().buildingLevelListId = buildingLevelListJson[id]["id"].AsInt;
		b.GetComponent<BuildingInfo>().buildingLevelListIdnextLevel = findNextLevel(buildingLevelListJson[id]["building_list"]["name"], buildingLevelListJson[id]["level"].AsInt, imgChampName);
		b.GetComponent<BuildingInfo>().levelRessource4 = buildingLevelListJson[id]["cost_h_c_upgrade"].AsInt;
		b.GetComponent<BuildingInfo>().xp = buildingLevelListJson[id]["xp"].AsInt;
		b.GetComponent<BuildingInfo>().instantR1 = buildingLevelListJson[id]["instant_r1"].AsInt;
		b.GetComponent<BuildingInfo>().instantR2 = buildingLevelListJson[id]["instant_r2"].AsInt;
		b.GetComponent<BuildingInfo>().instantR3 = buildingLevelListJson[id]["instant_r3"].AsInt;
		b.GetComponent<BuildingInfo>().instantTime = buildingLevelListJson[id]["instant_time"].AsInt;
		b.GetComponent<BuildingInfo>().instant_build_cost_r1 = buildingLevelListJson[id]["instant_build_cost_r1"].AsInt;
		b.GetComponent<BuildingInfo>().instant_build_cost_r2 = buildingLevelListJson[id]["instant_build_cost_r2"].AsInt;
		b.GetComponent<BuildingInfo>().instant_build_cost_r3 = buildingLevelListJson[id]["instant_build_cost_r3"].AsInt;
		b.GetComponent<BuildingInfo>().totalInstant = buildingLevelListJson[id]["total_instant"].AsInt;
		b.GetComponent<BuildingInfo>().healthShield = buildingLevelListJson[id]["building_effect"]["health_ability"].AsFloat;
	}

	int findNextLevel(string name, int level, string imgChampName)
	{
		for (int i = 0; i < buildingLevelListJson.Count; i++)
		{
			if ((string)buildingLevelListJson[i]["building_list"]["name"] == name && (int)(buildingLevelListJson[i]["level"].AsInt - 1) == level && (string)buildingLevelListJson[i][imgChampName][0]["name"] != null)
				return (buildingLevelListJson[i]["id"].AsInt);
		}
		return (-1);
	}

	//buildable
	void Update () 
	{
		if (doneGenerate)
			return ;
		if (nbr == totalNumber && !generateSpriteBool && boolD)
			generateSprite();
		if (finish)
		{
			loadingObject.GetComponent<Text>().text = "Opening Production Center...";
			//canvasObject.SetActive(true);
			//loadingObject.SetActive(false);
			doneGenerate = true;
		}
	}
}
