using UnityEngine;
using System.Collections;

public class NetworkConnect : Photon.MonoBehaviour
{
	public bool ia;
	public bool attInFight;
	public bool defInFight;
	public GameObject game;
	public  bool host = false;
	private RoomInfo[] roomsList;
	private const string roomName = "Kaboom";
	private bool ConnectInUpdate = true;
	private long uniqueId;
	private bool ready;
	private bool done;

	void createRandomId()
	{
		uniqueId = Random.Range(1000000000, 2000000000);
	}

	void Start ()
	{
		ia = true;
		if (ia)
			PhotonNetwork.offlineMode = true;
		game.SetActive(false);
		createRandomId();
	}

	void OnApplicationQuit()
	{
		PhotonNetwork.Disconnect();
	}

	void labelCustom(string textString)
	{
		GUILayout.BeginArea(new Rect(Screen.width / 2 - 200, Screen.height / 2 - 100, 400, 400));
			GUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();
					GUILayout.Label(textString);
				GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
		GUILayout.EndArea();
	}

	void OnGUI()
	{
		if (done)
			return ;
		displayPing();
		if (!PhotonNetwork.connected)
			labelCustom(PhotonNetwork.connectionStateDetailed.ToString());
		else if (PhotonNetwork.connected && !host)
			labelCustom("Vous vous connectez...");
		else if (host && !ready)
		{
			var a = (attInFight) ? "ATT" : "DEF";
			labelCustom("EN ATTENTE D'UN JOUEUR... | " + a);
		}
		if (!ready)
		{
			if (GUI.Button(new Rect(Screen.width - 120, 10, 100, 40), (1 == 1) ? "CANCEL" : "CANCEL"))
			{
				// PhotonNetwork.Disconnect();
				// Application.LoadLevel (0);
			}
		}
	}

	void displayPing()
	{
		GUILayout.Label(PhotonNetwork.networkingPeer.RoundTripTime.ToString());
	}

	void Update()
	{
		if (done)
			return ;
		if (ready || ia)
		{
			game.SetActive(true);
			done = true;
		}
		else if (!PhotonNetwork.connected && ConnectInUpdate)
		{
			ConnectInUpdate = false;
			PhotonNetwork.ConnectUsingSettings("0.1");
		}
	}

	public void OnConnectedToMaster()
	{
		PhotonNetwork.JoinRandomRoom();
	}

	public void OnPhotonRandomJoinFailed()
	{
		PhotonNetwork.CreateRoom(roomName + Random.Range(0, 10000), new RoomOptions() { maxPlayers = 2 }, null);
		host = true;
		if (Random.Range(0, 2) == 0)
			defInFight = true;
		else
			attInFight = true;
		photonView.RPC("defOrAtt", PhotonTargets.OthersBuffered, attInFight, defInFight);
	}

	public void OnFailedToConnectToPhoton(DisconnectCause cause)
	{
		Debug.LogError("Cause: " + cause);
	}

	[PunRPC]
	void defOrAtt(bool att, bool def)
	{
		if (host)
			ready = true;
		if (!host && (att || def))
		{
			attInFight = def;
			defInFight = att;
			ready = true;
		}
	}

	[PunRPC]
	void RequestForDefOrAtt()
	{
		if (host)
			photonView.RPC("defOrAtt", PhotonTargets.AllBufferedViaServer, attInFight, defInFight);
	}

	public void OnJoinedRoom()
	{
		if (!host)
		{
			photonView.RPC("RequestForDefOrAtt", PhotonTargets.AllBufferedViaServer);
		}
	}

	public void OnJoinedLobby()
	{
		OnConnectedToMaster();
	}

	void OnReceivedRoomListUpdate()
	{
		roomsList = PhotonNetwork.GetRoomList();
	}

	void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
	{
		// PhotonNetwork.Disconnect();
		// Application.LoadLevel (0);
	}
}
