using UnityEngine;
using System.Collections;
using SimpleJSON;
using UnityEngine.UI;
using System.IO;
using System;

public class readMapJson : MonoBehaviour
{
	public string filePath;
	public JSONNode fileMapJson;
	public Texture2D[] spriteListTexture;

	void Start()
	{
		TextAsset file = Resources.Load(filePath) as TextAsset;
		fileMapJson = JSON.Parse(file.text);
		loadMap();
	}

	void loadMap()
	{
		var contain = new GameObject();
		contain.name = "containTiled";
		int n = 0;
		for (int i = 0; i < fileMapJson["layers"].Count; i++)
		{
			var w = fileMapJson["tilesets"][i]["tilewidth"].AsInt;
			var h = fileMapJson["tilesets"][i]["tileheight"].AsInt;
			var nbrSpriteInTiledX = fileMapJson["tilesets"][i]["imagewidth"].AsInt / w;
			var nbrSpriteInTiledY = fileMapJson["tilesets"][i]["imageheight"].AsInt / h;
			for (int y = 0; y < fileMapJson["layers"][i]["height"].AsInt; y++)
			{
				for (int x = 0; x < fileMapJson["layers"][i]["width"].AsInt; x++)
				{
					var sP = fileMapJson["layers"][i]["data"][n].AsInt;
					if (sP > 0)
					{
						var g = new GameObject();
						g.AddComponent<SpriteRenderer>();
						g.transform.position = new Vector2((x * 0.5f - (y * 0.5f)), ((-y * 0.353515625f) + (x * -0.353515625f)) + 17.335f);
						var s = Sprite.Create(spriteListTexture[0], new Rect(((sP % nbrSpriteInTiledX) - 1) * w, (h * (nbrSpriteInTiledY - 1)) - ((int)(sP / nbrSpriteInTiledX) * h), w, h), new Vector2(0.5f, 0.5f), w, 0, SpriteMeshType.FullRect, Vector4.zero);
						//var s = Sprite.Create(spriteListTexture[0], new Rect(0, 0, w, h), new Vector2(0.5f, 0.5f), w, 0, SpriteMeshType.FullRect, Vector4.zero);
						g.GetComponent<SpriteRenderer>().sprite = s;
						g.transform.parent = contain.transform;
					}
					n++;
				}
			}
		}
	}
}
