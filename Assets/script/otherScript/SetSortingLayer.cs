﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[ExecuteInEditMode]
public class SetSortingLayer : MonoBehaviour
{
	public Renderer MyRenderer;
	public string MySortingLayer;
	public int MySortingOrderInLayer;

	void Start ()
	{
		if (MyRenderer == null)
			 MyRenderer = this.GetComponent<Renderer>();
	}

	void Update ()
	{
		if (MyRenderer == null)
			MyRenderer = this.GetComponent<Renderer>();
		MyRenderer.sortingLayerName = MySortingLayer;
		MyRenderer.sortingOrder = MySortingOrderInLayer;
		//Debug.Log(MyRenderer.sortingLayerName + " " + MyRenderer.sortingOrder);
	}
}