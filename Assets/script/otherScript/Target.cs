﻿using UnityEngine;
using System.Collections;

public class Target : MonoBehaviour
{
	private float t;

	void Start ()
	{
		t = transform.localScale.x;	
	}
	
	// Update is called once per frame
	void Update ()
	{
		t -= Time.deltaTime * 0.5f;
		if (t <= 0)
			Destroy(gameObject);
		transform.localScale = new Vector3(t, t, 1);
	}
}
