using UnityEngine;
using System.Collections;

public class AnimTexture : MonoBehaviour
{
	public Texture2D texture;
	public int nbrCaseX;
	public int nbrCaseY;
	public float framePerSecond;
	private int x;
	private int y;
	private float t;

	void Start ()
	{
		y = nbrCaseY - 1;
		GetComponent<Renderer>().material.mainTexture = texture;
		GetComponent<Renderer>().material.mainTextureScale = new Vector2((1.0f / nbrCaseX), (1.0f / nbrCaseY));
	}
	
	void animTexture()
	{
		if (t > (2.0f / framePerSecond))
		{
			t = 0;
			x++;
			if (x == nbrCaseX)
				x = 0;
			if (x == 0)
				y--;
			if (y == -1)
				y = nbrCaseY - 1;
		}
		GetComponent<Renderer>().material.mainTextureOffset = new Vector2(x * (1.0f / nbrCaseX), y * (1.0f / nbrCaseY));
		t += Time.deltaTime;
	}

	void Update()
	{
		animTexture();
	}
}
