using UnityEngine;

[System.Serializable]
public class Case : System.Object
{
	public Vector2 position;
	public bool isEmpty;
	public bool isDefense;
}