using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonCooldown : MonoBehaviour
{
	public float coolDown;
	public float t;
	public bool ready;

	void Update()
	{
		if (t < coolDown)
		{
			t += Time.deltaTime;
			GetComponent<Image>().fillAmount = 1.0f / (coolDown / t);
			return ;
		}
		if (!ready)
		{
			GetComponent<Image>().fillAmount = 1;
			GetComponent<Button>().interactable = true;
			ready = true;
		}

	}

	public void startCoolDown()
	{
		ready = false;
		t = 0;
		GetComponent<Button>().interactable = false;
	}
}