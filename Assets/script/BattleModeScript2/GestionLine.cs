using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GestionLine: Photon.MonoBehaviour
{
	public Transform parentMe;
	public float timeAlive;
	public float freeze_speed_reduc;
	public bool freeze;
	public bool fire;
	public bool attInFight;
	public bool defInFight;
	private LineRenderer lineDraw;
	private float tLineDraw;
	private List<Vector3> lineRendererList;

	void Start()
	{
		Destroy(this.gameObject, 20.0f);
		lineRendererList = new List<Vector3>();
		lineDraw = GetComponent<LineRenderer>();
		lineDraw.SetWidth(0.2f, 0.2f);
		lineDraw.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
		if (freeze)
			lineDraw.SetColors(Color.blue, Color.blue);
		if (fire)
			lineDraw.SetColors(Color.red, Color.red);
		lineDraw.useWorldSpace = true;
	}

	void drawLine()
	{
		tLineDraw += Time.deltaTime;

		if (tLineDraw > 0.2f)
		{
			if (freeze)
			{
				var freezeCollider = new GameObject("freezeCollider");
				freezeCollider.AddComponent<BoxCollider2D>();
				freezeCollider.GetComponent<BoxCollider2D>().isTrigger = true;
				freezeCollider.transform.position = transform.position;
				freezeCollider.AddComponent<FreezeInfo>();
				freezeCollider.GetComponent<FreezeInfo>().freeze_speed_reduc = freeze_speed_reduc;
				freezeCollider.GetComponent<FreezeInfo>().timeAlive = timeAlive;
				freezeCollider.GetComponent<FreezeInfo>().attInFight = attInFight;
				freezeCollider.GetComponent<FreezeInfo>().defInFight = defInFight;
			}
			else if (fire)
			{
				var fireCollider = new GameObject("fireCollider");
				fireCollider.AddComponent<BoxCollider2D>();
				fireCollider.GetComponent<BoxCollider2D>().isTrigger = true;
				fireCollider.transform.position = transform.position;
			}
			lineRendererList.Add(transform.position);
			lineDraw.SetVertexCount(lineRendererList.Count);
			lineDraw.SetPosition(lineRendererList.Count - 1, transform.position);
			Invoke("removeLine", timeAlive);
			tLineDraw = 0;
		}
	}

	void removeLine()
	{
		lineRendererList.RemoveAt(0);
		lineDraw.SetVertexCount(lineRendererList.Count);
		for (int i = 0; i < lineRendererList.Count; i++)
		{
			lineDraw.SetPosition(i, lineRendererList[i]);
		}
	}

	void Update()
	{
		drawLine();
		if (parentMe != null)
			transform.position = parentMe.position;
	}
}