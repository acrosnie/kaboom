﻿using UnityEngine;
using System.Collections;

public class MoveBattle : MonoBehaviour
{
	public float perspectiveZoomSpeed;
	public float MoveSensitivity = 2.0f;
	public float inertiaDuration = 1.0f;
	public float maxZoom, minZoom = 0.0f;
	private float scrollVelocity = 0.0f;
	private Vector3 scrollDirection = Vector3.zero;
	private float timeTouchPhaseEnded;
	private Vector2 delta;
	private Vector3 desiredVelocity;
	private Camera _camera;
	private ShootScript shootScript;

	void Start()
	{
		_camera = Camera.main;
		shootScript = GameObject.Find("script").GetComponent<ShootScript>();
	}

	void zoomInMobile()
	{
		Touch touchZero = Input.GetTouch(0);
		Touch touchOne = Input.GetTouch(1);
		Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
		Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
		float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
		float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
		float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
		if (deltaMagnitudeDiff > 1.5f)
			_camera.orthographicSize += perspectiveZoomSpeed *  Time.deltaTime;
		else if (deltaMagnitudeDiff < 1.5f)
			_camera.orthographicSize -= perspectiveZoomSpeed *  Time.deltaTime;
		if (_camera.orthographicSize < 4)
			_camera.orthographicSize = 4;
		if (_camera.orthographicSize > 10)
			_camera.orthographicSize = 10;
	}

	void touchMove()
	{
		#if UNITY_ANDROID// && !UNITY_EDITOR
			Touch[] touches = Input.touches;
			if (touches.Length < 1) // if no touch
			{
				if (scrollVelocity != 0.0f) // if velocity
				{
					float t = (Time.time - timeTouchPhaseEnded) / inertiaDuration;
					float frameVelocity = Mathf.Lerp (scrollVelocity, 0.0f, t);
					transform.Translate(scrollDirection.normalized * (frameVelocity * 0.0001f));
					if (t >= 1.0f)
						scrollVelocity = 0.0f;
				}
			}
			else if (touches.Length > 0) // if touch
			{
				if (touches.Length == 1)
				{
					if (touches[0].phase == TouchPhase.Began)
						scrollVelocity = 0.0f;
					else if (touches[0].phase == TouchPhase.Moved) // if drag, calcul velocity
					{
						delta = touches[0].deltaPosition;
						var v = new Vector3(-delta.x, -delta.y, 0);
						var pTemp = transform.position;
						transform.Translate(v * MoveSensitivity); // Move camera
						scrollDirection = v.normalized;
						scrollVelocity = v.magnitude / touches[0].deltaTime;
						if (scrollVelocity <= 1200) // 280
						{
							transform.position = pTemp;
							desiredVelocity = Vector3.zero;
							scrollVelocity = 0;
						}
						else
							shootScript.cameraIsMoved = true;
					}
					else if (touches[0].phase == TouchPhase.Ended)
						timeTouchPhaseEnded = Time.time;
				}
				// else if (touches.Length >= 2)
				// 	zoomInMobile();
			}
		#endif
	}

	//Move in unity editor with arrow
	void standaloneMove()
	{
		#if UNITY_EDITOR || UNITY_STANDALONE
			if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey("d")) //RIGHT
				transform.Translate(new Vector3(1, 0, 0) * MoveSensitivity * 12);
			else if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey("a")) // LEFT
				transform.Translate(new Vector3(-1, 0, 0) * MoveSensitivity * 12);
			else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey("s")) // DOWN
				transform.Translate(new Vector3(0, -1, 0) * MoveSensitivity * 12);
			else if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey("w")) // UP
				transform.Translate(new Vector3(0, 1, 0) * MoveSensitivity * 12);
			if (Input.GetAxis("Mouse ScrollWheel") < 0)
			{
				var pTemp = _camera.transform.position;
				_camera.transform.Translate(new Vector3(0, 0, -perspectiveZoomSpeed * Time.deltaTime));
				if (_camera.transform.position.y > maxZoom)
					_camera.transform.position = pTemp;
			}
			if (Input.GetAxis("Mouse ScrollWheel") > 0)
			{
				var pTemp = _camera.transform.position;
				_camera.transform.Translate(new Vector3(0, 0, perspectiveZoomSpeed * 3 * Time.deltaTime));
				if (_camera.transform.position.y < minZoom)
					_camera.transform.position = pTemp;
			}
		#endif
	}

	void Update()
	{
		return ;
		standaloneMove();
		touchMove();
	}

	void LateUpdate()
	{
		transform.position = new Vector3(
		Mathf.Clamp(transform.position.x, -10, 10), 
		Mathf.Clamp(transform.position.y, 0.95f, 0.95f), transform.position.z);
	}
}
