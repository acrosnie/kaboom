using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class MoveMissile: Photon.MonoBehaviour
{
	public Transform magnetized;
	public bool emp;
	public Vector3 target;
	public List<Vector2> targetList;
	public InfoMissile info;
	public bool isFreeze;
	public float freeze_speed_reduc;
	public float timeAliveFreeze;
	private float t;
	private Vector3 start;
	private InfoUserBattleMode infoUser;

	void Start()
	{
		start = transform.position;
		info = GetComponent<InfoMissile>();
		if (info.draggable)
			Destroy(this.gameObject, 45.0f);
		else
			Destroy(this.gameObject, 90.0f);
		if (GameObject.Find("script"))
			infoUser = GameObject.Find("script").GetComponent<InfoUserBattleMode>();
	}

	void stealth()
	{
		var d1 = Vector2.Distance(start, transform.position);
		float d2 = 0;
		if (info.attInFight)
			d2 = Vector2.Distance(new Vector3(transform.position.x, -4, transform.position.z), transform.position);
		else
			d2 = Vector2.Distance(target, transform.position);
		if (d1 < 3.0f || d2 < 3.0f || emp || magnetized)
		{
			var c = GetComponent<SpriteRenderer>().color;
			GetComponent<SpriteRenderer>().color = new Color(c.r, c.g, c.b, 1);
		}
		else
		{
			var c = GetComponent<SpriteRenderer>().color;
			GetComponent<SpriteRenderer>().color = new Color(c.r, c.g, c.b, 0);
		}
	}

	void Update()
	{
		if (info.name == "Stealth")
			stealth();
		if (emp)
			return ;
		if (infoUser == null)
			return ;
		moveToTarget();
		if (isFreeze)
		{
			t += Time.deltaTime;
			if (t > timeAliveFreeze)
			{
				t = 0;
				isFreeze = false;
			}
		}
	}

	void moveToTarget()
	{
		var speed = info.speed;
		if (isFreeze)
			speed *= freeze_speed_reduc;
		if (targetList != null && targetList.Count >= 1 && magnetized == null)
		{
			myLookAt(targetList[0], 10.0f);
			transform.position = Vector3.MoveTowards(transform.position, targetList[0], speed * Time.deltaTime);
			if ((Vector2)transform.position == targetList[0])
				targetList.RemoveAt(0);
		}
		else if (!info.draggable && transform.position != target)
		{
			var tar = target;
			if (magnetized != null)
			{
				tar = magnetized.position;
				target = (tar - transform.position).normalized * 200;
			}
			myLookAt(tar, 2.5f);
			transform.position = Vector3.MoveTowards(transform.position, tar, speed * Time.deltaTime);
		}
	}

	void myLookAt(Vector3 t, float tt)
	{
		var dir = t - transform.position;
		var angle = (Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg) - 90;
		var neededRotation = Quaternion.AngleAxis(angle, Vector3.forward);
		var interpolatedRotation = Quaternion.Slerp(transform.rotation, neededRotation, Time.deltaTime * tt);
		transform.rotation = interpolatedRotation;
	}

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
	}
}