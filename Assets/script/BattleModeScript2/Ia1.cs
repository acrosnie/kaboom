using UnityEngine;
using System.Collections;

public class Ia1 : MonoBehaviour
{
	public bool attInFight;
	public bool defInFight;
	public Transform areaPrefab;
	public Transform explosionPrefab;
	public float constTimeUpGenerateMissile;
	public float TimeBetweenGenerateMissile;
	public Transform missilePrefab;
	// public MissilesReview[] missilesReview;
	public Transform mainScript;
	public Transform script;
	public bool active;
	private Vector3 start;
	public Transform[] prefabMissileListBattle;

	void Start ()
	{
		mainScript = GameObject.Find("mainScript").transform;
		script = GameObject.Find("script").transform;
		// missilesReview = mainScript.GetComponent<GenerateResource>().missilesReview;
		// missilePrefab = missilesReview[0].missile;
		// missilePrefab.GetComponent<InfoMissile>().explosionPrefab = explosionPrefab;
		// missilePrefab.GetComponent<EffectMissile>().areaPrefab = areaPrefab;
		TimeBetweenGenerateMissile = 5;
		StartCoroutine (generateMissile());
	}

	Vector3 findBuildingToTarget()
	{
		int i;
		Vector3 target;

		target = new Vector3 (-99, -99, -99);
		i = 0;
		while (target == new Vector3(-99, -99, -99))
		{
			var t = GameObject.FindGameObjectsWithTag("tourelle");
			foreach (var t2 in t)
			{
				if (t2.GetComponent<BuildingBattle>().attInFight != attInFight && Random.Range(0, 11) == 0)
				{
					target = t2.transform.position;
					break ;
				}
			}
			if (target == new Vector3(-99, -99, -99))
			{
				var b = GameObject.FindGameObjectsWithTag("building");
				foreach (var b2 in b)
				{
					if (b2.GetComponent<BuildingBattle>().attInFight != attInFight && Random.Range(0, 11) == 0)
					{
						target = b2.transform.position;
						break ;
					}
				}
			}
			i++;
			if (i == 11)
				target = new Vector3 (Random.Range(-8, 8), -5, -4);
		}
		target = new Vector3 (target.x, target.y, -4);
		return (target);
	}

	Transform findMissileTarget()
	{
		GameObject g = null;
		Vector3 target;
		float dist = -1;
		var m = GameObject.FindGameObjectsWithTag("missile");
		var t = GameObject.FindGameObjectsWithTag("tourelle");

		foreach (var t2 in t)
		{
			var pos = t2.transform.position;
			if (t2.GetComponent<BuildingBattle>().attInFight == attInFight)
			{
				foreach (var m2 in m)
				{
					if (m2.GetComponent<InfoMissile>().attInFight != attInFight && (Vector2.Distance(pos, m2.transform.position) < dist || dist == -1))
					{
						start = t2.transform.position;
						dist = Vector2.Distance(pos, m2.transform.position);
						g = m2;
					}
				}
			}
		}
		if (g != null)
			return (g.transform);
		return (null);
	}

	void IaAttack()
	{
		Vector3 target;

		start = new Vector3 (Random.Range(-8, 8), 8.0f, -4);
		target = findBuildingToTarget();
		script.GetComponent<ShootScript>().instantiateMissileShootPublic(start, target, true, false, missilePrefab.GetComponent<InfoMissile>().missileLevelListId);
	}

	void IaDefend()
	{
		Transform missile;
		Transform target;

		missile = findMissileTarget();
		target = findMissileTarget();
		if (target != null)
			script.GetComponent<ShootScript>().instantiateMissileShootPublic(start, target.transform.position, false, true, missilePrefab.GetComponent<InfoMissile>().missileLevelListId);
	}

	void selectRandomMissile()
	{
		missilePrefab = null;
		int j = 0;
		int i = 0;
		int r = 0;

		while (missilePrefab == null)
		{
			i = 0;
			r = Random.Range(0, prefabMissileListBattle.Length);
			foreach (var m in prefabMissileListBattle)
			{
				if (m != null && !m.GetComponent<InfoMissile>().draggable && i == r)
					missilePrefab = m;
				i++;
			}
			j++;
			if (j > 100)
				break ;
		}
	}

	IEnumerator generateMissile()
	{
		while (true)
		{
			if (active == false)
				yield return new WaitForSeconds(5.0f);
			//generate
			prefabMissileListBattle = script.GetComponent<GenerateBattleMode>().prefabMissileListBattle;
			if (prefabMissileListBattle.Length > 0)
				selectRandomMissile();
			if (attInFight && missilePrefab != null)
			{
				IaAttack();
				yield return new WaitForSeconds(Random.Range(1.0f, 15.0f));
			}
			if (defInFight && missilePrefab != null)
			{
				IaDefend();
				yield return new WaitForSeconds(Random.Range(1.0f, 15.0f));
			}
			yield return (null);
		}
	}
}
