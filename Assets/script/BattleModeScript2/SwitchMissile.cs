using UnityEngine;
using System.Collections;

public class SwitchMissile : MonoBehaviour
{
	private ShootScript shootScript;

	void Start()
	{
		shootScript = GameObject.Find("script").GetComponent<ShootScript>();
	}

	public void changeMissile(Transform missile, GameObject button)
	{
		var tourelle = GameObject.FindGameObjectsWithTag("tourelle");
		foreach (var l in tourelle)
		{
			if (l.GetComponent<BuildingBattle>().attInFight == GameObject.Find("script").GetComponent<InfoUserBattleMode>().attInFight)
			{
				if (missile.GetComponent<InfoMissile>().draggable)
					l.GetComponent<BuildingBattle>().tourelleDraggable = true;
				else
					l.GetComponent<BuildingBattle>().tourelleDraggable = false;
			}
		}
		shootScript.missilePrefab = missile;
		shootScript.buttonMissile = button;
		shootScript.cancel = true;
	}
}
