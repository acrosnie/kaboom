using UnityEngine;
using System.Collections;

public class FreezeInfo: Photon.MonoBehaviour
{
	public float freeze_speed_reduc;
	public float timeAlive;
	public bool attInFight;
	public bool defInFight;

	void Start()
	{
		Destroy(this.gameObject, timeAlive);
	}
}