using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class EffectMissile: Photon.MonoBehaviour
{
	public InfoMissile info;
	public InfoUserBattleMode infoUser;
	public MoveMissile move;
	public Transform areaPrefab;
	public Transform bombPrefab;
	public Transform magnetPrefab;
	public GameObject lineEffect;

	void Start()
	{
		infoUser = GameObject.Find("script").GetComponent<InfoUserBattleMode>();
		info = GetComponent<InfoMissile>();
		move = GetComponent<MoveMissile>();
		if (info.freeze)
		{
			lineEffect = new GameObject();
			lineEffect.transform.position = transform.position; 
			lineEffect.AddComponent<LineRenderer>();
			lineEffect.AddComponent<GestionLine>();
			lineEffect.GetComponent<GestionLine>().parentMe = transform;
			lineEffect.GetComponent<GestionLine>().freeze = true;
			lineEffect.GetComponent<GestionLine>().timeAlive = 4;
			lineEffect.GetComponent<GestionLine>().freeze_speed_reduc = info.freeze_speed_reduc;
			lineEffect.GetComponent<GestionLine>().attInFight = info.attInFight;
			lineEffect.GetComponent<GestionLine>().defInFight = info.defInFight;
		}
		if (info.magnet)
		{
			var magnet = Instantiate (magnetPrefab, transform.position, magnetPrefab.transform.rotation) as Transform;
			magnet.parent = transform;
			magnet.localScale = new Vector3(info.magnet_radius * 10, info.magnet_radius * 10, 1);
		}
	}

	[PunRPC]
	void explosiveMax(int i, bool attInFight, int dmg_missiles_frag, int dmg_buildings_frag, float health_frag, float speed_frag, float area_frag, int id1, PhotonPlayer np)
	{
		var max = Instantiate (bombPrefab, transform.position, bombPrefab.transform.rotation) as Transform;
		if (i == 0)
			max.GetComponent<MoveMissile>().target = ((transform.position + new Vector3(-1, 1, 0)) - transform.position).normalized * 1000;
		else if (i == 1)
			max.GetComponent<MoveMissile>().target = ((transform.position + new Vector3(1, 1, 0)) - transform.position).normalized * 1000;
		else if (i == 2)
			max.GetComponent<MoveMissile>().target = ((transform.position + new Vector3(-1, -1, 0)) - transform.position).normalized * 1000;
		else if (i == 3)
			max.GetComponent<MoveMissile>().target = ((transform.position + new Vector3(1, -1, 0)) - transform.position).normalized * 1000;
		max.GetComponent<InfoMissile>().damageMissile = dmg_missiles_frag;
		max.GetComponent<InfoMissile>().damageBuilding = dmg_buildings_frag;
		max.GetComponent<InfoMissile>().health = (int)health_frag;
		max.GetComponent<InfoMissile>().speed = speed_frag;
		max.GetComponent<InfoMissile>().area = area_frag;
		max.GetComponent<InfoMissile>().attInFight = attInFight;
		max.GetComponent<InfoMissile>().defInFight = !attInFight;
		PhotonView[] nViews = max.GetComponentsInChildren<PhotonView>();
		nViews[0].viewID = id1;
		max.GetComponent<InfoMissile>().uniqueId = id1;
	}

	[PunRPC]
	public void explode(bool r = false)
	{
		if (!r && !infoUser.ia)
		{
			photonView.RPC("explode", PhotonTargets.All, true);
			return ;
		}
		var b = Instantiate (info.explosionPrefab, new Vector3(transform.position.x, transform.position.y, -10), info.explosionPrefab.rotation) as Transform;
		b.GetComponent<ParticleSystem>().startSize = info.area * 4.7f;
		b.GetComponent<ParticleSystem>().loop = false;
		b.GetComponent<ParticleSystem>().startLifetime = info.timeExplosion;
		var explosionArea = Instantiate (areaPrefab, transform.position, areaPrefab.transform.rotation) as Transform;
		explosionArea.position = new Vector3(explosionArea.position.x, explosionArea.position.y, -10);
		explosionArea.GetComponent<ExplosionAreaBattle>().damageMissile = info.damageMissile;
		explosionArea.GetComponent<ExplosionAreaBattle>().damageBuilding = info.damageBuilding;
		explosionArea.GetComponent<ExplosionAreaBattle>().area = info.area;
		explosionArea.GetComponent<ExplosionAreaBattle>().timeLife = info.timeExplosion;
		explosionArea.GetComponent<ExplosionAreaBattle>().attInFight = info.attInFight;
		explosionArea.GetComponent<ExplosionAreaBattle>().defInFight = info.defInFight;
		if (info.frag)
		{
			for (int i = 0; i < info.nb_frag; i++)
			{
				int id1 = PhotonNetwork.AllocateViewID();
				PhotonView photonView = GetComponent<PhotonView>();
				photonView.RPC("explosiveMax", PhotonTargets.All, i, info.attInFight, info.dmg_missiles_frag, info.dmg_buildings_frag, info.health_frag, info.speed_frag, info.area_frag, id1, PhotonNetwork.player);
			}
		}
		Destroy(transform.gameObject);
	}

	[PunRPC]
	void effectExplosionArea(int damageMissile, bool r = false)
	{
		if (!r && !infoUser.ia)
		{
			photonView.RPC("effectExplosionArea", PhotonTargets.All, damageMissile, true);
			return ;
		}
		info.health -= damageMissile;
		if (info.health <= 0)
			explode();
	}

	[PunRPC]
	void freeze(float timeAlive, float freeze_speed_reduc, bool r = false)
	{
		if (!r && !infoUser.ia)
		{
			photonView.RPC("freeze", PhotonTargets.All, timeAlive, freeze_speed_reduc, true);
			return ;
		}
		GetComponent<MoveMissile>().isFreeze = true;
		GetComponent<MoveMissile>().timeAliveFreeze = timeAlive;
		GetComponent<MoveMissile>().freeze_speed_reduc = freeze_speed_reduc;
	}

	[PunRPC]
	void removeFreeze(bool r = false)
	{
		if (!r && !infoUser.ia)
		{
			photonView.RPC("removeFreeze", PhotonTargets.All, true);
			return ;
		}
		GetComponent<MoveMissile>().isFreeze = false;
	}

	void explodeInPoint()
	{
		if (transform.position == move.target)
			explode();
	}

	void Update()
	{
		if (info != null && info.explodeInPoint && (infoUser.host || infoUser.ia))
			explodeInPoint();
	}

	Transform findMissile(int id)
	{
		var m = GameObject.FindGameObjectsWithTag("missile");

		foreach (var m2 in m)
		{
			if (m2 != null && m2.GetComponent<InfoMissile>() && m2.GetComponent<InfoMissile>().uniqueId == id)
				return (m2.transform);
		}
		return (null);
	}

	[PunRPC]
	void magnetize (int id, bool r = false)
	{
		if (!r && !infoUser.ia)
		{
			photonView.RPC("magnetize", PhotonTargets.All, id, true);
			return ;
		}
		GetComponent<MoveMissile>().magnetized = findMissile(id);
	}

	[PunRPC]
	void unMagnetize (int id, bool r = false)
	{
		if (!r && !infoUser.ia)
		{
			photonView.RPC("magnetize", PhotonTargets.All, id, true);
			return ;
		}
		GetComponent<MoveMissile>().magnetized = null;
	}

	void OnTriggerEnter2D(Collider2D other)
	{	
		if (other.tag == "magnet" && info.attInFight != other.transform.parent.GetComponent<InfoMissile>().attInFight && (infoUser.host || infoUser.ia))
			magnetize(other.transform.parent.GetComponent<InfoMissile>().uniqueId);
		else if (other.tag == "missileCollider" && info.attInFight && info.attInFight != other.transform.parent.GetComponent<InfoMissile>().attInFight && (infoUser.host || infoUser.ia))
		{
			if (info.damageMissile > other.transform.parent.GetComponent<InfoMissile>().health)
				other.transform.parent.GetComponent<EffectMissile>().explode();
			else if (info.health < other.transform.parent.GetComponent<InfoMissile>().health)
				explode ();
			else if (info.health > other.transform.parent.GetComponent<InfoMissile>().health)
				other.transform.parent.GetComponent<EffectMissile>().explode();
			else
			{
				other.transform.parent.GetComponent<EffectMissile>().explode();
				explode();
			}
		}
		else if (other.tag == "explosion" && info.attInFight && info.attInFight != other.transform.GetComponent<ExplosionAreaBattle>().attInFight && (infoUser.host || infoUser.ia))
			effectExplosionArea(other.transform.GetComponent<ExplosionAreaBattle>().damageMissile);
		else if (other.name == "freezeCollider" && info.attInFight != other.GetComponent<FreezeInfo>().attInFight && (infoUser.host || infoUser.ia))
			freeze(other.GetComponent<FreezeInfo>().timeAlive, other.GetComponent<FreezeInfo>().freeze_speed_reduc);
		else if (other.name == "fireCollider" && info.attInFight != other.GetComponent<FreezeInfo>().attInFight)
			explode();
		else if ((other.tag == "tourelle" || other.tag == "building") && other.GetComponent<BuildingBattle>().attInFight != info.attInFight && (infoUser.host || infoUser.ia))
			explode();
		else if (other.tag == "shield" && other.transform.parent.GetComponent<BuildingBattle>().attInFight != info.attInFight && (infoUser.host || infoUser.ia))
			explode();
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.name == "freezeCollider" && info.attInFight != other.GetComponent<FreezeInfo>().attInFight && (infoUser.host || infoUser.ia))
			removeFreeze();
		else if (other.tag == "magnet" && info.attInFight != other.transform.parent.GetComponent<InfoMissile>().attInFight && (infoUser.host || infoUser.ia))
			unMagnetize(other.transform.parent.GetComponent<InfoMissile>().uniqueId);
	}
}