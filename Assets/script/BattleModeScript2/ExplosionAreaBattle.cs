using UnityEngine;
using System.Collections;

public class ExplosionAreaBattle : Photon.MonoBehaviour
{
	public float area;
	public int damageMissile;
	public int damageBuilding;
	public float timeLife;
	public bool  attInFight;
	public bool  defInFight;
	public bool	 frileux;
	private float areaTmp = 0;
	private InfoUserBattleMode infoUser;

	void Start()
	{
		infoUser = GameObject.Find("script").GetComponent<InfoUserBattleMode>();
		StartCoroutine(timehealthArea());
		transform.localScale = new Vector3(0, 0, 0);
	}

    IEnumerator timehealthArea()
    {
        yield return new WaitForSeconds(timeLife);
        Destroy(gameObject);
	}

	// void effectExplosion(bool r = false)
	// {
	// 	if (!r)
	// 	{
	// 		photonView.RPC("effectExplosion", PhotonTargets.AllBufferedViaServer, true);
	// 		return ;
	// 	}
	// 	other.GetComponent<InfoMissile>().health -= damageMissile;
	// 	if (other.GetComponent<InfoMissile>().health <= 0)
	// 		other.GetComponent<EffectMissile>().explode();
	// }

	// void OnTriggerEnter2D(Collider2D other)
	// {	
	// 	if (other.tag == "missile" && attInFight != other.GetComponent<InfoMissile>().attInFight && (infoUser.host || infoUser.ia))
	// 	{
	// 		effectExplosion();
	// 	}
	// }

    void Update()
    {
    	areaTmp += 5.0f * Time.deltaTime;
    	if (areaTmp > area)
    		areaTmp = area;
    	transform.localScale = new Vector3(areaTmp, areaTmp, areaTmp);
    }
}
