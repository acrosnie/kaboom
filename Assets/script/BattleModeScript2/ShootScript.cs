using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.EventSystems;
using System.Text;

public class ShootScript : Photon.MonoBehaviour
{
	public GameObject buttonMissile;
	public Transform missilePrefab;
	public bool cameraIsMoved;
	public Transform arrowDir;
	public Transform targetPrefab;
	public MissilesReview[] missilesReview;
	public GameObject mainScript;
	public bool cancel;
	private List<Vector2> lineDraggable;
	private List<Vector2> lineDraggableDraw;
	private float timeCountAddDraggable;
	private bool goDrag;
	private LineRenderer lineDraw;
	private float tRemoveLineDraw;
	private InfoUserBattleMode infoUserBattleMode;
	private Vector2 pos1;
	private Vector2 pos2;
	private string lineDraggableString;

	void Awake()
	{
		lineDraw = gameObject.AddComponent<LineRenderer>();
		lineDraw.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
		lineDraw.SetWidth(0.2f, 0.2f);
		lineDraw.SetColors(Color.grey, Color.grey);
		lineDraw.useWorldSpace = true;
	}

	void Start()
	{
		mainScript = GameObject.Find("mainScript");
		missilesReview = mainScript.GetComponent<GenerateResource>().missilesReview;
		//missilePrefab = missilesReview[0].missile;
		infoUserBattleMode = GetComponent<InfoUserBattleMode>();
	}

	GameObject findCloseTourelle(Vector2 pos, bool att)
	{
		if (pos == new Vector2(-99, -99))
			return (null);
		GameObject g = null;
		var t = GameObject.FindGameObjectsWithTag("tourelle");
		float dist = -1;

		foreach (var t2 in t)
		{
			if (t2.GetComponent<BuildingBattle>().attInFight == att && (Vector2.Distance(pos, t2.transform.position) < dist || dist == -1))
			{
				dist = Vector2.Distance(pos, t2.transform.position);
				g = t2;
			}
		}
		return (g);
	}

	void shootAndroid()
	{
		if (EventSystem.current.IsPointerOverGameObject())
			return ;
		if (missilePrefab.GetComponent<InfoMissile>().draggable)
		{
			draggableAndroid();
			return ;
		}
		Touch[] touches = Input.touches;
		if (infoUserBattleMode.defInFight)
		{
			if (touches.Length > 0 && touches.Length == 1)
			{	
				if (touches[0].phase == TouchPhase.Began)
					cameraIsMoved = false;
				else if (touches[0].phase == TouchPhase.Ended && !cameraIsMoved && buttonMissile.GetComponent<ButtonCooldown>().ready)
				{
					if (cancel)
					{
						cancel = false;
						return ;
					}
					if (missilePrefab.GetComponent<InfoMissile>().draggable)
						return ;
						var posMouse = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
					if (mouseOnObject(posMouse, "building") || mouseOnObject(posMouse, "tourelle"))
						return ;
					var start = findCloseTourelle((Vector2)posMouse, false).transform.position;
					int id1 = PhotonNetwork.AllocateViewID();
					PhotonView photonView = this.GetComponent<PhotonView>();
					photonView.RPC("instantiateMissileShoot", PhotonTargets.All, start, (Vector2)posMouse,
									infoUserBattleMode.attInFight, infoUserBattleMode.defInFight, missilePrefab.GetComponent<InfoMissile>().missileLevelListId, id1, PhotonNetwork.player);
				}
			}
		}
		else if (infoUserBattleMode.attInFight)
		{
			if (touches.Length > 0 && touches.Length == 1)
			{
				if (touches[0].phase == TouchPhase.Ended)
					arrowDir.gameObject.SetActive(false);
				if (touches[0].phase == TouchPhase.Began)
				{
					pos1 = (Vector2)Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
					pos2 = pos1;
					cameraIsMoved = false;
				}
				else if (touches[0].phase == TouchPhase.Moved)
				{
					arrowDir.GetComponent<SpriteRenderer>().color = Color.green - new Color(0, 0, 0, 0.6f);
					pos2 = (Vector2)Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
					if (pos2 != pos1 && pos1.y > 1.0f)
					{
						arrowDir.gameObject.SetActive(true);
						var v2 = pos2 - pos1;
						arrowDir.position = pos1 + (v2) / 2.0f;
						arrowDir.localScale = new Vector3(arrowDir.localScale.x, v2.magnitude / 2.0f, arrowDir.localScale.z);
						arrowDir.rotation = Quaternion.FromToRotation(Vector2.up, v2);
						var d = Vector2.Distance(pos1, pos2);
						var a = Vector2.Angle((pos1 - pos2).normalized, Vector2.up);
						if (d < 4.0f || pos1.y < pos2.y || a < 0 || a > 70 && pos1.y > 1.0f)
							arrowDir.GetComponent<SpriteRenderer>().color = Color.red - new Color(0, 0, 0, 0.7f);
					}
				}
				else if (touches[0].phase == TouchPhase.Ended && !cameraIsMoved && buttonMissile.GetComponent<ButtonCooldown>().ready)
				{
					var a = Vector2.Angle((pos1 - pos2).normalized, Vector2.up);
					var d = Vector2.Distance(pos1, pos2);
					if (pos1.y > pos2.y && d > 4.0f && a >= 0 && a <= 70)
					{
						var start = findStartPosDragAtt(pos1, pos2);
						int id1 = PhotonNetwork.AllocateViewID();
						PhotonView photonView = this.GetComponent<PhotonView>();
						photonView.RPC("instantiateMissileShoot", PhotonTargets.All, start, pos2, infoUserBattleMode.attInFight, infoUserBattleMode.defInFight, missilePrefab.GetComponent<InfoMissile>().missileLevelListId, id1, PhotonNetwork.player);
					}
				}
			}
		}
	}

	void draggableAndroid()
	{
		if (EventSystem.current.IsPointerOverGameObject())
			return ;
		Touch[] touches = Input.touches;
		if (touches.Length > 0 && touches.Length == 1)
		{
			if (!buttonMissile.GetComponent<ButtonCooldown>().ready)
				return ;
			var mousePos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
			if (touches[0].phase == TouchPhase.Began)
			{
				lineDraggable = new List<Vector2>();
				if ((mouseOnObject(mousePos, "tourelle") && infoUserBattleMode.defInFight) || (infoUserBattleMode.attInFight && mousePos.y > 1.0f))
				{
					lineDraggable.Add(new Vector2(mousePos.x, mousePos.y));
					lineDraw.SetVertexCount(lineDraggable.Count);
					lineDraw.SetPosition(0, (Vector3)lineDraggable[0]);
					goDrag = true;
				}
			}
			else if (touches[0].phase == TouchPhase.Moved && goDrag)
			{
				lineDraggable.Add(new Vector2(mousePos.x, mousePos.y));
				lineDraw.SetVertexCount(lineDraggable.Count);
				lineDraw.SetPosition(lineDraggable.Count - 1, (Vector3)lineDraggable[lineDraggable.Count - 1]);
			}
			else if (touches[0].phase == TouchPhase.Ended && goDrag)
			{
				lineDraw.SetVertexCount(0);
				StringBuilder buildListString = new StringBuilder();
				foreach (Vector2 l in lineDraggable)
				{
					// Append each Vector2 to the StringBuilder overload.
					buildListString.Append(l).Append(",");
				}
				string resultStringList = buildListString.ToString();
				var start = (infoUserBattleMode.attInFight) ? findStartPosDragAtt(lineDraggable[0], lineDraggable[lineDraggable.Count - 1]) : findCloseTourelle(lineDraggable[0], false).transform.position;
				int id1 = PhotonNetwork.AllocateViewID();
				PhotonView photonView = this.GetComponent<PhotonView>();
				photonView.RPC("instantiateMissiledraggable", PhotonTargets.All, resultStringList, start, lineDraggable[lineDraggable.Count - 1], lineDraggable[lineDraggable.Count - 2], infoUserBattleMode.attInFight, infoUserBattleMode.defInFight,
													missilePrefab.GetComponent<InfoMissile>().missileLevelListId, id1, PhotonNetwork.player);
			}
		}
	}

	void draggableStandalone()
	{
		if (!buttonMissile.GetComponent<ButtonCooldown>().ready || EventSystem.current.IsPointerOverGameObject())
			return ;
		var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		if (Input.GetMouseButtonDown(0))
		{
			if ((mouseOnObject(mousePos, "tourelle") && infoUserBattleMode.defInFight) || (infoUserBattleMode.attInFight && mousePos.y > 1.0f))
			{
				lineDraggableDraw = new List<Vector2>();
				lineDraggable = new List<Vector2>();		
				lineDraggable.Add(new Vector2(mousePos.x, mousePos.y));
				lineDraggableDraw.Add(new Vector2(mousePos.x, mousePos.y));
				lineDraw.SetVertexCount(lineDraggable.Count);
				lineDraw.SetPosition(0, (Vector3)lineDraggable[0]);
				goDrag = true;
			}
		}
		else if (Input.GetMouseButton(0) && goDrag)
		{
			lineDraggableDraw.Add(new Vector2(mousePos.x, mousePos.y));
			timeCountAddDraggable += Time.deltaTime;
			var d = Vector2.Distance(lineDraggable[lineDraggable.Count - 1], (Vector2)mousePos);
			if (d > 0.4f && timeCountAddDraggable > 0.3f)
			{
				timeCountAddDraggable = 0;
				lineDraggable.Add(new Vector2(mousePos.x, mousePos.y));
			}
			lineDraw.SetVertexCount(lineDraggableDraw.Count);
			lineDraw.SetPosition(lineDraggableDraw.Count - 1, (Vector3)lineDraggableDraw[lineDraggableDraw.Count - 1]);
		}
		else if (Input.GetMouseButtonUp(0) && goDrag && lineDraggable.Count >= 3)
		{
			lineDraw.SetVertexCount(0);
			StringBuilder buildListString = new StringBuilder();
			foreach (Vector2 l in lineDraggable)
			{
				// Append each Vector2 to the StringBuilder overload.
				buildListString.Append(l).Append(",");
			}
			string resultStringList = buildListString.ToString();
			var start = (infoUserBattleMode.attInFight) ? findStartPosDragAtt(lineDraggable[0], lineDraggable[lineDraggable.Count - 1]) : findCloseTourelle(lineDraggable[0], false).transform.position;
			int id1 = PhotonNetwork.AllocateViewID();
			PhotonView photonView = this.GetComponent<PhotonView>();
			photonView.RPC("instantiateMissiledraggable", PhotonTargets.All, resultStringList, start, lineDraggable[lineDraggable.Count - 1], lineDraggable[lineDraggable.Count - 2], infoUserBattleMode.attInFight, infoUserBattleMode.defInFight,
												missilePrefab.GetComponent<InfoMissile>().missileLevelListId, id1, PhotonNetwork.player);
		}
	}

	void shootStandalone()
	{
		if (Input.GetMouseButtonUp(0))
			arrowDir.gameObject.SetActive(false);
		if (missilePrefab.GetComponent<InfoMissile>().draggable)
		{
			draggableStandalone();
			return ;
		}
		if (infoUserBattleMode.defInFight)
		{
			if (Input.GetMouseButtonDown(0))
				cameraIsMoved = false;
			if (Input.GetMouseButtonUp(0) && !EventSystem.current.IsPointerOverGameObject() && buttonMissile.GetComponent<ButtonCooldown>().ready)
			{
				if (!cameraIsMoved)
				{
					if (mouseOnObject(Camera.main.ScreenToWorldPoint(Input.mousePosition), "building") || mouseOnObject(Camera.main.ScreenToWorldPoint(Input.mousePosition), "tourelle"))
						return ;
					var start = findCloseTourelle((Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition), false).transform.position;
					int id1 = PhotonNetwork.AllocateViewID();
					PhotonView photonView = this.GetComponent<PhotonView>();
					photonView.RPC("instantiateMissileShoot", PhotonTargets.All, start, (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition),
						infoUserBattleMode.attInFight, infoUserBattleMode.defInFight, missilePrefab.GetComponent<InfoMissile>().missileLevelListId, id1, PhotonNetwork.player);
				}
			}
		}
		else if (infoUserBattleMode.attInFight)
		{
			if (Input.GetMouseButtonDown(0))
			{
				pos1 = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
				pos2 = pos1;
				cameraIsMoved = false;
			}
			else if (Input.GetMouseButton(0))
			{
				arrowDir.GetComponent<SpriteRenderer>().color = Color.green - new Color(0, 0, 0, 0.6f);
				pos2 = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
				if (pos2 != pos1 && pos1.y > 1.0f)
				{
					arrowDir.gameObject.SetActive(true);
					var v2 = pos2 - pos1;
					arrowDir.position = pos1 + (v2) / 2.0f;
					arrowDir.localScale = new Vector3(arrowDir.localScale.x, v2.magnitude / 2.0f, arrowDir.localScale.z);
					arrowDir.rotation = Quaternion.FromToRotation(Vector2.up, v2);
					var d = Vector2.Distance(pos1, pos2);
					var a = Vector2.Angle((pos1 - pos2).normalized, Vector2.up);
					if (d < 4.0f || pos1.y < pos2.y || a < 0 || a > 70)
						arrowDir.GetComponent<SpriteRenderer>().color = Color.red - new Color(0, 0, 0, 0.7f);
				}
			}
			else if (Input.GetMouseButtonUp(0) && !EventSystem.current.IsPointerOverGameObject() && buttonMissile.GetComponent<ButtonCooldown>().ready)
			{
				var a = Vector2.Angle((pos1 - pos2).normalized, Vector2.up);
				var d = Vector2.Distance(pos1, pos2);
				if (!cameraIsMoved && pos1.y > pos2.y && d > 4.0f && a >= 0 && a <= 70 && pos1.y > 1.0f)
				{
					var start = findStartPosDragAtt(pos1, pos2);
					int id1 = PhotonNetwork.AllocateViewID();
					PhotonView photonView = this.GetComponent<PhotonView>();
					photonView.RPC("instantiateMissileShoot", PhotonTargets.All, start, pos2, infoUserBattleMode.attInFight, infoUserBattleMode.defInFight,
															missilePrefab.GetComponent<InfoMissile>().missileLevelListId, id1, PhotonNetwork.player);
				}
			}
		}
	}

	bool mouseOnObject(Vector3 mousePos, string tag)
	{
		Collider2D[] col = Physics2D.OverlapPointAll(mousePos);
		if (col.Length > 0)
		{
			foreach (Collider2D cc in col)
			{
				if (cc.tag == tag)
					return (true);
			}
		}
		return (false);
	}

	void removeLineDraw()
	{
		if (!goDrag && lineDraggable != null && lineDraggable.Count > 0)
		{
			// tRemoveLineDraw += Time.deltaTime;
			// // if (tRemoveLineDraw < 0.2f)
			// // 	return ;
			// tRemoveLineDraw = 0;
			// for (int i = 0; i < lineDraggable.Count; i++)
			// {
			// 	if (i + 1 < lineDraggable.Count)
			// 		lineDraw.SetPosition(i, lineDraggable[i + 1]);
			// }
			// lineDraggable.RemoveAt(0);
			lineDraw.SetVertexCount(0);
		}
	}

	[PunRPC]
	void instantiateMissiledraggable(string resultStringList, Vector3 start, Vector2 end, Vector2 beforeEnd, bool att, bool def, int missileLevelListId, int id1, PhotonPlayer np)
	{
		lineDraggableString = resultStringList;
		string[] parts = lineDraggableString.Split(',');
		List<string> list = new List<string>(parts);
		int i = 0;
		int n = 0;

		lineDraggable = new List<Vector2>();
		foreach (string item in parts)
		{
			string tmp = item;
			tmp = tmp.Replace("(", "");
			tmp = tmp.Replace(")", "");
			if (tmp != "")
			{
				if (n % 2 == 0) // pair
					lineDraggable.Add(new Vector2(float.Parse(tmp), 0));
				else
				{
					lineDraggable[i] += new Vector2(0, float.Parse(tmp));
					i++;
				}
			}
			n++;
		}
		goDrag = false;
		var missile = selectMissileById(missileLevelListId);
		if ((start.y < 7.0f || start.y > 15.0f) && att)
		{
			print (start);
			return ;
		}
		var missileSelected = Instantiate(missile, start, missile.rotation) as Transform; // create missile
		if (lineDraggable != null && lineDraggable.Count > 0)
		{
			lineDraggable[0] = start;
			missileSelected.GetComponent<MoveMissile>().target = (end - beforeEnd).normalized * 1000;
			lineDraggable.Add(missileSelected.GetComponent<MoveMissile>().target);
			missileSelected.GetComponent<MoveMissile>().targetList = lineDraggable;
		}
		missileSelected.gameObject.SetActive(true);
		missileSelected.GetComponent<InfoMissile>().attInFight = att;
		missileSelected.GetComponent<InfoMissile>().defInFight = def;
		if (infoUserBattleMode.attInFight == att)
			buttonMissile.GetComponent<ButtonCooldown>().startCoolDown();
		PhotonView[] nViews = missileSelected.GetComponentsInChildren<PhotonView>();
		nViews[0].viewID = id1;
		missileSelected.GetComponent<InfoMissile>().uniqueId = id1;
	}

	Transform selectMissileById(int missileLevelListId)
	{
		foreach (var m in missilesReview)
		{
			if (m.missile.GetComponent<InfoMissile>().missileLevelListId == missileLevelListId)
				return (m.missile);
		}
		return (null);
	}

	Vector3 findStartPosDragAtt(Vector2 p1, Vector2 p2)
	{
		var n = (p1 - p2).normalized;
		var v = p1;
		int i = 0;

		while (v.y < 7.3f)
		{
			v += n;
			i++;
			if (i > 1000)
				break ;
		}
		return (v);
	}

	public void instantiateMissileShootPublic(Vector3 start, Vector2 final, bool att, bool def, int missileLevelListId)
	{
		int id1 = PhotonNetwork.AllocateViewID();
		PhotonView photonView = this.GetComponent<PhotonView>();
		photonView.RPC("instantiateMissileShoot", PhotonTargets.All, start, final, att, def, missileLevelListId, id1, PhotonNetwork.player);
	}

	[PunRPC]
	void instantiateMissileShoot(Vector3 start, Vector2 final, bool att, bool def, int missileLevelListId, int id1, PhotonPlayer np)
	{
		Transform targetIcone;
		var missile = selectMissileById(missileLevelListId);

		if ((start.y < 7.0f || start.y > 9.0f) && att)
			return ;
		var missileSelected = Instantiate(missile, start, missile.rotation) as Transform; // create missile
		if (def)
		{
			targetIcone = Instantiate(targetPrefab, final, targetPrefab.rotation) as Transform; // create targetIcone
			missileSelected.GetComponent<MoveMissile>().target = final;
		}
		if (att)
			missileSelected.GetComponent<MoveMissile>().target = (final - (Vector2)start).normalized * 1000;
		missileSelected.gameObject.SetActive(true);
		missileSelected.GetComponent<InfoMissile>().attInFight = att;
		missileSelected.GetComponent<InfoMissile>().defInFight = def;
		if (infoUserBattleMode.attInFight == att)
			buttonMissile.GetComponent<ButtonCooldown>().startCoolDown();
		PhotonView[] nViews = missileSelected.GetComponentsInChildren<PhotonView>();
		nViews[0].viewID = id1;
		missileSelected.GetComponent<InfoMissile>().uniqueId = id1;
	}

	void Update()
	{
		removeLineDraw();
		if (buttonMissile == null || missilePrefab == null)
			return ;
		#if UNITY_ANDROID && !UNITY_EDITOR
			shootAndroid();
		#endif
		#if UNITY_EDITOR
			shootStandalone();
		#endif
	}
}