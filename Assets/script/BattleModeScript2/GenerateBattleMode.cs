using UnityEngine;
using System.Collections;
using SimpleJSON;
using UnityEngine.UI;
using System.IO;
using System;

public class GenerateBattleMode : Photon.MonoBehaviour
{
	public Transform bombPrefab;
	public Transform magnetPrefab;
	public Transform areaPrefab;
	public Transform explosionPrefab;
	public Transform buttonMissile;
	public Transform panelButtonMissile;
	public GameObject mainScript;
	public MissilesReview[] missilesReview;
	private Transform[] prefabBuildingListBattle;
	public Transform[] prefabMissileListBattle;
	public bool start;
	public Transform loading;

	void Start()
	{
		mainScript = GameObject.Find("mainScript");
		mainScript.GetComponent<GenerateResource>().startGenerate(true);
		prefabBuildingListBattle = mainScript.GetComponent<GenerateResource>().prefabBuildingListBattle;
	}

	[PunRPC]
	void instantiateBuilding(float decallageXLeft, string effect, int id, int i, int id1, PhotonPlayer np)
	{
		var b = searchBuilding(id);
		var v = Instantiate(b, new Vector3(-8.779f, -4.294f, b.position.z), b.rotation) as Transform;
		v.gameObject.SetActive(true);
		v.localScale *= 0.4f;
		v.position += new Vector3(decallageXLeft, 0, 0);
		if (effect == "Fire")
		{
			v.GetComponent<BuildingBattle>().tourelle = true;
			v.gameObject.tag = "tourelle";
		}
		else if (effect == "Shield" && GetComponent<InfoUserBattleMode>().defInFight)
		{
			var buildingPlayerListJson = mainScript.GetComponent<GenerateResource>().buildingPlayerListJson;
			v.GetComponent<BuildingBattle>().shield = true;
			v.GetComponent<BuildingBattle>().numberShield = buildingPlayerListJson[i]["building_level_list"]["building_effect"]["number_available"].AsInt;
			v.GetComponent<BuildingBattle>().cooldownShield = buildingPlayerListJson[i]["building_level_list"]["building_effect"]["cooldown"].AsFloat * 3600;
			v.GetComponent<BuildingBattle>().healthShield = buildingPlayerListJson[i]["building_level_list"]["building_effect"]["health_ability"].AsFloat;
			v.GetComponent<BuildingBattle>().healthMaxShield = buildingPlayerListJson[i]["building_level_list"]["building_effect"]["health_ability"].AsFloat;
			v.GetComponent<BuildingBattle>().durationShield = buildingPlayerListJson[i]["building_level_list"]["building_effect"]["duration"].AsFloat * 3600;
		}
		else if (effect == "EMP" && GetComponent<InfoUserBattleMode>().defInFight)
		{
			var buildingPlayerListJson = mainScript.GetComponent<GenerateResource>().buildingPlayerListJson;
			v.GetComponent<BuildingBattle>().emp = true;
			v.GetComponent<BuildingBattle>().numberEmp = buildingPlayerListJson[i]["building_level_list"]["building_effect"]["number_available"].AsInt;
			v.GetComponent<BuildingBattle>().cooldownEmp = buildingPlayerListJson[i]["building_level_list"]["building_effect"]["cooldown"].AsFloat * 3600;
		}
		PhotonView[] nViews = v.GetComponentsInChildren<PhotonView>();
		nViews[0].viewID = id1;
	}

	Transform searchBuilding(int id)
	{
		foreach (var b in prefabBuildingListBattle)
		{
			if (id == b.GetComponent<BuildingBattle>().buildingLevelListId)
				return (b);
		}
		return (null);
	}

	void generateBuildingDefense()
	{
		if (GetComponent<InfoUserBattleMode>().attInFight)
		{
			// do ?
		}
		else
		{
			var buildingPlayerListJson = mainScript.GetComponent<GenerateResource>().buildingPlayerListJson;
			for (int i = 0; i < buildingPlayerListJson.Count; i++)
			{
				if (buildingPlayerListJson[i]["building_level_list"]["def_line"].AsInt > 0)
				{
					foreach (var b in prefabBuildingListBattle)
					{
						if (buildingPlayerListJson[i]["building_level_list"]["id"].AsInt == b.GetComponent<BuildingBattle>().buildingLevelListId)
						{
							int id1 = PhotonNetwork.AllocateViewID();
							PhotonView photonView = this.GetComponent<PhotonView>();
							photonView.RPC("instantiateBuilding", PhotonTargets.All, (int)buildingPlayerListJson[i]["distance_fromleft"].AsFloat * 0.4f, (string)buildingPlayerListJson[i]["building_level_list"]["building_effect"]["effect"], buildingPlayerListJson[i]["building_level_list"]["id"].AsInt, i, id1, PhotonNetwork.player);
						}
					}
				}
			}
		}
	}

	void generateUiButtonMissile()
	{
		int i = 0;

		foreach (var m in missilesReview)
		{
			if (m.missile != null)
			{
				m.missile.GetComponent<InfoMissile>().explosionPrefab = explosionPrefab;
				m.missile.GetComponent<EffectMissile>().areaPrefab = areaPrefab;
				m.missile.GetComponent<EffectMissile>().magnetPrefab = magnetPrefab;
				m.missile.GetComponent<EffectMissile>().bombPrefab = bombPrefab;
				if ((m.equipedAtt && GetComponent<InfoUserBattleMode>().attInFight) || (GetComponent<InfoUserBattleMode>().defInFight && m.equipedDef))
				{
					var b = Instantiate(buttonMissile, buttonMissile.position, buttonMissile.rotation) as Transform;
					b.gameObject.SetActive(true);
					b.position = new Vector3(b.position.x + (100 * i), b.position.y, b.position.z);
					b.GetComponent<Image>().sprite = m.missile.GetComponent<InfoMissile>().icone;
					m.missile.GetComponent<InfoMissile>().attInFight = GetComponent<InfoUserBattleMode>().attInFight;
					m.missile.GetComponent<InfoMissile>().defInFight = GetComponent<InfoUserBattleMode>().defInFight;
					b.parent = panelButtonMissile;
					var missile = m.missile;
					var button = b.gameObject;
					b.GetComponent<ButtonCooldown>().coolDown = m.missile.GetComponent<InfoMissile>().coolDown;
					prefabMissileListBattle[i] = missile;
					b.GetComponent<Button>().onClick.AddListener(delegate{ GetComponent<SwitchMissile>().changeMissile(missile, button); });
					ColorBlock cb = b.GetComponent<Button>().colors;
					cb.normalColor = new Color(0.4f, 0.4f, 0.4f, 0.6f);
					cb.pressedColor = new Color(0.2f, 0.5f, 0.8f, 1.0f);
					b.GetComponent<Button>().colors = cb;
					i++;
				}
			}
		}
	}

	void Update()
	{
		if (start)
		{
			missilesReview = mainScript.GetComponent<GenerateResource>().missilesReview;
			prefabMissileListBattle = new Transform[missilesReview.Length];
			generateUiButtonMissile();
			if (GameObject.Find("network").GetComponent<NetworkConnect>().ia)
			{
				GetComponent<Ia1>().active = true;
				GetComponent<InfoUserBattleMode>().ia = true;
				GetComponent<InfoUserBattleMode>().attInFight = false;//(UnityEngine.Random.Range(0, 1) == 0) ? true : false;
				GetComponent<InfoUserBattleMode>().defInFight = !GetComponent<InfoUserBattleMode>().attInFight;
				GetComponent<Ia1>().attInFight = !GetComponent<InfoUserBattleMode>().attInFight;
				GetComponent<Ia1>().defInFight = !GetComponent<InfoUserBattleMode>().defInFight;
			}
			else
			{
				GetComponent<InfoUserBattleMode>().attInFight = GameObject.Find("network").GetComponent<NetworkConnect>().attInFight;
				GetComponent<InfoUserBattleMode>().defInFight = GameObject.Find("network").GetComponent<NetworkConnect>().defInFight;
				GetComponent<InfoUserBattleMode>().host = GameObject.Find("network").GetComponent<NetworkConnect>().host;
			}
			if (GetComponent<InfoUserBattleMode>().defInFight)
				generateBuildingDefense();
			start = false;
			loading.gameObject.SetActive(false);
		}
	}
}
