using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BuildingBattle : Photon.MonoBehaviour
{
	public InfoUserBattleMode infoUser;
	public int buildingPlayerId;
	public int buildingLevelListId;
	public Transform explosionPrefab;
	public Transform slider;
	public bool attInFight;
	public bool defInFight;
	public bool tourelle;
	public bool tourelleDraggable;
	public bool empReady;
	public bool emp;
	public float cooldownEmp;
	public int numberEmp;
	public bool shield;
	public bool shieldReady;
	public bool shieldActivate;
	public float cooldownShield;
	public int numberShield;
	public float healthMaxShield;
	public float healthShield;
	public float durationShield;
	public float health;
	public float healthMax;
	private bool colorUp;
	private float tColor;
	private Color colorbase;
	private float tShield;
	private float tEmp;
	private float tDurationShield;

	public void explode()
	{
		var b = Instantiate (explosionPrefab, transform.position, explosionPrefab.rotation) as Transform;
		Destroy(this.gameObject);
	}

	void Start()
	{
		infoUser = GameObject.Find("script").GetComponent<InfoUserBattleMode>();
		healthMax = health;
		colorbase = GetComponent<SpriteRenderer>().color;
	}

	void animTourelle()
	{
		tColor += Time.deltaTime;
		if (tColor < 0.02f)
			return ;
		tColor = 0;
		var col = GetComponent<SpriteRenderer>().color;
		if (col.b >= 1)
			colorUp = false;
		else if (col.b < 0.4f)
			colorUp = true;
		if (colorUp)
			col = new Color(col.r + 0.03f, col.g + 0.03f, col.b + 0.03f, 1.0f);
		else
			col = new Color(col.r - 0.03f, col.g - 0.03f, col.b - 0.03f, 1.0f);
		GetComponent<SpriteRenderer>().color = col;
	}

	[PunRPC]
	void activateShield(float d)
	{
		shield = true;
		shieldReady = false;
		shieldActivate = true;
		tDurationShield = d;
		transform.Find("Shield").gameObject.SetActive(true);
	}

	void OnMouseUpAsButton()
	{
		if (infoUser.attInFight == true)
			return ;
		if (shieldReady)
		{
			numberShield--;
			tShield = 0;
			photonView.RPC("activateShield", PhotonTargets.AllBufferedViaServer, durationShield);
		}
		else if (empReady)
		{
			empReady = false;
			numberEmp--;
			tEmp = 0;
			photonView.RPC("empPowerEffect", PhotonTargets.AllBufferedViaServer);
		}
	}

	[PunRPC]
	void empPowerEffect ()
	{
		empReady = false;
		var m = GameObject.FindGameObjectsWithTag("missile");
		foreach (var m2 in m)
		{
			if (m2 != null && m2.GetComponent<MoveMissile>())
			{
				m2.GetComponent<MoveMissile>().emp = true;
				m2.GetComponent<Rigidbody2D>().isKinematic = false;
				m2.transform.Find("missileColliderObject").GetComponent<BoxCollider2D>().enabled = false;
				Destroy(m2.gameObject, 3.0f);
			}
		}
	}

	void shield_gestion()
	{
		if (shield)
		{
			if (numberShield > 0 && tShield < cooldownShield)
				tShield += Time.deltaTime;
			else if (numberShield > 0 && tShield > cooldownShield && !shieldReady)
				shieldReady = true;
			else if (shieldReady)
				animTourelle();
			if (shieldActivate)
			{
				tDurationShield -= Time.deltaTime;
				if (tDurationShield <= 0)
					shieldActivate = false;
			}
			else
				transform.Find("Shield").gameObject.SetActive(false);
			if (!shieldReady)
				GetComponent<SpriteRenderer>().color = colorbase;
		}

	}

	void emp_gestion()
	{
		if (numberEmp > 0 && tEmp < cooldownEmp)
			tEmp += Time.deltaTime;
		else if (numberEmp > 0 && tEmp > cooldownEmp && !empReady)
			empReady = true;
		else if (empReady)
			animTourelle();
	}

	void Update()
	{
		if (tourelleDraggable)
			animTourelle();
		else if (!shieldReady && !empReady)
			GetComponent<SpriteRenderer>().color = colorbase;
		shield_gestion();
		emp_gestion();
	}

	public void effectBuildingPublic(float damageBuilding, bool r = false)
	{
		effectBuilding(damageBuilding, r);
	}

	[PunRPC]
	void effectBuilding(float damageBuilding, bool r = false)
	{
		if (!r && !infoUser.ia)
		{
			photonView.RPC("effectBuilding", PhotonTargets.AllViaServer, damageBuilding, true);
			return ;
		}
		health -= damageBuilding;
		if (slider != null)
			slider.GetComponent<Image>().fillAmount = 1 - (health / healthMax);
		if (health <= 0)
			explode();
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "missileCollider" && other.transform.parent.GetComponent<InfoMissile>().attInFight != attInFight && (infoUser.host || infoUser.ia))
			effectBuilding(other.transform.parent.GetComponent<InfoMissile>().damageBuilding);
	}
}