using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class Shield : Photon.MonoBehaviour
{
	public InfoUserBattleMode infoUser;
	public BuildingBattle buildingBattle;
	public Transform slider;

	void Start()
	{
		infoUser = GameObject.Find("script").GetComponent<InfoUserBattleMode>();
		buildingBattle = transform.parent.GetComponent<BuildingBattle>();
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "missile" && other.GetComponent<InfoMissile>().attInFight != buildingBattle.attInFight && (infoUser.host || infoUser.ia))
			effectShield(other.GetComponent<InfoMissile>().damageBuilding);
	}

	[PunRPC]
	void effectShield(float damageBuilding, bool r = false)
	{
		if (!r && !infoUser.ia)
		{
			photonView.RPC("effectShield", PhotonTargets.AllBufferedViaServer, damageBuilding, true);
			return ;
		}

		buildingBattle.healthShield -= damageBuilding;
		if (slider != null)
			slider.GetComponent<Image>().fillAmount = 1 - (buildingBattle.healthShield / buildingBattle.healthMaxShield);
		if (buildingBattle.healthShield <= 0)
			buildingBattle.shieldActivate = false;
	}
}