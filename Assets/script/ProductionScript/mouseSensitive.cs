using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Linq;

public class MouseSensitive : MonoBehaviour
{
	private Transform terrain;
	private Vector3 posCam;
	public bool unactiveSensitive;
	private Transform script;
	private bool okForClick;
	private Transform distFromLeftObject;
	private bool OnMouseUpAsButtonBool;
	//mouse sensitive is an invisible plane with collider for trigger mouse or touch
	void Start()
	{
		script = GameObject.Find("script").transform;
		terrain = GameObject.Find("terrain").transform;
		distFromLeftObject = GameObject.Find("distFromLeftPoint").transform;
	}

	//deselect all building and if this building is selected update him in database ans in array matrix map position
	public bool unselectAll()
	{
		bool b = false;
		var t = GameObject.FindGameObjectsWithTag("building");

		foreach (var t2 in t)
		{
			if (t2 != null && t2.GetComponent<BuildingPlacementProd>() && t2.GetComponent<BuildingPlacementProd>().selected == true) // if selected
			{
				t2.GetComponent<BuildingInfo>().distFromleft = Mathf.Round(Vector2.Distance((Vector2)distFromLeftObject.position, (Vector2)t2.GetComponent<BuildingPlacementProd>().lastGoodPosition) / 0.66f);
				if (!t2.GetComponent<BuildingPlacementProd>().canPlaceHere())
					t2.GetComponent<BuildingPlacementProd>().finalPosition = t2.GetComponent<BuildingPlacementProd>().lastGoodPosition; // posfinal = last good position
				if (t2.GetComponent<BuildingPlacementProd>().newBuilding) // if new 
					t2.GetComponent<BuildingInfo>().insertThisBuilding(t2.GetComponent<BuildingPlacementProd>().finalPosition); //add in database
				else
					t2.GetComponent<BuildingInfo>().updateThisBuilding(t2.GetComponent<BuildingPlacementProd>().finalPosition); //update in database
				script.GetComponent<GenerateProd>().fillMatrixPosition(t2.transform, false, (int)t2.transform.GetComponent<BuildingInfo>().oldPosMatrixX, (int)t2.transform.GetComponent<BuildingInfo>().oldPosMatrixY);
				script.GetComponent<GenerateProd>().fillMatrixPosition(t2.transform, true, (int)t2.transform.GetComponent<BuildingInfo>().posMatrixX, (int)t2.transform.GetComponent<BuildingInfo>().posMatrixY);
				//script.GetComponent<GenerateProd>().fillMatrixPosition(t2.GetComponent<BuildingPlacementProd>().oldPosition, t2.transform.Find("spriteFloor").localScale.x, t2.transform.Find("spriteFloor").localScale.y, false);
				//script.GetComponent<GenerateProd>().fillMatrixPosition(t2.GetComponent<BuildingPlacementProd>().finalPosition, t2.transform.Find("spriteFloor").localScale.x, t2.transform.Find("spriteFloor").localScale.y, true);
				t2.GetComponent<BuildingPlacementProd>().oldPosition = t2.GetComponent<BuildingPlacementProd>().finalPosition; // old position = new position
				script.GetComponent<SelectBuildingProd>().haveBuildingSelected = false; // unselect
				t2.GetComponent<BuildingPlacementProd>().selected = false;
				t2.GetComponent<BuildingPlacementProd>().newBuilding = false;
				t2.transform.Find("spriteBuilding").GetComponent<Renderer>().material.color = new Color(1, 1, 1);
				t2.transform.Find("spriteFloor").Find("arrowGroupe").gameObject.SetActive(false);
				b = true;
				var sList = t2.GetComponentsInChildren<SpriteRenderer>();
				foreach (SpriteRenderer s in sList)
					s.sortingOrder -= 5000;
				t2.GetComponent<BuildingPlacementProd>().canvasBuilding.Find("inSelect").gameObject.SetActive(false);
				script.GetComponent<GenerateProd>().fillInfoUserRessource();
				script.GetComponent<GenerateProd>().reGenerateUIMissile();
				script.GetComponent<GenerateProd>().reorderSprite();
				script.GetComponent<GenerateProd>().reUpdateBuildingButtonBuild();
				GameObject.Find("GameController").GetComponent<ClickOnBuildingOnOff>().desactive();
			if (terrain)
				terrain.GetComponent<TerrainInfo>().chooseTexture(false);
			}
		}
		return (b);
	}

	void OnMouseDown()
	{
		if (unactiveSensitive)
			return ;
		GameObject.Find("CameraTarget").GetComponent<Move>().setVelocity(Vector3.zero);
		posCam = GameObject.Find("CameraTarget").transform.position;
	}

	public void OnMouseUp()
	{
		#if UNITY_ANDROID && !UNITY_EDITOR
			if (Input.touchCount > 0)
			{
				if (unactiveSensitive || EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
				{
					OnMouseUpAsButtonBool = false;
					return ;
				}
				if (OnMouseUpAsButtonBool)
					unselectAll();
			}	
			OnMouseUpAsButtonBool = false;
		#endif
		#if UNITY_EDITOR || UNITY_STANDALONE
			if (unactiveSensitive || EventSystem.current.IsPointerOverGameObject())
			{
				OnMouseUpAsButtonBool = false;
				return ;
			}
			if (OnMouseUpAsButtonBool)
				unselectAll();
			OnMouseUpAsButtonBool = false;
		#endif
	}

	void OnMouseUpAsButton()
	{
		GameObject.Find("GameController").GetComponent<DisplayInfosSlider>().desactive();
		if (unactiveSensitive)
			return ;
		// if (okForClick)
		// {
			okForClick = false;
			if (posCam != GameObject.Find("CameraTarget").transform.position)
				return ;
			OnMouseUpAsButtonBool = true;
			// if (unselectAll())
			// {
			// 	if (terrain)
			// 		terrain.GetComponent<TerrainInfo>().chooseTexture(false);
			// }
		// }
	}
}
