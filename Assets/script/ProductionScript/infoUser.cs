using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InfoUser : MonoBehaviour
{
	public Transform sliderRessource1;
	public Transform sliderRessource2;
	public Transform sliderRessource3;
	public int id;
	public string pseudo;
	public string dateLastConnection;
	public string dateNow;
	public int differenceInSeconds;
	public int timeCoInSec;
	public int level;
	public int xp;
	public bool fightMode;
	public int nbrTotalR1;
	public int nbrTotalR2;
	public int nbrTotalR3;
	public int nbrTotalR4;
	public int nbrMaxR1;
	public int nbrMaxR2;
	public int nbrMaxR3;
	public float prodRateNumberRessource1;
	public float prodRateNumberRessource2;
	public float prodRateNumberRessource3;
	private int nbrGodMode;
	private float tGodMode;
	private bool godModeActive;

	void Start()
	{
		level = 1;
	}

	void godMode()
	{
		if (godModeActive)
		{
			tGodMode += Time.deltaTime;
			if (tGodMode > 8.0f)
			{
				tGodMode = 0;
				nbrTotalR1 = 1000000;
				nbrTotalR2 = 1000000;
				nbrTotalR3 = 1000000;
				nbrMaxR1 = 1000000;
				nbrMaxR2 = 1000000;
				nbrMaxR3 = 1000000;
				GetComponent<GenerateProd>().reGenerateUIMissile();
				GameObject.Find("GameController").GetComponent<UpdateSlider>().displayRessource();
			}
			return ;
		}
		if (!godModeActive && nbrGodMode == 10)
		{
			tGodMode = 0;
			nbrGodMode = 0;
			godModeActive = true;
			print ("godModeActive");
			nbrTotalR1 = 1000000;
			nbrTotalR2 = 1000000;
			nbrTotalR3 = 1000000;
			nbrTotalR4 = 1000000;
			nbrMaxR1 = 1000000;
			nbrMaxR2 = 1000000;
			nbrMaxR3 = 1000000;
			GetComponent<GenerateProd>().reGenerateUIMissile();
			GameObject.Find("GameController").GetComponent<UpdateSlider>().displayRessource();
		}
		else if (Input.GetMouseButtonUp(0))
		{
			if (tGodMode <= 0.6f && Input.mousePosition.x > 600 && Input.mousePosition.y > 350)
			{
				tGodMode = 0;
				nbrGodMode++;
			}
		}
		else
		{
			tGodMode += Time.deltaTime;
			if (tGodMode > 0.6f)
			{
				tGodMode = 0;
				nbrGodMode = 0;
			}
		}
	}

	void Update()
	{
		godMode();
	}
}
