using UnityEngine;
using System.Collections;

public class BuildingInfo : MonoBehaviour
{
	public int oldPosMatrixX;
	public int oldPosMatrixY;
	public int posMatrixX;
	public int posMatrixY;
	public int idHash;
	public int buildingPlayerId;
	public int buildingLevelListId;
	public string name;
	public string description;
	public int level;
	public bool buyable;
	public bool buildable;
	public bool preConstructed;
	public float health;
	public Texture2D texture;
	public bool canEnter;
	public Transform panel;
	public bool isCity;
	public bool isProd;
	public bool isMovibleInfight;
	public float speed;
	public float range;
	public int levelRessource1; // level up
	public int levelRessource2; // level up
	public int levelRessource3; //level Up
	public int levelRessource4; //level Up
	public float costBuyRessource1; // cost buy building
	public float costBuyRessource2; // cost buy building
	public float costBuyRessource3; //cost buy building
	public float costBuyRessource4; //cost buy building
	public int capacityRessource1; // capacity
	public int capacityRessource2; // capacity
	public int capacityRessource3; //capacity
	public int capacityRessource4; //capacity
	public int stockRessource1; // stock
	public int stockRessource2; // stock
	public int stockRessource3; //stock
	public int stockRessource4; //stock
	public float ratioRessource1; // ratio
	public float ratioRessource2; // ratio
	public float ratioRessource3; //ratio
	public float ratioRessource4; //ratio
	public int nbrRessource1; // nbr
	public int nbrRessource2; // nbr
	public int nbrRessource3; //nbr
	public int buildingLevelListIdnextLevel;
	public float distFromleft;
	public bool att;
	public bool def;
	public float xp;
	public float instantR1;
	public float instantR2;
	public float instantR3;
	public float instantTime;
	public float instant_build_cost_r1;
	public float instant_build_cost_r2;
	public float instant_build_cost_r3;
	public float healthShield;

	public float constructionTime;
	public float totalInstant;
	public bool upgrade;
	public float timeBetweenStartUpgrade;
	public int costRessource4UpgradeLeft;

	public bool missileUpgrade;
	public string missileName;
	public float timeBetweenStartUpgradeMissile;
	public float totalInstantMissile;
	public float upgradeTimeMissile;
	public int costRessource4UpgradeLeftMissile;
	public InfoMissile infoMissileOld;
	public InfoMissile infoMissileNew;

	private int idPlayer;

	void Start()
	{
		idPlayer = GameObject.Find("mainScript").GetComponent<GenerateResource>().idPlayer;
		if (description == "")
			description = "Ce batiment n'a pas encore de description.";
	}

	IEnumerator insertBuilding(Vector3 pos)
	{
		if (idPlayer == 0)
			idPlayer = GameObject.Find("mainScript").GetComponent<GenerateResource>().idPlayer;
		var x = pos.x;
		var y = pos.y;
		var z = pos.z;
		var url = "http://5.196.68.62/api/addBuildingPlayer/"+idPlayer+"/"+buildingLevelListId+"/"+x+"/"+y+"/"+z+"/"+distFromleft;
		//print (url);
		url = url.Replace(" ", "%20");
		WWW www = new WWW(url);
		yield return (www);
		if (www.text != "")
			buildingPlayerId = int.Parse(www.text);
		else
			print ("le batiment n'a pas pu etre ajoute");
	}

	IEnumerator updateBuilding(Vector3 pos, bool levelUpFinish)
	{
		var l = (levelUpFinish == true) ? 1 : 0;
		if (idPlayer == 0)
			idPlayer = GameObject.Find("mainScript").GetComponent<GenerateResource>().idPlayer;
		var x = pos.x;
		var y = pos.y;
		var z = pos.z;
		var nbrRessource4 = GameObject.Find("script").GetComponent<InfoUser>().nbrTotalR4;
		var url = "http://5.196.68.62/api/updateBuildingPlayer/"+idPlayer+"/"+buildingLevelListId+"/"+buildingPlayerId+"/"+x+"/"+y+"/"+z+"/"+distFromleft+"/"+nbrRessource1+"/"+nbrRessource2+"/"+nbrRessource3+"/"+nbrRessource4+"/"+l;
		//print (url);
		url = url.Replace(" ", "%20");
		WWW www = new WWW(url);
		yield return www;
	}

	IEnumerator launchLevelUpBuilding()
	{
		if (idPlayer == 0)
			idPlayer = GameObject.Find("mainScript").GetComponent<GenerateResource>().idPlayer;
		var url = "http://5.196.68.62/api/updateBuildingPlayerLaunchLevelUp/"+idPlayer+"/"+buildingLevelListId+"/"+buildingPlayerId;
		//print (url);
		url = url.Replace(" ", "%20");
		WWW www = new WWW(url);
		yield return www;
	}

	// IEnumerator RemoveBuilding()
	// {
	// 	var url = "http://5.196.68.62/building.php?RemoveBuilding=true&id="+buildingPlayerId+"&pseudo="+pseudo+"";
	// 	url = url.Replace(" ", "%20");
	// 	WWW www = new WWW(url);
	// 	yield return www;
	// }

	public void insertThisBuilding(Vector3 pos = new Vector3())
	{
		StartCoroutine(insertBuilding(pos));
	}

	public void updateThisBuilding(Vector3 pos = new Vector3(), bool levelUpFinish = false)
	{
		StartCoroutine(updateBuilding(pos, levelUpFinish));
	}

	public void launchLevelUpThisBuilding()
	{
		StartCoroutine(launchLevelUpBuilding());
	}

	public void RemoveThisBuilding()
	{
		//StartCoroutine(RemoveBuilding());
	}
}
