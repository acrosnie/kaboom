using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonActionBuilding : MonoBehaviour
{
	public Texture2D iconeHealth;
	public Texture2D iconeRessource1;
	public Texture2D iconeRessource2;
	public Texture2D iconeRessource3;
	public Texture2D iconeXp;
	private MouseSensitive MouseSensitive;
	private Transform infoPanel;
	private Transform upgradePanel;
	private Transform greyFilter;
	private BuildingInfo info;
	private Transform menuSelected;

	void Start()
	{
		if (GameObject.Find("mouseCollider"))
			MouseSensitive = GameObject.Find("mouseCollider").GetComponent<MouseSensitive>();
		if (GameObject.Find("UICanvas"))
		{
			infoPanel = GameObject.Find("UICanvas").transform.Find("InfoPanel");
			upgradePanel = GameObject.Find("UICanvas").transform.Find("UpgradePanel");
			greyFilter = GameObject.Find("UICanvas").transform.Find("greyFilter");
		}
	}

	public void buttonInfo ()
	{
		MouseSensitive.unactiveSensitive = true;
		menuSelected = infoPanel;
		buildSlotInfo();
		infoPanel.gameObject.SetActive(true);
		upgradePanel.gameObject.SetActive(false);
		greyFilter.gameObject.SetActive(true);
	}

	public void buttonUpgrade ()
	{
		MouseSensitive.unactiveSensitive = true;
		menuSelected = upgradePanel;
		buildSlotUpgrade();
		infoPanel.gameObject.SetActive(false);
		upgradePanel.gameObject.SetActive(true);
		greyFilter.gameObject.SetActive(true);
	}

	private void buildSlotInfo()
	{
		if (menuSelected == null)
			return ;
		info = transform.parent.GetComponent<BuildingInfo>();
		fillInfo(info.name.ToUpper(), info.level.ToString(), info.description, info.texture);
		// SLOT 1
		fillSlot(menuSelected.Find("slot1"), iconeHealth, "Health", info.health.ToString()); // health
		//SLOT 2
		fillSlot(menuSelected.Find("slot2"), iconeXp, "Level up Ressource :", info.levelRessource1.ToString() + " / " + info.levelRessource2.ToString() + " / " + info.levelRessource3.ToString());
		//SLOT 3
		if (info.stockRessource1 > 0)
			fillSlot(menuSelected.Find("slot3"), iconeRessource1, "Stock Ressource 1", info.nbrRessource1.ToString() + " / " + info.stockRessource1.ToString());
		else if (info.stockRessource2 > 0)
			fillSlot(menuSelected.Find("slot3"), iconeRessource2, "Stock Ressource 2", info.nbrRessource2.ToString() + " / " + info.stockRessource2.ToString());
		else if (info.stockRessource3 > 0)
			fillSlot(menuSelected.Find("slot3"), iconeRessource3, "Stock Ressource 3", info.nbrRessource3.ToString() + " / " + info.stockRessource3.ToString());
		else if (info.ratioRessource1 > 0)
			fillSlot(menuSelected.Find("slot3"), iconeRessource1, "Nombre de Ressource1 par Heure", (info.ratioRessource1 * 3600).ToString());
		else if (info.ratioRessource2 > 0)
			fillSlot(menuSelected.Find("slot3"), iconeRessource2, "Nombre de Ressource2 par Heure", (info.ratioRessource2 * 3600).ToString());
		else if (info.ratioRessource3 > 0)
			fillSlot(menuSelected.Find("slot3"), iconeRessource3, "Nombre de Ressource3 par Heure", (info.ratioRessource3 * 3600).ToString());
		else
			menuSelected.Find("slot3").gameObject.SetActive(false);
		menuSelected.Find("slot4").gameObject.SetActive(false);
	}

	private void buildSlotUpgrade()
	{
		if (menuSelected == null)
			return ;
		info = transform.parent.GetComponent<BuildingInfo>();
		fillUpgrade(info.name.ToUpper(), info.texture);
		// SLOT 1
		fillSlot(menuSelected.Find("slot1"), iconeXp, "Level up Ressource :", info.levelRessource1.ToString() + " / " + info.levelRessource2.ToString() + " / " + info.levelRessource3.ToString());
		//SLOT 2
		fillSlot(menuSelected.Find("slot2"), iconeXp, "Points d'experience", "+17"); // XP
		menuSelected.Find("Button").GetComponent<Button>().onClick.RemoveAllListeners();
		menuSelected.Find("Button").GetComponent<Button>().onClick.AddListener(delegate{ transform.parent.GetComponent<BuildingAction>().levelUp(); });
	}


	private void fillUpgrade(string title, Texture2D texture)
	{
		menuSelected.Find("Titre").GetComponent<Text>().text = title + " AMELIORER AU NIVEAU " + (info.level + 1);
		menuSelected.Find("Niveau").GetComponent<Text>().text = "";
		menuSelected.Find("ImageSprite").GetComponent<RawImage>().texture = texture;
	}

	private void fillInfo(string title, string level, string description, Texture2D texture)
	{
		menuSelected.Find("Titre").GetComponent<Text>().text = title;
		menuSelected.Find("Niveau").GetComponent<Text>().text = "Niveau " + level;
		//slot.Find("Description").GetComponent<Text>().text = description;
		menuSelected.Find("ImageSprite").GetComponent<RawImage>().texture = texture;
	}

	private void fillSlot(Transform slot, Texture2D texture, string intitule, string val)
	{
		slot.gameObject.SetActive(true);
		slot.Find("Image").GetComponent<RawImage>().texture = texture;
		slot.Find("Intitule").GetComponent<Text>().text = intitule;
		slot.Find("Value").GetComponent<Text>().text = val;
	}
}
