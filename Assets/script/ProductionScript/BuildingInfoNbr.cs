using UnityEngine;

[System.Serializable]
public class BuildingInfoNbr : System.Object
{
	public string name;
	public int nbrMax;
	public int nbr;
	public int[] level; //TownHall
	public int[] nbrForLevel; //TownHall
}
