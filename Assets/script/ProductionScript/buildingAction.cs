using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BuildingAction : MonoBehaviour
{
	private BuildingInfo info;
	private InfoUser infoUser;
	private Transform script;
	public float tempRessource1;
	public float tempRessource2;
	public float tempRessource3;
	public Transform infoText;

	void Start()
	{
		infoText = GameObject.Find("Canvas").transform.Find("info");
		script = GameObject.Find("script").transform;
		info = GetComponent<BuildingInfo>();
		infoUser = GameObject.Find("script").GetComponent<InfoUser>();
		//fillInfoUserRessource();
		if (info.ratioRessource1 > 0)
			StartCoroutine (pumpRatioRessource1()); // if building can pump ressource1, pump ressource 1 !
		if (info.ratioRessource2 > 0)
			StartCoroutine (pumpRatioRessource2()); // if building can pump ressource2, pump ressource 2 !
		if (info.ratioRessource3 > 0)
			StartCoroutine (pumpRatioRessource3()); // if building can pump ressource3, pump ressource 3 !
	}

	void errorMessage(string error)
	{
		var infoTextClone = Instantiate(infoText, infoText.position, infoText.rotation) as Transform;
		infoTextClone.gameObject.SetActive(true);
		infoTextClone.GetComponent<Text>().text = error;
		infoTextClone.parent = GameObject.Find("Canvas").transform;
		Destroy(infoTextClone.gameObject, 2.0f);
		print (error);
	}

	IEnumerator pumpRatioRessource1()
	{
		while (true)
		{
			yield return new WaitForSeconds(2.0f); // each second ==> 2.0f = 1.0f second in time manager
			if (info.nbrRessource1 >= info.capacityRessource1) // capacity <= 0 || info.nbrRessource1 >= info.capacityRessource1) // if can't pump ?
			{
				// do nothing ? 
			}
			else // pump
			{
				tempRessource1 += info.ratioRessource1;
				if (tempRessource1 >= 1)
				{
					info.nbrRessource1 += (int)tempRessource1;
					tempRessource1 -= (int)tempRessource1;
				}
			}
		}
	}

	public void fillInfoBuildingPanel()
	{
		var buildingList = script.GetComponent<SelectBuildingProd>().building;
		var f = GameObject.Find("GameController").GetComponent<FillBuildingPanelLevel>();
		if (info.name == "Town Hall")
			f.fillInfoGeneral(info, buildingList, transform);
		else if (info.name == "Ressource 1 Stock")
			f.fillInfoGeneral(info, buildingList, transform);
		else if (info.name == "Ressource 2 Stock")
			f.fillInfoGeneral(info, buildingList, transform);
		else if (info.name == "Ressource 3 Stock")
			f.fillInfoGeneral(info, buildingList, transform);
		else if (info.name == "Ressource 1 Prod")
			f.fillInfoGeneral(info, buildingList, transform);
		else if (info.name == "Ressource 2 Prod")
			f.fillInfoGeneral(info, buildingList, transform);
		else if (info.name == "Ressource 3 Prod")
			f.fillInfoGeneral(info, buildingList, transform);
		else if (info.name == "Attack Whale")
			f.fillInfoGeneral(info, buildingList, transform);
		else if (info.name == "Missile Launcher")
			f.fillInfoGeneral(info, buildingList, transform);
		else if (info.name == "Exploration Manta Ray")
			f.fillInfoGeneral(info, buildingList, transform);
		else if (info.name == "Local Shield Generator")
			f.fillInfoGeneral(info, buildingList, transform);
		else if (info.name == "EMP Tower")
			f.fillInfoGeneral(info, buildingList, transform);
		else if (info.name == "Ballon Mines")
			f.fillInfoGeneral(info, buildingList, transform);
		else if (info.name == "Bunker")
			f.fillInfoGeneral(info, buildingList, transform);
		else if (info.name == "Geo's Lab")
			f.fillInfoGeneral(info, buildingList, transform);
		else if (info.name == "Research Center")
			f.fillInfoGeneral(info, buildingList, transform);
		else if (info.name == "Missile Factory")
			f.fillInfoGeneral(info, buildingList, transform);
	}

	public void fillUpgradeBuildingPanel()
	{
		var buildingList = script.GetComponent<SelectBuildingProd>().building;
		var f = GameObject.Find("GameController").GetComponent<FillBuildingPanelLevel>();
		// if (info.name == "Town Hall")
		// 	f.fillUpgradeTownHall(info, buildingList, transform);
		// else
		// 	f.fillUpgradeGeneral(info, buildingList, transform);
		f.fillUpgradeGeneral(info, buildingList, transform);
	}

	public void cancelUpgrade()
	{
		//script.GetComponent<GenerateProd>().recupRessource(info.levelRessource1, info.levelRessource2, info.levelRessource3);
		GetComponent<BuildingPlacementProd>().canvasBuilding.Find("inSelect").gameObject.SetActive(false);
		GetComponent<BuildingPlacementProd>().selected = false;
		GetComponent<BuildingInfo>().timeBetweenStartUpgrade = 0;
		GetComponent<BuildingInfo>().upgrade = false;
		script.GetComponent<GenerateProd>().recupRessource(info.levelRessource1, info.levelRessource2, info.levelRessource3);
		script.GetComponent<GenerateProd>().fillInfoUserRessource();
		GetComponent<BuildingInfo>().updateThisBuilding(transform.position, true); // finishlevelUp
		GameObject.Find("mouseCollider").GetComponent<MouseSensitive>().unactiveSensitive = false;
		GameObject.Find("GameController").GetComponent<InfoMenuOn>().desactive();
		GameObject.Find("GameController").GetComponent<ClickOnBuildingOnOff>().desactive();
		transform.Find("spriteFloor").Find("arrowGroupe").gameObject.SetActive(false);
		print ("Cancel Level Up !");
	}

	public void cancelUpgradeMissile()
	{
		script.GetComponent<upgradeMissile>().cancelUpdateMissileControllerPublic(GetComponent<BuildingInfo>().infoMissileOld, GetComponent<BuildingInfo>().infoMissileNew);
		GetComponent<BuildingPlacementProd>().canvasBuilding.Find("inSelect").gameObject.SetActive(false);
		GetComponent<BuildingPlacementProd>().selected = false;
		GetComponent<BuildingInfo>().timeBetweenStartUpgradeMissile = 0;
		GetComponent<BuildingInfo>().missileUpgrade = false;
		script.GetComponent<GenerateProd>().recupRessource(0, (int)GetComponent<BuildingInfo>().infoMissileOld.ressource2CostUpgrade, 0);
		script.GetComponent<GenerateProd>().fillInfoUserRessource();
		GameObject.Find("mouseCollider").GetComponent<MouseSensitive>().unactiveSensitive = false;
		GameObject.Find("GameController").GetComponent<InfoMenuOn>().desactive();
		GameObject.Find("GameController").GetComponent<ClickOnBuildingOnOff>().desactive();
		transform.Find("spriteFloor").Find("arrowGroupe").gameObject.SetActive(false);
		print ("Cancel Level Up Missile!");
	}

	public void finishLevelUpGlobesMissile(float cost = 0)
	{
		if (cost > 0)
			GetComponent<BuildingInfo>().costRessource4UpgradeLeftMissile = (int)cost;
		if (infoUser.nbrTotalR4 >= GetComponent<BuildingInfo>().costRessource4UpgradeLeftMissile)
		{
			script.GetComponent<GenerateProd>().spendGlobes(GetComponent<BuildingInfo>().costRessource4UpgradeLeftMissile);
			script.GetComponent<upgradeMissile>().finishUpdateMissileControllerPublic(GetComponent<BuildingInfo>().infoMissileOld.missileLevelListId, GetComponent<BuildingInfo>().infoMissileNew.missileLevelListId, GetComponent<BuildingInfo>().infoMissileNew.missileListId);
			GetComponent<BuildingInfo>().missileUpgrade = false;
			GameObject.Find("mouseCollider").GetComponent<MouseSensitive>().unactiveSensitive = false;
			GameObject.Find("GameController").GetComponent<InfoMenuOn>().desactive();
			GameObject.Find("GameController").GetComponent<ClickOnBuildingOnOff>().desactive();
		}
		else
			print ("not enough globes");
	}

	public void finishLevelUpMissile()
	{
		script.GetComponent<upgradeMissile>().finishUpdateMissileControllerPublic(GetComponent<BuildingInfo>().infoMissileOld.missileLevelListId, GetComponent<BuildingInfo>().infoMissileNew.missileLevelListId, GetComponent<BuildingInfo>().infoMissileNew.missileListId);
		GetComponent<BuildingInfo>().missileUpgrade = false;
		GameObject.Find("mouseCollider").GetComponent<MouseSensitive>().unactiveSensitive = false;
		GameObject.Find("GameController").GetComponent<InfoMenuOn>().desactive();
		GameObject.Find("GameController").GetComponent<ClickOnBuildingOnOff>().desactive();
	}

	public void finishLevelUpGlobes(float cost = 0)
	{
		if (cost > 0)
			GetComponent<BuildingInfo>().costRessource4UpgradeLeft = (int)cost;
		if (infoUser.nbrTotalR4 >= GetComponent<BuildingInfo>().costRessource4UpgradeLeft)
		{
			var b = script.GetComponent<SelectBuildingProd>().createSelectableBuilding(0, info.name, info.level + 1, false);
			if (b == null)
				return ;
			script.GetComponent<GenerateProd>().spendGlobes(GetComponent<BuildingInfo>().costRessource4UpgradeLeft);
			script.GetComponent<GenerateProd>().fillInfoUserRessource();
			b.position = transform.position;
			b.GetComponent<BuildingPlacementProd>().selected = false;
			b.GetComponent<BuildingPlacementProd>().newBuilding = false;
			b.GetComponent<BuildingPlacementProd>().lastGoodPosition = b.transform.position;
			b.GetComponent<BuildingPlacementProd>().finalPosition = b.transform.position;
			b.GetComponent<BuildingPlacementProd>().initialPosition = b.transform.position;
			b.GetComponent<BuildingPlacementProd>().oldPosition = b.transform.position;
			b.GetComponent<BuildingInfo>().nbrRessource1 = info.nbrRessource1;
			b.GetComponent<BuildingInfo>().nbrRessource2 = info.nbrRessource2;
			b.GetComponent<BuildingInfo>().nbrRessource3 = info.nbrRessource3;
			b.GetComponent<BuildingInfo>().buildingPlayerId = info.buildingPlayerId;
			b.GetComponent<BuildingInfo>().updateThisBuilding(transform.position, true);
			b.transform.Find("spriteFloor").Find("arrowGroupe").gameObject.SetActive(false);
			if (b.GetComponent<BuildingInfo>().name == "Town Hall")
				script.GetComponent<GenerateProd>().levelTownHall += 1;
			else if (b.GetComponent<BuildingInfo>().name == "Geo's Lab")
				script.GetComponent<GenerateProd>().levelGeoLab += 1;
			else if (b.GetComponent<BuildingInfo>().name == "Missile Factory")
				script.GetComponent<GenerateProd>().missileFactoryLevel += 1;
			script.GetComponent<GenerateProd>().reGenerateUIMissile();
			GameObject.Find("mouseCollider").GetComponent<MouseSensitive>().unactiveSensitive = false;
			script.GetComponent<GenerateProd>().reorderSprite();
			print ("Level Up Globes!");
			GameObject.Find("GameController").GetComponent<InfoMenuOn>().desactive();
			GameObject.Find("GameController").GetComponent<ClickOnBuildingOnOff>().desactive();
			Destroy(gameObject);
		}
		else
			print ("not enough globes");
	}

	public void finishLevelUp()
	{
		var b = script.GetComponent<SelectBuildingProd>().createSelectableBuilding(0, info.name, info.level + 1, false);
		if (b == null)
			return ;
		b.position = transform.position;
		b.GetComponent<BuildingPlacementProd>().selected = false;
		b.GetComponent<BuildingPlacementProd>().newBuilding = false;
		b.GetComponent<BuildingPlacementProd>().lastGoodPosition = b.transform.position;
		b.GetComponent<BuildingPlacementProd>().finalPosition = b.transform.position;
		b.GetComponent<BuildingPlacementProd>().initialPosition = b.transform.position;
		b.GetComponent<BuildingPlacementProd>().oldPosition = b.transform.position;
		b.GetComponent<BuildingInfo>().nbrRessource1 = info.nbrRessource1;
		b.GetComponent<BuildingInfo>().nbrRessource2 = info.nbrRessource2;
		b.GetComponent<BuildingInfo>().nbrRessource3 = info.nbrRessource3;
		b.GetComponent<BuildingInfo>().buildingPlayerId = info.buildingPlayerId;
		b.GetComponent<BuildingInfo>().updateThisBuilding(transform.position, true);
		b.transform.Find("spriteFloor").Find("arrowGroupe").gameObject.SetActive(false);
		if (b.GetComponent<BuildingInfo>().name == "Town Hall")
			script.GetComponent<GenerateProd>().levelTownHall += 1;
		else if (b.GetComponent<BuildingInfo>().name == "Geo's Lab")
			script.GetComponent<GenerateProd>().levelGeoLab += 1;
		else if (b.GetComponent<BuildingInfo>().name == "Missile Factory")
			script.GetComponent<GenerateProd>().missileFactoryLevel += 1;
		script.GetComponent<GenerateProd>().reGenerateUIMissile();
		GameObject.Find("mouseCollider").GetComponent<MouseSensitive>().unactiveSensitive = false;
		script.GetComponent<GenerateProd>().reorderSprite();
		print ("Level Up !");
		GameObject.Find("GameController").GetComponent<InfoMenuOn>().desactive();
		GameObject.Find("GameController").GetComponent<ClickOnBuildingOnOff>().desactive();
		Destroy(gameObject);
	}

	public void levelUp()
	{
		if (info.buildingLevelListIdnextLevel != -1)
		{
			if (infoUser.nbrTotalR1 >= info.levelRessource1 && infoUser.nbrTotalR2 >= info.levelRessource2 && infoUser.nbrTotalR3 >= info.levelRessource3)
			{
				GetComponent<BuildingPlacementProd>().canvasBuilding.Find("inSelect").gameObject.SetActive(false);
				GetComponent<BuildingPlacementProd>().selected = false;
				GetComponent<BuildingInfo>().timeBetweenStartUpgrade = 0;
				GetComponent<BuildingInfo>().upgrade = true;
				script.GetComponent<GenerateProd>().spendRessource(info.levelRessource1, info.levelRessource2, info.levelRessource3);
				GetComponent<BuildingInfo>().launchLevelUpThisBuilding();
				print ("Start Level Up !");
				GameObject.Find("mouseCollider").GetComponent<MouseSensitive>().unactiveSensitive = false;
				GameObject.Find("GameController").GetComponent<InfoMenuOn>().desactive();
				GameObject.Find("GameController").GetComponent<ClickOnBuildingOnOff>().desactive();
				transform.Find("spriteFloor").Find("arrowGroupe").gameObject.SetActive(false);
			}
			else
			{
				var tr = transform;
				GameObject.Find("GameController").GetComponent<ConvertGlobes>().displayBuildingUpgrade(tr);
			}
		}
		else
			errorMessage ("Can't level up because no next level available");
	}

	public void levelUpForced(int r1, int r2, int r3, int r4)
	{
		GetComponent<BuildingPlacementProd>().canvasBuilding.Find("inSelect").gameObject.SetActive(false);
		GetComponent<BuildingPlacementProd>().selected = false;
		GetComponent<BuildingInfo>().timeBetweenStartUpgrade = 0;
		GetComponent<BuildingInfo>().upgrade = true;
		script.GetComponent<GenerateProd>().spendRessource(r1, r2, r3);
		script.GetComponent<GenerateProd>().spendGlobes(r4);
		GetComponent<BuildingInfo>().launchLevelUpThisBuilding();
		print ("Start Level Up !");
		GameObject.Find("mouseCollider").GetComponent<MouseSensitive>().unactiveSensitive = false;
		GameObject.Find("GameController").GetComponent<InfoMenuOn>().desactive();
		GameObject.Find("GameController").GetComponent<ClickOnBuildingOnOff>().desactive();
		transform.Find("spriteFloor").Find("arrowGroupe").gameObject.SetActive(false);
	}

	IEnumerator pumpRatioRessource2()
	{
		while (true)
		{
			yield return new WaitForSeconds(2.0f); // each second ==> 2.0f = 1.0f second in time manager
			if (info.nbrRessource2 >= info.capacityRessource2) // capacity <= 0 || info.nbrRessource2 >= info.capacityRessource2) // if can't pump ?
			{
				// do nothing ? 
			}
			else // pump
			{
				tempRessource2 += info.ratioRessource2;
				if (tempRessource2 >= 1)
				{
					info.nbrRessource2 += (int)tempRessource2;
					tempRessource2 -= (int)tempRessource2;
				}
			}
		}
	}

	IEnumerator pumpRatioRessource3()
	{
		while (true)
		{
			yield return new WaitForSeconds(2.0f); // each second ==> 2.0f = 1.0f second in time manager
			if (info.nbrRessource3 >= info.capacityRessource3) // capacity <= 0 || info.nbrRessource3 >= info.capacityRessource3) // if can't pump ?
			{
				// do nothing ? 
			}
			else // pump
			{
				tempRessource3 += info.ratioRessource3;
				if (tempRessource3 >= 1)
				{
					info.nbrRessource3 += (int)tempRessource3;
					tempRessource3 -= (int)tempRessource3;
				}
			}
		}
	}

	public void getRessource()
	{
		bool m;
		var buildings = GameObject.FindGameObjectsWithTag("building");
		int nbrRessource1 = info.nbrRessource1;
		int nbrRessource2 = info.nbrRessource2;
		int nbrRessource3 = info.nbrRessource3;
		if ((nbrRessource1 == 0 && nbrRessource2 == 0 && nbrRessource3 == 0) || (info.ratioRessource1 == 0 && info.ratioRessource2 == 0 && info.ratioRessource3 == 0))
			return ;
		foreach (GameObject b in buildings)
		{
			m = false;
			if (info.nbrRessource1 > 0)
			{
				for (int i = 0; i < nbrRessource1; i++)
				{
					if (b.GetComponent<BuildingInfo>().nbrRessource1 < b.GetComponent<BuildingInfo>().stockRessource1)
					{
						b.GetComponent<BuildingInfo>().nbrRessource1++;
						info.nbrRessource1--;
						m = true;
					}
				}
			}
			if (info.nbrRessource2 > 0)
			{
				for (int i = 0; i < info.nbrRessource2; i++)
				{
					if (b.GetComponent<BuildingInfo>().nbrRessource2 < b.GetComponent<BuildingInfo>().stockRessource2)
					{
						b.GetComponent<BuildingInfo>().nbrRessource2++;
						info.nbrRessource2--;
						m = true;
					}
				}
			}
			if (info.nbrRessource3 > 0)
			{
				for (int i = 0; i < info.nbrRessource3; i++)
				{
					if (b.GetComponent<BuildingInfo>().nbrRessource3 < b.GetComponent<BuildingInfo>().stockRessource3)
					{
						b.GetComponent<BuildingInfo>().nbrRessource3++;
						info.nbrRessource3--;
						m = true;
					}
				}
			}
			if (m)
				b.GetComponent<BuildingInfo>().updateThisBuilding(b.GetComponent<BuildingPlacementProd>().finalPosition);
		}
		//update in database
		GetComponent<BuildingInfo>().updateThisBuilding(GetComponent<BuildingPlacementProd>().finalPosition);
		script.GetComponent<GenerateProd>().reUpdateBuildingButtonBuild();
		script.GetComponent<GenerateProd>().reGenerateUIMissile();
		script.GetComponent<GenerateProd>().fillInfoUserRessource();
	}

	void displayHideButton()
	{
		// if ((info.nbrRessource1 > 0 && info.ratioRessource1 > 0) || (info.nbrRessource2 > 0 && info.ratioRessource2 > 0) || (info.nbrRessource3> 0 && info.ratioRessource3 > 0))
		// 	transform.Find("CanvasBuilding").Find("getRessource").gameObject.SetActive(true);
		// else
		// 	transform.Find("CanvasBuilding").Find("getRessource").gameObject.SetActive(false);
	}

	void capacityRessource1Action()
	{
		if (info.nbrRessource1 > info.capacityRessource1 && info.stockRessource1 == 0)
			info.nbrRessource1 = info.capacityRessource1;
		if (info.nbrRessource2 > info.capacityRessource2 && info.stockRessource2 == 0)
			info.nbrRessource2 = info.capacityRessource2;
		if (info.nbrRessource3 > info.capacityRessource3 && info.stockRessource3 == 0)
			info.nbrRessource3 = info.capacityRessource3;	
	}

	void Update()
	{
		displayHideButton();
		capacityRessource1Action(); // check if curentlyNbr > maxNumber
	}
}
