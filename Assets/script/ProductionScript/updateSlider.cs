using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UpdateSlider : MonoBehaviour
{
	public Transform sliderRessource1;
	public Transform sliderRessource2;
	public Transform sliderRessource3;
	public Transform sliderRessource4;
	private Image fillerRessource1;
	private Image fillerRessource2;
	private Image fillerRessource3;
	private Image fillerRessource4;
	private Text textRessource1;
	private Text textRessource2;
	private Text textRessource3;
	private Text textRessource4;
	private InfoUser infoUser;

	void Start()
	{
		infoUser = GameObject.Find("script").GetComponent<InfoUser>();
		fillerRessource1 = sliderRessource1.Find("Fill").GetComponent<Image>();
		textRessource1 = sliderRessource1.Find("Number").GetComponent<Text>();
		fillerRessource2 = sliderRessource2.Find("Fill").GetComponent<Image>();
		textRessource2 = sliderRessource2.Find("Number").GetComponent<Text>();
		fillerRessource3 = sliderRessource3.Find("Fill").GetComponent<Image>();
		textRessource3 = sliderRessource3.Find("Number").GetComponent<Text>();
		fillerRessource4 = sliderRessource4.Find("Fill").GetComponent<Image>();
		textRessource4 = sliderRessource4.Find("Number").GetComponent<Text>();
	}

	public void displayRessource()
	{
		if (infoUser == null)
			return ;
		fillerRessource1.fillAmount = (infoUser.nbrTotalR1 == 0) ? 0 : ((100 * infoUser.nbrTotalR1) / infoUser.nbrMaxR1) / 100.0f;
		textRessource1.text = "" + infoUser.nbrTotalR1;
		fillerRessource2.fillAmount = (infoUser.nbrTotalR2 == 0) ? 0 : ((100 * infoUser.nbrTotalR2) / infoUser.nbrMaxR2) / 100.0f;
		textRessource2.text = "" + infoUser.nbrTotalR2;
		fillerRessource3.fillAmount = (infoUser.nbrTotalR3 == 0) ? 0 : ((100 * infoUser.nbrTotalR3) / infoUser.nbrMaxR3) / 100.0f;
		textRessource3.text = "" + infoUser.nbrTotalR3;
		textRessource4.text = "" + infoUser.nbrTotalR4;
	}
}
