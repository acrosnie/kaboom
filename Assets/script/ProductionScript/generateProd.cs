using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine.UI;
using System.IO;
using System.Linq;

public class GenerateProd : MonoBehaviour
{
	public Texture2D[] missileTexture;
	public Transform[] buttonBuildingList;
	public int sizeX;
	public int sizeY;
	public Text textGenerate;
	public Case[,] caseObject;
	public BuildingInfoNbr[] buildingInfoNbrObject;
	public List<Transform> missileConstruction;
	public Transform slotPrefabGeosLabUi;
	public Transform tileset;
	public Transform slotPrefabBuildingUi;
	public Transform parentSlotPrefabBuildingUiAtt;
	public Transform parentSlotPrefabBuildingUiDef;
	public Transform slotPrefabMissileConstructUi;
	public Transform parentSlotPrefabMissileConstructUi;
	public Transform parentSlotPrefabGeosLabUi;
	public Transform slotPrefabMissileAttLine;
	public Transform parentSlotPrefabMissileAttLineUi;
	public Transform parentSlotPrefabMissileDefLineUi;
	public MissilesReview[] missilesReview;
	public int levelGeoLab;
	public int missileFactoryLevel;
	public int levelTownHall;
	private Transform terrain;
	private Transform script;
	private JSONNode buildingLevelListJson;
	private JSONNode buildingPlayerListJson;
	public JSONNode missilePlayerConstructJson;
	public Transform mainScript;
	private InfoUser infoUser;

	void Start ()
	{
		//textGenerate.gameObject.SetActive(true);
		sizeX = 50;
		sizeY = 50;
		terrain = GameObject.Find("terrain").transform;
		if (!GameObject.Find("mainScript"))
			return ;
		mainScript = GameObject.Find("mainScript").transform;
		buildingInfoNbrObject = mainScript.GetComponent<GenerateResource>().buildingInfoNbrObject;
		buildingLevelListJson = mainScript.GetComponent<GenerateResource>().buildingLevelListJson;
		buildingPlayerListJson = mainScript.GetComponent<GenerateResource>().buildingPlayerListJson;
		missilePlayerConstructJson = mainScript.GetComponent<GenerateResource>().missilePlayerConstructJson;
		script = GameObject.Find("script").transform;
		script.GetComponent<SelectBuildingProd>().building = mainScript.GetComponent<GenerateResource>().prefabBuildingListProd;
		caseObject = new Case[sizeY, sizeX];
		createMapMatrix();
		missilesReview = mainScript.GetComponent<GenerateResource>().missilesReview;
		generateBuildingPlayer();
		setNbrMaxInBuildingInfoNbrObject();
		infoUser = GameObject.Find("script").GetComponent<InfoUser>();
		infoUser.nbrTotalR4 = mainScript.GetComponent<GenerateResource>().playerInfoJson["nbr_ressource4"].AsInt;
		script.GetComponent<GenerateProd>().fillInfoUserRessource();
		generateUi();
		reorderSprite();
		GetComponent<MissileProdScript>().generateMissileConstructionLine();
		//textGenerate.gameObject.SetActive(false);
	}

	void addInBuildingInfoNbrObject(string name)
	{
		for (int i = 0; i < buildingInfoNbrObject.Length; i++)
		{
			if ((string)buildingInfoNbrObject[i].name == name)
			{
				buildingInfoNbrObject[i].nbr += 1;
				break ;
			}
		}
	}

	public int getNbrBuilding(string name)
	{
		for (int i = 0; i < buildingInfoNbrObject.Length; i++)
		{
			if ((string)buildingInfoNbrObject[i].name == name)
				return (buildingInfoNbrObject[i].nbr);
		}
		return (0);
	}

	public int getNbrMaxBuilding(string name)
	{
		for (int i = 0; i < buildingInfoNbrObject.Length; i++)
		{
			if ((string)buildingInfoNbrObject[i].name == name)
			{
				for (int j = 0; j < buildingInfoNbrObject[i].level.Length; j++)
				{
					if (buildingInfoNbrObject[i].level[j] == levelTownHall)
						return (buildingInfoNbrObject[i].nbrForLevel[j]);
				}
			}
		}
		return (0);
	}

	public int getNextLevelTownHallForBuyBuilding(string name)
	{
		for (int i = 0; i < buildingInfoNbrObject.Length; i++)
		{
			if ((string)buildingInfoNbrObject[i].name == name)
			{
				for (int ii = 0; ii < buildingInfoNbrObject[i].level.Length; ii++)
				{
					if (buildingInfoNbrObject[i].nbrForLevel[ii] > buildingInfoNbrObject[i].nbr && buildingInfoNbrObject[i].nbrForLevel[ii] > 0)
					{
						return (buildingInfoNbrObject[i].level[ii]);
					}
				}
			}
		}
		return (-1);
	}

	void setNbrMaxInBuildingInfoNbrObject()
	{
		script.GetComponent<InfoUser>().level = levelTownHall;
		int nbrMax = 0;

		for (int i = 0; i < buildingLevelListJson.Count; i++)
		{
			string nbrBuildingLevel = buildingLevelListJson[i]["building_list"]["level_nbr"];
			if (nbrBuildingLevel == null)
				return ;
			string name = (string)buildingLevelListJson[i]["building_list"]["name"];
			var split = nbrBuildingLevel.Split(',');
			foreach (var s in split)
			{
				var ss = s.Split(':');
				if (int.Parse(ss[0]) == levelTownHall)
				{
					nbrMax = int.Parse(ss[1]);
				}
			}
			for (int j = 0; j < buildingInfoNbrObject.Length; j++)
			{
				if (name == buildingInfoNbrObject[j].name)
				{
					buildingInfoNbrObject[j].nbrMax = nbrMax;
					break ;
				}
			}
		}
	}

	public void reUpdateBuildingButtonBuild()
	{
		int nbrValide = 0;
		foreach (var b in buttonBuildingList)
		{
			if (b == null)
				continue ;
			if (b != null && b.Find("Number"))
				b.Find("Number").GetComponent<Text>().text = getNbrBuilding(b.Find("Name").GetComponent<Text>().text) + "/" + getNbrMaxBuilding(b.Find("Name").GetComponent<Text>().text);
			if (getNbrBuilding(b.Find("Name").GetComponent<Text>().text) >= getNbrMaxBuilding(b.Find("Name").GetComponent<Text>().text))
			{
				b.Find("Image").GetComponent<RawImage>().color = Color.black;
				b.Find("Indisponible").gameObject.SetActive(true);
				b.Find("Ressource1Image").gameObject.SetActive(false);
				b.Find("Ressource2Image").gameObject.SetActive(false);
				b.Find("ConstructionTime").gameObject.SetActive(false);
				b.Find("Number").gameObject.SetActive(false);
					var nextLevel = getNextLevelTownHallForBuyBuilding(b.Find("Name").GetComponent<Text>().text);
					if (nextLevel == -1)
						b.Find("Indisponible").GetComponent<Text>().text = "Vous avez deja construit le nombre maximum de ces batiments.";
					else if (getNbrBuilding(b.Find("Name").GetComponent<Text>().text) == 0)
						b.Find("Indisponible").GetComponent<Text>().text = "Q.G niveau " + getNextLevelTownHallForBuyBuilding(b.Find("Name").GetComponent<Text>().text) + " requis pour y acceder !";
					else
						b.Find("Indisponible").GetComponent<Text>().text = "Q.G niveau " + getNextLevelTownHallForBuyBuilding(b.Find("Name").GetComponent<Text>().text) + " requis pour en construire plus !";
			}
			else
			{
				var r = mainScript.GetComponent<GenerateResource>().prefabBuildingListProd;
				foreach (var val in r)
				{
						if (val && val.GetComponent<BuildingInfo>() && val.GetComponent<BuildingInfo>().buyable && val.GetComponent<BuildingInfo>().level == 1 && val.GetComponent<BuildingInfo>().name == b.Find("Name").GetComponent<Text>().text)
						{
							b.Find("Ressource1Image").Find("PriceRessource1").GetComponent<Text>().color = Color.white;
							b.Find("Ressource2Image").Find("PriceRessource2").GetComponent<Text>().color = Color.white;
							if (infoUser.nbrTotalR1 < val.GetComponent<BuildingInfo>().costBuyRessource1)
								b.Find("Ressource1Image").Find("PriceRessource1").GetComponent<Text>().color = Color.red;
							if (infoUser.nbrTotalR2 < val.GetComponent<BuildingInfo>().costBuyRessource2)
								b.Find("Ressource2Image").Find("PriceRessource2").GetComponent<Text>().color = Color.red;
								var buildingInfoG = val.GetComponent<BuildingInfo>();
								var name = val.GetComponent<BuildingInfo>().name;
								var level = val.GetComponent<BuildingInfo>().level;
								b.GetComponent<Button>().onClick.RemoveAllListeners();
								if (infoUser.nbrTotalR1 < val.GetComponent<BuildingInfo>().costBuyRessource1 || infoUser.nbrTotalR2 < val.GetComponent<BuildingInfo>().costBuyRessource2)
									b.GetComponent<Button>().onClick.AddListener(delegate{ GameObject.Find("GameController").GetComponent<ConvertGlobes>().display(buildingInfoG); });
								else
									b.GetComponent<Button>().onClick.AddListener(delegate{ GameObject.Find("GameController").GetComponent<ButtonSelectBuilding>().addBuilding(name, level); });	
							b.Find("Image").GetComponent<RawImage>().color = Color.white;
							b.Find("Indisponible").gameObject.SetActive(false);
							b.Find("Ressource1Image").gameObject.SetActive(true);
							b.Find("Ressource2Image").gameObject.SetActive(true);
							b.Find("ConstructionTime").gameObject.SetActive(true);
							b.Find("Number").gameObject.SetActive(true);
							nbrValide++;
							break ;
					}
				}
			}
		}
		GameObject.Find("Canvas").transform.Find("BuildButton").transform.Find("Number").transform.Find("Text").GetComponent<Text>().text = "" + nbrValide;
	}

	void generateUIBuilding()
	{
		int nbrValide = 0;
		int cAtt = 0;
		int cDef = 0;
		int i = 0;
		var r = mainScript.GetComponent<GenerateResource>().prefabBuildingListProd;
		buttonBuildingList = new Transform[r.Length];
		foreach (var val in r)
		{
			if (val && val.GetComponent<BuildingInfo>() && val.GetComponent<BuildingInfo>().buyable && val.GetComponent<BuildingInfo>().level == 1)
			{
				var slot = Instantiate(slotPrefabBuildingUi, slotPrefabBuildingUi.position, slotPrefabBuildingUi.rotation) as Transform;
				var x = slot.position.x;
				var y = slot.position.y;
				var z = slot.position.z;
				slot.Find("Image").GetComponent<RawImage>().texture = val.GetComponent<BuildingInfo>().texture;
				// float scaleY = val.GetComponent<BuildingInfo>().texture.height / (val.GetComponent<BuildingInfo>().texture.width / slot.Find("Image").GetComponent<RectTransform>().sizeDelta.x);
				// slot.Find("Image").GetComponent<RectTransform>().sizeDelta = new Vector2(slot.Find("Image").GetComponent<RectTransform>().sizeDelta.x, scaleY);
				slot.Find("Name").GetComponent<Text>().text = val.GetComponent<BuildingInfo>().name;
				slot.Find("ConstructionTime").GetComponent<Text>().text = "" + val.GetComponent<BuildingInfo>().constructionTime;
				slot.Find("Number").GetComponent<Text>().text = getNbrBuilding(val.GetComponent<BuildingInfo>().name) + "/" + getNbrMaxBuilding(val.GetComponent<BuildingInfo>().name);
				if (script.GetComponent<GenerateProd>().getNbrBuilding(slot.Find("Name").GetComponent<Text>().text) >= script.GetComponent<GenerateProd>().getNbrMaxBuilding(slot.Find("Name").GetComponent<Text>().text))
				{
					slot.Find("Image").GetComponent<RawImage>().color = Color.black;
					slot.Find("Indisponible").gameObject.SetActive(true);
					slot.Find("Ressource1Image").gameObject.SetActive(false);
					slot.Find("Ressource2Image").gameObject.SetActive(false);
					slot.Find("ConstructionTime").gameObject.SetActive(false);
					slot.Find("Number").gameObject.SetActive(false);
					var nextLevel = getNextLevelTownHallForBuyBuilding(slot.Find("Name").GetComponent<Text>().text);
					if (nextLevel == -1)
						slot.Find("Indisponible").GetComponent<Text>().text = "Vous avez deja construit le nombre maximum de ces batiments.";
					else if (getNbrBuilding(val.GetComponent<BuildingInfo>().name) == 0)
						slot.Find("Indisponible").GetComponent<Text>().text = "Q.G niveau " + getNextLevelTownHallForBuyBuilding(slot.Find("Name").GetComponent<Text>().text) + " requis pour y acceder !";
					else
						slot.Find("Indisponible").GetComponent<Text>().text = "Q.G niveau " + getNextLevelTownHallForBuyBuilding(slot.Find("Name").GetComponent<Text>().text) + " requis pour en construire plus !";
				}
				else
				{
					nbrValide++;
					slot.Find("Image").GetComponent<RawImage>().color = Color.white;
					slot.Find("Indisponible").gameObject.SetActive(false);
					slot.Find("Ressource1Image").gameObject.SetActive(true);
					slot.Find("Ressource2Image").gameObject.SetActive(true);
					slot.Find("ConstructionTime").gameObject.SetActive(true);
					slot.Find("Number").gameObject.SetActive(true);
					slot.Find("Ressource1Image").Find("PriceRessource1").GetComponent<Text>().text = "" + val.GetComponent<BuildingInfo>().costBuyRessource1;
					slot.Find("Ressource2Image").Find("PriceRessource2").GetComponent<Text>().text = "" + val.GetComponent<BuildingInfo>().costBuyRessource2;
					slot.Find("Ressource1Image").Find("PriceRessource1").GetComponent<Text>().color = Color.white;
					slot.Find("Ressource2Image").Find("PriceRessource2").GetComponent<Text>().color = Color.white;
					if (infoUser.nbrTotalR1 < val.GetComponent<BuildingInfo>().costBuyRessource1)
						slot.Find("Ressource1Image").Find("PriceRessource1").GetComponent<Text>().color = Color.red;
					if (infoUser.nbrTotalR2 < val.GetComponent<BuildingInfo>().costBuyRessource2)
						slot.Find("Ressource2Image").Find("PriceRessource2").GetComponent<Text>().color = Color.red;

				}
				var buildingInfoG = val.GetComponent<BuildingInfo>();
				var name = val.GetComponent<BuildingInfo>().name;
				var level = val.GetComponent<BuildingInfo>().level;
				slot.GetComponent<Button>().onClick.RemoveAllListeners();
				if (infoUser.nbrTotalR1 < val.GetComponent<BuildingInfo>().costBuyRessource1 || infoUser.nbrTotalR2 < val.GetComponent<BuildingInfo>().costBuyRessource2)
					slot.GetComponent<Button>().onClick.AddListener(delegate{ GameObject.Find("GameController").GetComponent<ConvertGlobes>().display(buildingInfoG); });
				else
					slot.GetComponent<Button>().onClick.AddListener(delegate{ GameObject.Find("GameController").GetComponent<ButtonSelectBuilding>().addBuilding(name, level); });
				if (val.GetComponent<BuildingInfo>().att)
				{
					parentSlotPrefabBuildingUiAtt.GetComponent<RectTransform>().sizeDelta = new Vector2((cAtt + 1) * 450, parentSlotPrefabBuildingUiAtt.GetComponent<RectTransform>().sizeDelta.y);
					slot.SetParent(parentSlotPrefabBuildingUiAtt);
					slot.localScale = new Vector3(1, 1, 1);
					slot.localPosition = new Vector3(cAtt * 450, y, z);
					cAtt++;
				}
				else
				{
					parentSlotPrefabBuildingUiDef.GetComponent<RectTransform>().sizeDelta = new Vector2((cDef + 1) * 450, parentSlotPrefabBuildingUiDef.GetComponent<RectTransform>().sizeDelta.y);
					slot.SetParent(parentSlotPrefabBuildingUiDef);
					slot.localScale = new Vector3(1, 1, 1);
					slot.localPosition = new Vector3(cDef * 450, y, z);
					cDef++;
				}
				buttonBuildingList[i] = slot;
				i++;
			}
		}
		GameObject.Find("Canvas").transform.Find("BuildButton").transform.Find("Number").transform.Find("Text").GetComponent<Text>().text = "" + nbrValide;
	}

	public void reGenerateUIMissile(int missileLevelListIdOld = -1, int missileLevelListIdNew = -1)
	{
		foreach (Transform child in parentSlotPrefabMissileAttLineUi)
		{
			Destroy(child.gameObject);
		}
		foreach (Transform child in parentSlotPrefabMissileDefLineUi)
		{
			Destroy(child.gameObject);
		}
		var e1 = parentSlotPrefabMissileAttLineUi.parent.parent.Find("LineEquipedScroll").Find("Selection").transform;
		var e2 = parentSlotPrefabMissileDefLineUi.parent.parent.Find("LineEquipedScroll").Find("Selection").transform;
		foreach (Transform a in e1)
		{
			a.GetComponent<RawImage>().texture = null;
		}
		foreach (Transform a in e2)
		{
			a.GetComponent<RawImage>().texture = null;
		}
		foreach (Transform child in parentSlotPrefabGeosLabUi)
		{
			Destroy(child.gameObject);
		}
		generateUIMissileReConstruct(missileLevelListIdOld, missileLevelListIdNew);
		generateUIMissileAttack();
		generateUIMissileDefense();
		generateUIGeosLab();
	}

	void generateUIMissileReConstruct(int missileLevelListIdOld, int missileLevelListIdNew)
	{
		if (missileLevelListIdOld >= 0 && missileLevelListIdNew >= 0)
		{
			foreach (Transform child in parentSlotPrefabMissileConstructUi)
			{
				if (child.GetComponent<SmallInfoUiMissile>().missileLevelListId == missileLevelListIdOld)
					child.GetComponent<SmallInfoUiMissile>().missileLevelListId = missileLevelListIdNew;
			}
		}
		foreach (Transform child in parentSlotPrefabMissileConstructUi)
		{
			var missileLevelListId = child.GetComponent<SmallInfoUiMissile>().missileLevelListId;
			for (int i = 0; i < missilePlayerConstructJson.Count; i++)
			{
				if (missilePlayerConstructJson[i]["missile_level_list"]["id"].AsInt == missileLevelListId)
				{
					if (missilePlayerConstructJson[i]["missile_list"]["lvl_missile_fact"].AsInt > missileFactoryLevel)
					{
						child.Find("Price Ressource 2").GetComponent<Text>().text = "";
						child.Find("Message Bloque").GetComponent<Text>().text = "Débloqué au niveau " + missilePlayerConstructJson[i]["missile_list"]["lvl_missile_fact"].AsInt;
						child.GetComponent<Button>().interactable = false;
						child.Find("Image").GetComponent<RawImage>().color = new Color(0, 0, 0);
					}
					else
					{
						child.Find("Image").GetComponent<RawImage>().color = new Color(1, 1, 1);
						child.Find("Price Ressource 2").GetComponent<Text>().text = (string)missilePlayerConstructJson[i]["missile_level_list"]["ressource2_cost_build"];
						child.Find("Message Bloque").GetComponent<Text>().text = "";
						child.GetComponent<Button>().interactable = true;
					}
					if (GetComponent<InfoUser>().nbrTotalR2 < (int)missilePlayerConstructJson[i]["missile_level_list"]["ressource2_cost_build"].AsInt)
						child.Find("Price Ressource 2").GetComponent<Text>().color = Color.red;
					else
						child.Find("Price Ressource 2").GetComponent<Text>().color = Color.white;
					var id = missilePlayerConstructJson[i]["missile_level_list"]["id"].AsInt;
					var name = (string)missilePlayerConstructJson[i]["missile_level_list"]["missile_list"]["name"];
					child.GetComponent<Button>().onClick.RemoveAllListeners();
					child.GetComponent<Button>().onClick.AddListener(delegate{ GameObject.Find("script").GetComponent<MissileProdScript>().constructMissile(id, name); });
					child.Find("Info").GetComponent<Button>().onClick.RemoveAllListeners();
					foreach	(var m in missilesReview)
					{
						if (m.missile.GetComponent<InfoMissile>().missileLevelListId == id)
						{
							var infoMissile = m.missile.GetComponent<InfoMissile>();
							child.Find("Info").GetComponent<Button>().onClick.AddListener(delegate{ GameObject.Find("GameController").GetComponent<MissileInfoOnOff>().active(infoMissile); });
							break ;
						}
					}
				}
			}
		}
	}

	void generateUIMissileConstruct()
	{
		var startX = -644;
		var startY = 350;
		var c = 0;
		var y = 0;
		var x = 0;

		for (int i = 0; i < missilePlayerConstructJson.Count; i++)
		{
			var slot = Instantiate(slotPrefabMissileConstructUi, slotPrefabMissileConstructUi.position, slotPrefabMissileConstructUi.rotation) as Transform;
			foreach (var tt in missileTexture)
			{
				if ((string)missilePlayerConstructJson[i]["missile_list"]["name"] == tt.name)
				{
					slot.Find("Image").GetComponent<RawImage>().texture = tt;
					break ;
				}
			}
			if (missilePlayerConstructJson[i]["missile_list"]["lvl_missile_fact"].AsInt > missileFactoryLevel)
			{
				slot.Find("Price Ressource 2").GetComponent<Text>().text = "";
				slot.Find("Message Bloque").GetComponent<Text>().text = "Débloqué au niveau " + missilePlayerConstructJson[i]["missile_list"]["lvl_missile_fact"].AsInt;
				slot.GetComponent<Button>().interactable = false;
				slot.Find("Image").GetComponent<RawImage>().color = new Color(0, 0, 0);
			}
			else
			{
				slot.Find("Image").GetComponent<RawImage>().color = new Color(1, 1, 1);
				slot.Find("Price Ressource 2").GetComponent<Text>().text = (string)missilePlayerConstructJson[i]["missile_level_list"]["ressource2_cost_build"];
				slot.Find("Message Bloque").GetComponent<Text>().text = "";
			}
			slot.GetComponent<SmallInfoUiMissile>().name = (string)missilePlayerConstructJson[i]["missile_list"]["name"];
			slot.GetComponent<SmallInfoUiMissile>().missileLevelListId = missilePlayerConstructJson[i]["missile_level_list"]["id"].AsInt;
			slot.SetParent(parentSlotPrefabMissileConstructUi);
			slot.localScale = new Vector3(1, 1, 1);
			if (GetComponent<InfoUser>().nbrTotalR2 < (int)missilePlayerConstructJson[i]["missile_level_list"]["ressource2_cost_build"].AsInt)
				slot.Find("Price Ressource 2").GetComponent<Text>().color = Color.red;
			else
				slot.Find("Price Ressource 2").GetComponent<Text>().color = Color.white;
			var id = missilePlayerConstructJson[i]["missile_level_list"]["id"].AsInt;
			var name = (string)missilePlayerConstructJson[i]["missile_level_list"]["missile_list"]["name"];
			slot.GetComponent<Button>().onClick.RemoveAllListeners();
			slot.GetComponent<Button>().onClick.AddListener(delegate{ GameObject.Find("script").GetComponent<MissileProdScript>().constructMissile(id, name); });
			slot.Find("Info").GetComponent<Button>().onClick.RemoveAllListeners();
			foreach	(var m in missilesReview)
			{
				if (m.missile.GetComponent<InfoMissile>().missileLevelListId == id)
				{
					var infoMissile = m.missile.GetComponent<InfoMissile>();
					slot.Find("Info").GetComponent<Button>().onClick.AddListener(delegate{ GameObject.Find("GameController").GetComponent<MissileInfoOnOff>().active(infoMissile); });
					break ;
				}
			}
			if (c == 5)
			{
				c = 0;
				y++;
				x = 0;
			}
			parentSlotPrefabMissileConstructUi.GetComponent<RectTransform>().sizeDelta = new Vector2(parentSlotPrefabMissileConstructUi.GetComponent<RectTransform>().sizeDelta.x, (y + 1) * 340);
			slot.GetComponent<RectTransform>().anchoredPosition = new Vector2(startX + (x * 322.5f), startY - (y * 322.5f));
			x++;
			c++;
		}
	}

	void generateUIMissileAttack()
	{
		var startX = -525;
		var startY = -15;
		var c = 0;
		var y = 0;
		var x = 0;

		var e = parentSlotPrefabMissileAttLineUi.parent.parent.Find("LineEquipedScroll").Find("Selection").transform;
		foreach	(var m in missilesReview)
		{
			if (!m.missile.GetComponent<InfoMissile>().att || !GetComponent<MissileProdScript>().hasMissileId(m.missile.GetComponent<InfoMissile>().missileLevelListId))
				continue ;
			var slot = Instantiate(slotPrefabMissileAttLine, slotPrefabMissileAttLine.position, slotPrefabMissileAttLine.rotation) as Transform;
			foreach (var tt in missileTexture)
			{
				if (m.missile.GetComponent<InfoMissile>().name == tt.name)
				{
					slot.Find("Image").GetComponent<RawImage>().texture = tt;
					break ;
				}
			}
			slot.Find("Info").GetComponent<Button>().onClick.RemoveAllListeners();
			var infoMissile = m.missile.GetComponent<InfoMissile>();
			slot.Find("Info").GetComponent<Button>().onClick.AddListener(delegate{ GameObject.Find("GameController").GetComponent<MissileInfoOnOff>().active(infoMissile); });
			slot.GetComponent<SmallInfoUiMissile>().name = m.missile.GetComponent<InfoMissile>().name;
			slot.GetComponent<SmallInfoUiMissile>().missileLevelListId = m.missile.GetComponent<InfoMissile>().missileLevelListId;
			slot.GetComponent<SmallInfoUiMissile>().att = true;
			// if (!GetComponent<MissileProdScript>().hasMissileId(missilePlayerConstructJson[i]["missile_level_list"]["id"].AsInt))
			// 	slot.Find("Image").GetComponent<RawImage>().color = new Color(0.03f, 0.03f, 0.03f);
			slot.Find("NumberInStock").GetComponentInChildren<Text>().text = "" + GetComponent<MissileProdScript>().nbrMissileId(m.missile.GetComponent<InfoMissile>().missileLevelListId);
			slot.SetParent(parentSlotPrefabMissileAttLineUi);
			slot.localScale = new Vector3(1, 1, 1);
			if (c == 4)
			{
				c = 0;
				y++;
				x = 0;
			}
			parentSlotPrefabMissileAttLineUi.GetComponent<RectTransform>().sizeDelta = new Vector2(parentSlotPrefabMissileAttLineUi.GetComponent<RectTransform>().sizeDelta.x, (y + 1) * 370.0f);
			slot.GetComponent<RectTransform>().anchoredPosition = new Vector2(startX + (x * 350f), startY - (y * 350f));
			x++;
			c++;
			if (GetComponent<MissileProdScript>().isEquiped(m.missile.GetComponent<InfoMissile>().missileLevelListId, true))
			{
				foreach (Transform a in e)
				{
					if (a != null && a.GetComponent<RawImage>().texture == null)
					{
						a.GetComponent<RawImage>().texture = slot.Find("Image").GetComponent<RawImage>().texture;
						break ;
					}
				}
			}
		}
	}

	void generateUIMissileDefense()
	{
		var startX = -525;
		var startY = -15;
		var c = 0;
		var y = 0;
		var x = 0;

		var e = parentSlotPrefabMissileDefLineUi.parent.parent.Find("LineEquipedScroll").Find("Selection").transform;
		foreach	(var m in missilesReview)
		{
			if (!m.missile.GetComponent<InfoMissile>().def || !GetComponent<MissileProdScript>().hasMissileId(m.missile.GetComponent<InfoMissile>().missileLevelListId))
				continue ;
			var slot = Instantiate(slotPrefabMissileAttLine, slotPrefabMissileAttLine.position, slotPrefabMissileAttLine.rotation) as Transform;
			foreach (var tt in missileTexture)
			{
				if (m.missile.GetComponent<InfoMissile>().name == tt.name)
				{
					slot.Find("Image").GetComponent<RawImage>().texture = tt;
					break ;
				}
			}
			slot.Find("Info").GetComponent<Button>().onClick.RemoveAllListeners();
			var infoMissile = m.missile.GetComponent<InfoMissile>();
			slot.Find("Info").GetComponent<Button>().onClick.AddListener(delegate{ GameObject.Find("GameController").GetComponent<MissileInfoOnOff>().active(infoMissile); });
			slot.GetComponent<SmallInfoUiMissile>().name = m.missile.GetComponent<InfoMissile>().name;
			slot.GetComponent<SmallInfoUiMissile>().missileLevelListId = m.missile.GetComponent<InfoMissile>().missileLevelListId;
			slot.GetComponent<SmallInfoUiMissile>().att = false;
			// if (!GetComponent<MissileProdScript>().hasMissileId(missilePlayerConstructJson[i]["missile_level_list"]["id"].AsInt))
			// 	slot.Find("Image").GetComponent<RawImage>().color = new Color(0.03f, 0.03f, 0.03f);
			slot.Find("NumberInStock").GetComponentInChildren<Text>().text = "" + GetComponent<MissileProdScript>().nbrMissileId(m.missile.GetComponent<InfoMissile>().missileLevelListId);
			slot.SetParent(parentSlotPrefabMissileDefLineUi);
			slot.localScale = new Vector3(1, 1, 1);
			if (c == 4	)
			{
				c = 0;
				y++;
				x = 0;
			}
			parentSlotPrefabMissileDefLineUi.GetComponent<RectTransform>().sizeDelta = new Vector2(parentSlotPrefabMissileDefLineUi.GetComponent<RectTransform>().sizeDelta.x, (y + 1) * 370f);
			slot.GetComponent<RectTransform>().anchoredPosition = new Vector2(startX + (x * 337.5f), startY - (y * 337.5f));
			x++;
			c++;
			if (GetComponent<MissileProdScript>().isEquiped(m.missile.GetComponent<InfoMissile>().missileLevelListId, false))
			{
				foreach (Transform a in e)
				{
					if (a != null && a.GetComponent<RawImage>().texture == null)
					{
						a.GetComponent<RawImage>().texture = slot.Find("Image").GetComponent<RawImage>().texture;
						break ;
					}
				}
			}
		}
	}

	Transform getMissileTransform(string name, int level)
	{
		foreach (var m in missilesReview)
		{
			if (m.missile.GetComponent<InfoMissile>().name == name && m.missile.GetComponent<InfoMissile>().missileLevel == level)
				return (m.missile);
		}
		return (null);
	}

	void generateUIGeosLab ()
	{
		var startX = -540;
		var startY = 560;
		var c = 0;
		var y = 0;
		var x = 0;

		for (int i = 0; i < missilePlayerConstructJson.Count; i++)
		{
			var actualMissile = getMissileTransform((string)missilePlayerConstructJson[i]["missile_list"]["name"], missilePlayerConstructJson[i]["missile_level_list"]["missile_level"].AsInt);
			var nextMissile = getMissileTransform((string)missilePlayerConstructJson[i]["missile_list"]["name"], missilePlayerConstructJson[i]["missile_level_list"]["missile_level"].AsInt + 1);
			var slot = Instantiate(slotPrefabGeosLabUi, slotPrefabGeosLabUi.position, slotPrefabGeosLabUi.rotation) as Transform;
			foreach (var tt in missileTexture)
			{
				if ((string)missilePlayerConstructJson[i]["missile_list"]["name"] == tt.name)
				{
					slot.Find("Image").GetComponent<RawImage>().texture = tt;
					break ;
				}
			}
			
			if (nextMissile == null || actualMissile == null)
			{
				slot.Find("Ressource 2 Image").gameObject.SetActive(false);
				slot.Find("Info").gameObject.SetActive(false);
				slot.Find("Price Ressource 2").gameObject.SetActive(false);
				slot.Find("errorMessage").gameObject.SetActive(true);
				slot.Find("Image").GetComponent<RawImage>().color = Color.red;
				slot.Find("errorMessage").GetComponent<Text>().text = "Le missile "+ missilePlayerConstructJson[i]["missile_list"]["name"] + " n'existe pas dans la bdd.";
				if (actualMissile != null)
				{
					slot.Find("Image").GetComponent<RawImage>().color = Color.black;
					slot.Find("errorMessage").GetComponent<Text>().text = "Pas d'amelioration existante pour le missile  " + missilePlayerConstructJson[i]["missile_list"]["name"];
				}
			}
			else if (nextMissile.GetComponent<InfoMissile>().missileFactLevel > missileFactoryLevel) // si le missile n'a pas ete debloque
			{
				slot.Find("Ressource 2 Image").gameObject.SetActive(false);
				slot.Find("Price Ressource 2").gameObject.SetActive(false);
				slot.Find("Info").gameObject.SetActive(false);
				slot.Find("Image").GetComponent<RawImage>().color = new Color(0, 0, 0);
				slot.Find("errorMessage").gameObject.SetActive(true);
				slot.Find("errorMessage").GetComponent<Text>().text = "Le missile doit d'abord etre debloque dans la missile factory !";
			}
			else if (missilePlayerConstructJson[i]["upgrade"].AsInt == 1) // si le missile est deja entrain de monter de niveau
			{
				slot.Find("Ressource 2 Image").gameObject.SetActive(false);
				slot.Find("Price Ressource 2").gameObject.SetActive(false);
				slot.Find("Info").gameObject.SetActive(true);
				var infoMissileOld = actualMissile.GetComponent<InfoMissile>();
				var infoMissileNew = nextMissile.GetComponent<InfoMissile>();
				slot.Find("Image").GetComponent<RawImage>().color = new Color(0.3f, 0.3f, 0.3f, 0.6f);
				slot.Find("Info").GetComponent<Button>().onClick.RemoveAllListeners();
				slot.Find("Info").GetComponent<Button>().onClick.AddListener(delegate{ GameObject.Find("GameController").GetComponent<MissileInfoOnOff>().active(infoMissileOld); });
				slot.Find("errorMessage").gameObject.SetActive(true);
				slot.Find("errorMessage").GetComponent<Text>().text = "Amelioration en cours.";
				var buildings = GameObject.FindGameObjectsWithTag("building");
				foreach (GameObject b in buildings)
				{
					if (b.GetComponent<BuildingInfo>().name == "Geo's Lab")
					{
						b.GetComponent<BuildingInfo>().infoMissileOld = infoMissileOld;
						b.GetComponent<BuildingInfo>().infoMissileNew = infoMissileNew;
						b.GetComponent<BuildingInfo>().missileUpgrade = true;
						b.GetComponent<BuildingInfo>().timeBetweenStartUpgradeMissile = missilePlayerConstructJson[i]["difference_in_seconds_for_upgrade"].AsInt;
						b.GetComponent<BuildingInfo>().missileName = missilePlayerConstructJson[i]["missile_list"]["name"];
						b.GetComponent<BuildingInfo>().totalInstantMissile = actualMissile.GetComponent<InfoMissile>().intantR2Upgrade + actualMissile.GetComponent<InfoMissile>().intantTimeUpgrade;
						b.GetComponent<BuildingInfo>().upgradeTimeMissile = actualMissile.GetComponent<InfoMissile>().upgradeTime;
						b.GetComponent<BuildingInfo>().costRessource4UpgradeLeftMissile = (int)(actualMissile.GetComponent<InfoMissile>().intantR2Upgrade + actualMissile.GetComponent<InfoMissile>().intantTimeUpgrade);
					}
				}
			}
			else if (nextMissile.GetComponent<InfoMissile>().labUnlockLevel > levelGeoLab) // si le missile attend une montee de niveau du geoLab
			{
				slot.Find("Ressource 2 Image").gameObject.SetActive(false);
				slot.Find("Price Ressource 2").gameObject.SetActive(false);
				slot.Find("Info").gameObject.SetActive(true);
				var infoMissileOld = actualMissile.GetComponent<InfoMissile>();
				slot.Find("Image").GetComponent<RawImage>().color = new Color(0.3f, 0.3f, 0.3f, 0.6f);
				slot.Find("Info").GetComponent<Button>().onClick.RemoveAllListeners();
				slot.Find("Info").GetComponent<Button>().onClick.AddListener(delegate{ GameObject.Find("GameController").GetComponent<MissileInfoOnOff>().active(infoMissileOld); });
				slot.Find("errorMessage").gameObject.SetActive(true);
				slot.Find("errorMessage").GetComponent<Text>().text = "Niveau " + nextMissile.GetComponent<InfoMissile>().labUnlockLevel + " de Geo's Lab requis ameliorer ce missile.";
			}
			else
			{
				slot.Find("errorMessage").gameObject.SetActive(false);
				slot.Find("Ressource 2 Image").gameObject.SetActive(true);
				slot.Find("Price Ressource 2").gameObject.SetActive(true);
				slot.Find("Info").gameObject.SetActive(true);
				var infoMissileOld = actualMissile.GetComponent<InfoMissile>();
				var infoMissileNew = nextMissile.GetComponent<InfoMissile>();
				slot.Find("Image").GetComponent<Button>().onClick.RemoveAllListeners();
				slot.Find("Price Ressource 2").Find("Text").GetComponent<Text>().text = "" + actualMissile.GetComponent<InfoMissile>().ressource2CostUpgrade;
				if (actualMissile.GetComponent<InfoMissile>().ressource2CostUpgrade <= infoUser.nbrTotalR2)
					slot.Find("Price Ressource 2").Find("Text").GetComponent<Text>().color = Color.white;
				else
					slot.Find("Price Ressource 2").Find("Text").GetComponent<Text>().color = Color.red;
				slot.Find("Image").GetComponent<Button>().onClick.AddListener(delegate{ GameObject.Find("GameController").GetComponent<MissileUpgradeOnOff>().active(infoMissileOld, infoMissileNew); });
				slot.Find("Info").GetComponent<Button>().onClick.RemoveAllListeners();
				slot.Find("Info").GetComponent<Button>().onClick.AddListener(delegate{ GameObject.Find("GameController").GetComponent<MissileInfoOnOff>().active(infoMissileOld); });
				slot.Find("Image").GetComponent<RawImage>().color = new Color(1, 1, 1);
			}
			slot.SetParent(parentSlotPrefabGeosLabUi);
			slot.localScale = new Vector3(1, 1, 1);
			if (c == 4)
			{
				c = 0;
				y++;
				x = 0;
			}
			parentSlotPrefabGeosLabUi.GetComponent<RectTransform>().sizeDelta = new Vector2(parentSlotPrefabGeosLabUi.GetComponent<RectTransform>().sizeDelta.x, (y + 1) * 380);
			slot.GetComponent<RectTransform>().anchoredPosition = new Vector2(startX + (x * 360f), startY - (y * 360f));
			x++;
			c++;
		}
	}

	void generateUIMissile()
	{
		generateUIMissileConstruct();
		generateUIMissileAttack();
		generateUIMissileDefense();
		generateUIGeosLab();
	}

	void generateUi()
	{
		generateUIBuilding();
		generateUIMissile();
	}

	void fillWithLevel(Transform b, int i)
	{
		int nbrR1 = (int)(buildingPlayerListJson[i]["difference_in_seconds"].AsInt * b.GetComponent<BuildingInfo>().ratioRessource1);
		int nbrR2 = (int)(buildingPlayerListJson[i]["difference_in_seconds"].AsInt * b.GetComponent<BuildingInfo>().ratioRessource2);
		int nbrR3 = (int)(buildingPlayerListJson[i]["difference_in_seconds"].AsInt * b.GetComponent<BuildingInfo>().ratioRessource3);
		b.GetComponent<BuildingInfo>().buildingPlayerId = buildingPlayerListJson[i]["id"].AsInt;
		b.GetComponent<BuildingInfo>().nbrRessource1 = buildingPlayerListJson[i]["nbr_ressource1"].AsInt + nbrR1;
		b.GetComponent<BuildingInfo>().nbrRessource2 = buildingPlayerListJson[i]["nbr_ressource2"].AsInt + nbrR2;
		b.GetComponent<BuildingInfo>().nbrRessource3 = buildingPlayerListJson[i]["nbr_ressource3"].AsInt + nbrR3;
		b.GetComponent<BuildingInfo>().upgrade = (buildingPlayerListJson[i]["upgrade"].AsInt == 0) ? false : true;
		b.GetComponent<BuildingInfo>().timeBetweenStartUpgrade = buildingPlayerListJson[i]["difference_in_seconds_for_upgrade"].AsInt;
	}

	void generatePreConstructed()
	{
		//CHECK IF NEED BUILDING
		bool a = false;
		for (int i = 0; i < buildingLevelListJson.Count; i++)
		{
			if (buildingLevelListJson[i]["pre_constructed"].AsInt == 1 && buildingLevelListJson[i]["level"].AsInt == 1)
			{
				a = false;
				var building = GameObject.FindGameObjectsWithTag("building");
				foreach (var b in building)
				{
					if (b.GetComponent<BuildingInfo>().name == (string)buildingLevelListJson[i]["building_list"]["name"])
					{
						a = true;
						break ;
					}
				}
				if (!a)
					break ;
			}
		}
		if (a)
			return ;
		// if (PlayerPrefs.GetInt("preConstructed") == PlayerPrefs.GetInt("idPreConstructed"))
		// 	return ;
		// if (PlayerPrefs.GetInt("preConstructed") == -1)
		// 	return ;
		for (int i = 0; i < buildingLevelListJson.Count; i++)
		{
			if (buildingLevelListJson[i]["pre_constructed"].AsInt == 1 && buildingLevelListJson[i]["level"].AsInt == 1)
			{
				var b = GetComponent<SelectBuildingProd>().createSelectableBuilding(0, buildingLevelListJson[i]["building_list"]["name"], buildingLevelListJson[i]["level"].AsInt, true, true);
				if (b == null)
				{
					print ("ERROR IN generatePreConstructed with building : " + buildingLevelListJson[i]["building_list"]["name"]);
					return ;
				}
				if (b.GetComponent<BuildingPlacementProd>())
				{
					if (b.GetComponent<BuildingInfo>().name == "Geo's Lab" && b.GetComponent<BuildingInfo>().level > levelGeoLab)
						levelGeoLab = b.GetComponent<BuildingInfo>().level;
					else if (b.GetComponent<BuildingInfo>().name == "Missile Factory" && b.GetComponent<BuildingInfo>().level > missileFactoryLevel)
						missileFactoryLevel = b.GetComponent<BuildingInfo>().level;
					else if (b.GetComponent<BuildingInfo>().name == "Town Hall" && b.GetComponent<BuildingInfo>().level > levelTownHall)
						levelTownHall = b.GetComponent<BuildingInfo>().level;
					b.GetComponent<BuildingPlacementProd>().selected = false;
					b.GetComponent<BuildingPlacementProd>().lastGoodPosition = b.transform.position;
					b.GetComponent<BuildingPlacementProd>().finalPosition = b.transform.position;
					b.GetComponent<BuildingPlacementProd>().initialPosition = b.transform.position;
					b.GetComponent<BuildingPlacementProd>().oldPosition = b.transform.position;
					//b.Find("CanvasBuilding").Find("getRessource").GetComponent<Button>().onClick.AddListener(delegate{ b.GetComponent<BuildingAction>().getRessource(); });
					addInBuildingInfoNbrObject((string)buildingLevelListJson[i]["building_list"]["name"]);
				}
				fillMatrixPosition(b, true, b.GetComponent<BuildingInfo>().posMatrixX, b.GetComponent<BuildingInfo>().posMatrixY);
				GameObject.Find("script").GetComponent<SelectBuildingProd>().haveBuildingSelected = false;
				b.GetComponent<BuildingInfo>().insertThisBuilding(b.position);
				b.GetComponent<BuildingPlacementProd>().changeSpritesOrder(false);
				b.GetComponent<BuildingPlacementProd>().newBuilding = false;
			}
		}
		//PlayerPrefs.SetInt("preConstructed", PlayerPrefs.GetInt("idPreConstructed"));
	}

	void generateBuildingPlayer()
	{
		// // for read json i use SimpleJSON plugin
		if (buildingPlayerListJson == null)
			return ;
		// //loop building and place them
		for (int i = 0; i < buildingPlayerListJson.Count; i++)
		{
			var b = GetComponent<SelectBuildingProd>().createSelectableBuilding(0, buildingPlayerListJson[i]["building_level_list"]["building_list"]["name"], buildingPlayerListJson[i]["building_level_list"]["level"].AsInt, false);
			if (b == null)
			{
				print ("ERROR IN generateBuildingPlayer");
				return ;
			}
			b.position = new Vector3(buildingPlayerListJson[i]["x"].AsFloat, buildingPlayerListJson[i]["y"].AsFloat, buildingPlayerListJson[i]["z"].AsFloat);
			if (b.GetComponent<BuildingPlacementProd>())
			{
				if (b.GetComponent<BuildingInfo>().name == "Geo's Lab" && b.GetComponent<BuildingInfo>().level > levelGeoLab)
					levelGeoLab = b.GetComponent<BuildingInfo>().level;
				else if (b.GetComponent<BuildingInfo>().name == "Missile Factory" && b.GetComponent<BuildingInfo>().level > missileFactoryLevel)
					missileFactoryLevel = b.GetComponent<BuildingInfo>().level;
				else if (b.GetComponent<BuildingInfo>().name == "Town Hall" && b.GetComponent<BuildingInfo>().level > levelTownHall)
					levelTownHall = b.GetComponent<BuildingInfo>().level;
				b.GetComponent<BuildingPlacementProd>().selected = false;
				b.GetComponent<BuildingPlacementProd>().newBuilding = false;
				b.GetComponent<BuildingPlacementProd>().lastGoodPosition = b.transform.position;
				b.GetComponent<BuildingPlacementProd>().finalPosition = b.transform.position;
				b.GetComponent<BuildingPlacementProd>().initialPosition = b.transform.position;
				b.GetComponent<BuildingPlacementProd>().oldPosition = b.transform.position;
				b.GetComponent<BuildingInfo>().nbrRessource1 = (int)(buildingPlayerListJson[i]["difference_in_seconds"].AsFloat * b.GetComponent<BuildingInfo>().ratioRessource1);
				b.GetComponent<BuildingInfo>().nbrRessource2 = (int)(buildingPlayerListJson[i]["difference_in_seconds"].AsFloat * b.GetComponent<BuildingInfo>().ratioRessource2);
				b.GetComponent<BuildingInfo>().nbrRessource3 = (int)(buildingPlayerListJson[i]["difference_in_seconds"].AsFloat * b.GetComponent<BuildingInfo>().ratioRessource3);
				b.GetComponent<BuildingAction>().tempRessource1 = (float)(buildingPlayerListJson[i]["difference_in_seconds"].AsFloat * b.GetComponent<BuildingInfo>().ratioRessource1) - (int)(buildingPlayerListJson[i]["difference_in_seconds"].AsFloat * b.GetComponent<BuildingInfo>().ratioRessource1);
				b.GetComponent<BuildingAction>().tempRessource2 = (float)(buildingPlayerListJson[i]["difference_in_seconds"].AsFloat * b.GetComponent<BuildingInfo>().ratioRessource2) - (int)(buildingPlayerListJson[i]["difference_in_seconds"].AsFloat * b.GetComponent<BuildingInfo>().ratioRessource2);
				b.GetComponent<BuildingAction>().tempRessource3 = (float)(buildingPlayerListJson[i]["difference_in_seconds"].AsFloat * b.GetComponent<BuildingInfo>().ratioRessource3) - (int)(buildingPlayerListJson[i]["difference_in_seconds"].AsFloat * b.GetComponent<BuildingInfo>().ratioRessource3);
				//difference_in_seconds
				//b.Find("CanvasBuilding").Find("getRessource").GetComponent<Button>().onClick.AddListener(delegate{ b.GetComponent<BuildingAction>().getRessource(); });
				addInBuildingInfoNbrObject((string)buildingPlayerListJson[i]["building_level_list"]["building_list"]["name"]);
				//b.gameObject.SetActive(false);
			}
			fillWithLevel(b, i);
			//fillMatrixPosition(b.position, b.Find("spriteFloor").localScale.x, b.Find("spriteFloor").localScale.y, true);
			fillMatrixPosition(b, true, b.GetComponent<BuildingInfo>().posMatrixX, b.GetComponent<BuildingInfo>().posMatrixY);
			GameObject.Find("script").GetComponent<SelectBuildingProd>().haveBuildingSelected = false;
		}
		generatePreConstructed();
		//displayNbrCaseTaken();
	}

	public void reorderSprite()
	{
		var building = GameObject.FindGameObjectsWithTag("building");
		foreach (var b in building)
		{
			var posY = Mathf.RoundToInt(b.GetComponent<BuildingPlacementProd>().lastGoodPosition.y * 10f) * -1;
			var p = b.transform.Find("spriteBuilding").transform.position;
			b.transform.Find("spriteBuilding").transform.position = new Vector3(p.x, p.y, -(posY * 0.001f));
		}
	}

	void createMapMatrix()
	{
		int x = 0;
		int y = 0;

		//var containTile = new GameObject();
		//containTile.name = "containTiles";
		y = 0;
		while (y < sizeY)
		{
			x = 0;
			while (x < sizeX)
			{
				var c = new Case();
				c.position = new Vector2((x * 0.5f - (y * 0.5f)), ((-y * 0.353515625f) + (x * -0.353515625f)) + 17.335f);
				// var tiles = Instantiate(tileset, c.position, tileset.rotation) as Transform;
				// tiles.localScale = new Vector3(1, 0.7125f, 1);
				// tiles.SetParent(containTile.transform);
				c.isEmpty = true;
				if (x >= 45)
					c.isDefense = true;
				caseObject[y, x] = c;
				x++;
			}
			y++;
		}
		//containTile.transform.position = new Vector3(0, 0, 1);
		//containTile.transform.SetParent(GameObject.Find("map").transform.Find("terrain"));
		//createDefenseTerrain();
	}

	void createDefenseTerrain()
	{
		int x = 0;
		int y = 0;

		var containTile = new GameObject();
		containTile.name = "containTiles";
		y = 0;
		while (y < sizeY)
		{
			x = 0;
			while (x < sizeX)
			{
				if (x >= 45)
				{
					var c = new Case();
					c.position = new Vector2((x * 0.5f - (y * 0.5f)), ((-y * 0.353515625f) + (x * -0.353515625f)) + 17.335f);
					var tiles = Instantiate(tileset, c.position, tileset.rotation) as Transform;
					tiles.localScale = new Vector3(1, 0.7125f, 1);
					tiles.SetParent(containTile.transform);
					c.isEmpty = true;
				}
				x++;
			}
			y++;
		}
		containTile.transform.position = new Vector3(0, 0, 1);
		containTile.transform.SetParent(GameObject.Find("map").transform.Find("terrain"));
	}

	void displayNbrCaseTaken()
	{
		int i = 0;
		int x = 0;
		int y = 0;

		while (y < sizeY)
		{
			x = 0;
			while (x < sizeX)
			{
				if (!caseObject[y, x].isEmpty)
					i++;
				x++;
			}
			y++;
		}
		print (i);
	}

	public void fillMatrixPosition(Transform g, bool val, int valX, int valY)
	{
		int sx = (int)g.Find("spriteFloor").transform.localScale.x;
		int sy = (int)g.Find("spriteFloor").transform.localScale.y;
		int ssx = (int)(g.Find("spriteFloor").transform.localScale.x / 2.0f);
		int ssy = (int)(g.Find("spriteFloor").transform.localScale.y / 2.0f);

		for (int j = 0; j < sy; j++)
		{
			for (int i = 0; i < sx; i++)
			{
				var oY = (valY + ssy) - j;
				var oX = (valX - ssx) + i;
				if (oY < 0 || oY >= sizeY || oX < 0 || oX >= sizeX)
					continue ;
				caseObject[oY, oX].isEmpty = !val;
			}
		}
	}

	// public void fillMatrixPosition(Vector2 p, float sx, float sy, bool val)
	// {
	// 	float decalX = (int)((sx / 2.0f));
	// 	float decalY = (int)((sy / 2.0f));
	// 	if (decalY % 2 == 0)
	// 		decalY -= 0.5f;
	// 	Vector2 top = new Vector2(p.x, p.y + (decalY * 0.70703125f));
	// 	int x = 0;
	// 	int y = 0;

	// 	while (y < sizeY)
	// 	{
	// 		x = 0;
	// 		while (x < sizeX)
	// 		{
	// 			if (Mathf.Approximately(caseObject[y, x].position.x, top.x) && Mathf.Approximately(caseObject[y, x].position.y, top.y)) // top case
	// 			{
	// 				for (int j = 0; j < sy; j++)
	// 				{
	// 					for (int i = 0; i < sx; i++)
	// 					{
	// 						if ((j + y) < 0 || (j + y) >= sizeY || (i + x) < 0 || (i + x) >= sizeX)
	// 							continue ;
	// 						caseObject[j + y, i + x].isEmpty = !val;
	// 					}
	// 				}
	// 				break ;
	// 			}
	// 			x++;
	// 		}
	// 		y++;
	// 	}
	// }

	public void spendGlobes(int r4)
	{
		infoUser.nbrTotalR4 -= r4;
		script = GameObject.Find("script").transform;
		fillInfoUserRessource();
		reGenerateUIMissile();
		reUpdateBuildingButtonBuild();
		//spend globes on server ==> not need just need to upgrade building
	}

	public void spendRessource(int r1, int r2, int r3)
	{
		var buildings = GameObject.FindGameObjectsWithTag("building");
		bool ooo;
		foreach (GameObject b in buildings)
		{
			ooo = false;
			if (r1 == 0 && r2 == 0 && r3 == 0)
				break  ;
			while (r1 > 0 && b.GetComponent<BuildingInfo>().nbrRessource1 > 0)
			{
				r1--;
				b.GetComponent<BuildingInfo>().nbrRessource1--;
				ooo = true;
			}
			while (r2 > 0 && b.GetComponent<BuildingInfo>().nbrRessource2 > 0)
			{
				r2--;
				b.GetComponent<BuildingInfo>().nbrRessource2--;
				ooo = true;
			}
			while (r3 > 0 && b.GetComponent<BuildingInfo>().nbrRessource3 > 0)
			{
				r3--;
				b.GetComponent<BuildingInfo>().nbrRessource3--;
				ooo = true;
			}
			if (ooo)
				b.GetComponent<BuildingInfo>().updateThisBuilding(b.transform.position);
		}
		script = GameObject.Find("script").transform;
		fillInfoUserRessource();
		reGenerateUIMissile();
		reUpdateBuildingButtonBuild();
	}

	public void recupRessource(int r1, int r2, int r3)
	{
		var buildings = GameObject.FindGameObjectsWithTag("building");
		bool ooo;

		foreach (GameObject b in buildings)
		{
			ooo = false;
			if (r1 == 0 && r2 == 0 && r3 == 0)
				break  ;
			while (r1 > 0 && b.GetComponent<BuildingInfo>().nbrRessource1 <= b.GetComponent<BuildingInfo>().stockRessource1 && b.GetComponent<BuildingInfo>().stockRessource1 > 0)
			{
				r1--;
				b.GetComponent<BuildingInfo>().nbrRessource1++;
				ooo = true;
			}
			while (r2 > 0 && b.GetComponent<BuildingInfo>().nbrRessource2 <= b.GetComponent<BuildingInfo>().stockRessource2 && b.GetComponent<BuildingInfo>().stockRessource2 > 0)
			{
				r2--;
				b.GetComponent<BuildingInfo>().nbrRessource2++;
				ooo = true;
			}
			while (r3 > 0 && b.GetComponent<BuildingInfo>().nbrRessource3 <= b.GetComponent<BuildingInfo>().stockRessource3 && b.GetComponent<BuildingInfo>().stockRessource3 > 0)
			{
				r3--;
				b.GetComponent<BuildingInfo>().nbrRessource3++;
				ooo = true;
			}
			if (ooo)
				b.GetComponent<BuildingInfo>().updateThisBuilding(b.transform.position);
		}
		script = GameObject.Find("script").transform;
		fillInfoUserRessource();
		reGenerateUIMissile();
		reUpdateBuildingButtonBuild();
	}

	public void fillInfoUserRessource()
	{
		infoUser.nbrTotalR1 = 0;
		infoUser.nbrMaxR1 = 0;
		infoUser.nbrTotalR2 = 0;
		infoUser.nbrMaxR2 = 0;
		infoUser.nbrTotalR3 = 0;
		infoUser.nbrMaxR3 = 0;

		var buildings = GameObject.FindGameObjectsWithTag("building");

		foreach (GameObject b in buildings)
		{
			if (b.GetComponent<BuildingInfo>().stockRessource1 > 0)
			{
				infoUser.nbrTotalR1 += b.GetComponent<BuildingInfo>().nbrRessource1;
				infoUser.nbrMaxR1 += b.GetComponent<BuildingInfo>().stockRessource1;
			}
			if (b.GetComponent<BuildingInfo>().stockRessource2 > 0)
			{
				infoUser.nbrTotalR2 += b.GetComponent<BuildingInfo>().nbrRessource2;
				infoUser.nbrMaxR2 += b.GetComponent<BuildingInfo>().stockRessource2;
			}
			if (b.GetComponent<BuildingInfo>().stockRessource3 > 0)
			{
				infoUser.nbrTotalR3 += b.GetComponent<BuildingInfo>().nbrRessource3;
				infoUser.nbrMaxR3 += b.GetComponent<BuildingInfo>().stockRessource3;
			}
			infoUser.prodRateNumberRessource1 += b.GetComponent<BuildingInfo>().ratioRessource1;
			infoUser.prodRateNumberRessource2 += b.GetComponent<BuildingInfo>().ratioRessource2;
			infoUser.prodRateNumberRessource3 += b.GetComponent<BuildingInfo>().ratioRessource3;
		}
		GameObject.Find("GameController").GetComponent<UpdateSlider>().displayRessource();
	}
}
