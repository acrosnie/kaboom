using UnityEngine;
using System.Collections;

public class CancelMenuButton : MonoBehaviour
{
	private Transform infoPanel;
	private Transform upgradePanel;
	private Transform greyFilter;
	private Transform MouseSensitive;
	public bool isInfoPanel;
	public bool exitForce;

	void Start()
	{
		infoPanel = GameObject.Find("UICanvas").transform.Find("InfoPanel");
		upgradePanel = GameObject.Find("UICanvas").transform.Find("UpgradePanel");
		greyFilter = GameObject.Find("UICanvas").transform.Find("greyFilter");
		MouseSensitive = GameObject.Find("mouseCollider").transform;
	}

	public void exitButtonInfoUpgrade()
	{
		MouseSensitive.GetComponent<MouseSensitive>().unactiveSensitive = false;
		greyFilter.gameObject.SetActive(false);
		if (isInfoPanel || exitForce)
			infoPanel.gameObject.SetActive(false);
		if (!isInfoPanel || exitForce)
			upgradePanel.gameObject.SetActive(false);
	}
}
