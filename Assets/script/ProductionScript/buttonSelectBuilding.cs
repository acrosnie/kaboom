using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonSelectBuilding : MonoBehaviour
{
	public GameObject cameraTarget;
	public GameObject mouseCollider;
	public GameObject script;
	//public Transform infoText;
	private Vector3 posPanel;
	private Transform terrain;

	void Start()
	{
		//name = transform.GetChild(1).GetComponent<Text>().text;
		terrain = GameObject.Find("terrain").transform;
	}

	void errorMessage(string error)
	{
		// var infoTextClone = Instantiate(infoText, infoText.position, infoText.rotation) as Transform;
		// infoTextClone.gameObject.SetActive(true);
		// infoTextClone.GetComponent<Text>().text = error;
		// infoTextClone.parent = GameObject.Find("UICanvas").transform;
		// Destroy(infoTextClone.gameObject, 2.0f);
		print (error);
	}

	public void addBuilding(string nameBuilding, int level)
	{
		var m = script.GetComponent<GenerateProd>();
		var infoUser = script.GetComponent<InfoUser>();
		foreach (var val in m.buildingInfoNbrObject)
		{
			if (val.name == nameBuilding)
			{
				if (val.nbr >= m.getNbrMaxBuilding(val.name))
				{
					errorMessage("Can't add building because nbr max reach");
					return ;
				}
				else
				{
					var b = script.GetComponent<SelectBuildingProd>().createSelectableBuilding(0, nameBuilding, level, true);
					if (infoUser.nbrTotalR1 >= b.GetComponent<BuildingInfo>().costBuyRessource1 && infoUser.nbrTotalR2 >= b.GetComponent<BuildingInfo>().costBuyRessource2 && infoUser.nbrTotalR3 >= b.GetComponent<BuildingInfo>().costBuyRessource3)
					{
						GameObject.Find("GameController").GetComponent<BuildMenuOnOff>().desactive();
						val.nbr++;
						m.spendRessource((int)b.GetComponent<BuildingInfo>().costBuyRessource1, (int)b.GetComponent<BuildingInfo>().costBuyRessource2, (int)b.GetComponent<BuildingInfo>().costBuyRessource3);
						cameraTarget.GetComponent<Move>().block = false;
						mouseCollider.GetComponent<MouseSensitive>().unactiveSensitive = false;
						return ;
					}
					else
					{
						errorMessage("Not enough ressource for buy building");
						Destroy(b.gameObject);
						return ;
					}
				}
			}
		}
		errorMessage("Building not exist or not fill in csv :)");
	}

	public void addBuildingGlobes(string nameBuilding, int level, int r1, int r2, int r3, int r4)
	{
		var m = script.GetComponent<GenerateProd>();
		var infoUser = script.GetComponent<InfoUser>();
		foreach (var val in m.buildingInfoNbrObject)
		{
			if (val.name == nameBuilding)
			{
				if (val.nbr >= m.getNbrMaxBuilding(val.name))
				{
					errorMessage("Can't add building because nbr max reach");
					return ;
				}
				else
				{
					var b = script.GetComponent<SelectBuildingProd>().createSelectableBuilding(0, nameBuilding, level, true);
					GameObject.Find("GameController").GetComponent<BuildMenuOnOff>().desactive();
					val.nbr++;
					m.spendRessource(r1, r2, r3);
					m.spendGlobes(r4);
					cameraTarget.GetComponent<Move>().block = false;
					mouseCollider.GetComponent<MouseSensitive>().unactiveSensitive = false;
					return ;
				}
			}
		}
		errorMessage("Building not exist or not fill in csv :)");
	}
}
