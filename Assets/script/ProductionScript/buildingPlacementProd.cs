using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BuildingPlacementProd : MonoBehaviour
{
	public bool selected;
	public Vector3 initialPosition;
	public Vector3 oldPosition;
	public Vector3 lastGoodPosition;
	public bool newBuilding;
	public bool mouseDown;
	public Vector3 finalPosition;
	private bool noBuildingHere;
	private float timeBeforeSelect;
	private Transform terrain;
	private Transform myCamera;
	private Transform mouseCollider;
	private Vector2 initMousePos;
	private bool hasMoved;
	private float timeLimite = 1.2f;
	private bool distBool;
	private Vector3 posCam;
	private Transform script;
	private bool colorUp;
	private float tcolor;
	private Transform distFromLeftObject;
	private Transform GameController;
	private Transform nextBuilding;
	public Transform canvasBuilding;

	void Start()
	{
		loadVar();
	}

	void updateIfNextBuilding()
	{
		nextBuilding = null;
		var buildingList = script.GetComponent<SelectBuildingProd>().building;
		var info = GetComponent<BuildingInfo>();

		for (int i = 0; i < buildingList.Length; i++)
		{
			if (info && buildingList[i] != null && buildingList[i].GetComponent<BuildingInfo>().name == info.name && buildingList[i].GetComponent<BuildingInfo>().level == info.level + 1)
			{
				nextBuilding = buildingList[i];
				break ;
			}
		}
	}

	public void loadVar()
	{
		canvasBuilding = transform.Find("CanvasBuilding");
		canvasBuilding.Find("topBarUpgradeTime").gameObject.SetActive(false);
		canvasBuilding.Find("inSelect").Find("bottomBarUpgradeTime").gameObject.SetActive(false);
		if (GameObject.Find("script"))
			script = GameObject.Find("script").transform;
		if (GameObject.Find("GameController"))
			GameController = GameObject.Find("GameController").transform;
		noBuildingHere = true;
		finalPosition = transform.position;
		if (GameObject.Find("terrain"))
		{
			terrain = GameObject.Find("terrain").transform;
			mouseCollider = terrain.Find("mouseCollider");
		}
		if (GameObject.Find("CameraTarget"))
			myCamera = GameObject.Find("CameraTarget").transform;
		if (GameObject.Find("distFromLeftPoint"))
			distFromLeftObject = GameObject.Find("distFromLeftPoint").transform;
		updateIfNextBuilding();
	}

	void OnLevelWasLoaded(int level)
	{
		loadVar();
		// if (newBuilding)
		// 	transform.Find("arrowGroupe").gameObject.SetActive(true);
	}
	// get touch pos (x && z)
	Vector2 getMousePos()
	{
		#if UNITY_ANDROID && !UNITY_EDITOR
		if (Input.touchCount == 1)
		{
			RaycastHit2D[] hits = Physics2D.RaycastAll(Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position), Input.GetTouch(0).position);
			for (int i = 0; i < hits.Length; i++)
			{
				RaycastHit2D hit = hits[i];
				if (hit.collider.tag == "mouseSensitive")
				{
					var pos = new Vector2(hit.point.x, hit.point.y);
					return (pos);
				}
			}
		}
		#endif
		#if UNITY_EDITOR || UNITY_STANDALONE
			RaycastHit2D[] hits = Physics2D.RaycastAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), Input.mousePosition);
			for (int i = 0; i < hits.Length; i++)
			{
				RaycastHit2D hit = hits[i];
				if (hit.collider.tag == "mouseSensitive")
				{
					var pos = new Vector2(hit.point.x, hit.point.y);
					return (pos);
				}
			}
		#endif
		return (transform.position);
	}

	bool isInmatrixArray(Vector2 p)
	{
		var caseObject = script.GetComponent<GenerateProd>().caseObject;
		for (int i = 0; i < script.GetComponent<GenerateProd>().sizeY; i++)
		{
			for (int j = 0; j < script.GetComponent<GenerateProd>().sizeX; j++)
			{
				if (caseObject[i, j].position.x == p.x && caseObject[i, j].position.y == p.y)
					return (true);
			}
		}
		return (false);
	}

	//check if building is in terrain
	bool isInTerrain(Vector2 p)
	{
		var decalX = (transform.Find("spriteFloor").localScale.x / 2.0f) - 0.5f;
		var decalY = (transform.Find("spriteFloor").localScale.y / 2.0f) - 0.5f;
		Vector2 left = new Vector2(p.x - decalX, p.y);
		Vector2 right = new Vector2(p.x + decalX, p.y);
		Vector2 bottom = new Vector2(p.x, p.y - (decalY * 0.70703125f));
		Vector2 top = new Vector2(p.x, p.y + (decalY * 0.70703125f));//0.70703125
		if (isInmatrixArray(left) && isInmatrixArray(right) && isInmatrixArray(top) && isInmatrixArray(bottom))
			return (true);
		return (false);
	}

	//deselect all building and if this building is selected update him in database ans in array matrix map position
	void deselectAll()
	{
		var t = GameObject.FindGameObjectsWithTag("building");

		foreach (var t2 in t)
		{
			if (t2 != null && t2.GetComponent<BuildingPlacementProd>() && t2.GetComponent<BuildingInfo>() && t2.GetComponent<BuildingPlacementProd>().selected == true) // if selected
			{
				t2.GetComponent<BuildingInfo>().distFromleft = Mathf.Round(Vector2.Distance((Vector2)distFromLeftObject.position, (Vector2)t2.GetComponent<BuildingPlacementProd>().lastGoodPosition)) / 0.66666f;
				t2.GetComponent<BuildingPlacementProd>().finalPosition = t2.GetComponent<BuildingPlacementProd>().lastGoodPosition; // posfinal = last good position
				if (t2.GetComponent<BuildingPlacementProd>().newBuilding) // if new
					t2.GetComponent<BuildingInfo>().insertThisBuilding(t2.GetComponent<BuildingPlacementProd>().finalPosition); //add in database
				else
					t2.GetComponent<BuildingInfo>().updateThisBuilding(t2.GetComponent<BuildingPlacementProd>().finalPosition); //update in database
				if (t2.GetComponent<BuildingPlacementProd>().oldPosition != t2.GetComponent<BuildingPlacementProd>().finalPosition) // if old position != new position ==> change matrix array map
				{
					// var startX = script.GetComponent<GenerateProd>().getXinArray(t2.GetComponent<BuildingPlacementProd>().finalPosition);
					// var startY = script.GetComponent<GenerateProd>().getXinArray(t2.GetComponent<BuildingPlacementProd>().finalPosition);
				}
				//if (!t2.GetComponent<BuildingPlacementProd>().newBuilding)
				//t2.GetComponent<BuildingPlacementProd>().changeSpritesOrder(false);
				if (script != null)
				{
					//script.GetComponent<GenerateProd>().fillMatrixPosition(t2.GetComponent<BuildingPlacementProd>().oldPosition, t2.transform.Find("spriteFloor").localScale.x, t2.transform.Find("spriteFloor").localScale.y, false);
					//script.GetComponent<GenerateProd>().fillMatrixPosition(t2.GetComponent<BuildingPlacementProd>().finalPosition, t2.transform.Find("spriteFloor").localScale.x, t2.transform.Find("spriteFloor").localScale.y, true);
				}
				t2.transform.Find("spriteFloor").Find("arrowGroupe").gameObject.SetActive(false);
				t2.GetComponent<BuildingPlacementProd>().oldPosition = t2.GetComponent<BuildingPlacementProd>().finalPosition; // old position = new position
				t2.GetComponent<BuildingPlacementProd>().newBuilding = false; // unselect
				GameObject.Find("script").GetComponent<SelectBuildingProd>().haveBuildingSelected = false;
				t2.GetComponent<BuildingPlacementProd>().selected = false;
				script.GetComponent<GenerateProd>().fillInfoUserRessource();
				script.GetComponent<GenerateProd>().reGenerateUIMissile();
				t2.transform.Find("spriteBuilding").GetComponent<Renderer>().material.color = new Color(1, 1, 1);
				t2.GetComponent<BuildingPlacementProd>().canvasBuilding.Find("inSelect").gameObject.SetActive(false);
				if (script != null)
					script.GetComponent<GenerateProd>().reorderSprite();
				if (terrain)
					terrain.GetComponent<TerrainInfo>().chooseTexture(false);	
			}
		}
		GameObject.Find("GameController").GetComponent<DisplayInfosSlider>().desactive();
	}

	//function for select a building without touch, for new building...
	public void checkForSelect()
	{
		deselectAll();
		selected = true;
		timeBeforeSelect = 0;
		initialPosition = transform.position;
		finalPosition = transform.position;
		lastGoodPosition = transform.position;
		oldPosition = transform.position;
		changeSpritesOrder(true);
	}

	void OnMouseDown()
	{
		if (mouseCollider.GetComponent<MouseSensitive>().unactiveSensitive)
			return ;
		loadVar();
		if (myCamera != null)
			myCamera.GetComponent<Move>().setVelocity(Vector3.zero);
		posCam = myCamera.position;
		mouseDown = true;
		timeBeforeSelect = 0;
		initMousePos = getMousePos();
		initialPosition = transform.position;
	}

	void OnMouseOver()
	{
		if (mouseCollider.GetComponent<MouseSensitive>().unactiveSensitive)
			return ;
		if (selected || !mouseDown)
			return ;
		if ((GetComponent<BuildingInfo>().nbrRessource1 > 0 || GetComponent<BuildingInfo>().nbrRessource2 > 0 || GetComponent<BuildingInfo>().nbrRessource3 > 0)
			&& (GetComponent<BuildingInfo>().ratioRessource1 > 0 || GetComponent<BuildingInfo>().ratioRessource2 > 0 || GetComponent<BuildingInfo>().ratioRessource3 > 0))
			return ;	
		timeBeforeSelect += Time.deltaTime;
		if (timeBeforeSelect > 1.8f)
		{
			myCamera.GetComponent<Move>().block = true;
			OnMouseUpAsButton();
		}
	}

	void OnMouseExit()
	{
		if (mouseCollider.GetComponent<MouseSensitive>().unactiveSensitive)
			return ;
		if (!selected)
			timeBeforeSelect = 0;
	}

	//click on building for select
	void OnMouseUpAsButton()
	{
		if (mouseCollider.GetComponent<MouseSensitive>().unactiveSensitive || checkIfHud())
			return ;
		mouseDown = false;
		if (selected)
		{
			if (finalPosition == initialPosition)
			{
				if (terrain)
					terrain.GetComponent<TerrainInfo>().chooseTexture(false);
				if (!canPlaceHere())
					finalPosition = lastGoodPosition;	
				deselectAll();
			}
			return ;
		}
		else if (myCamera != null && myCamera.position != posCam)
			return ;
		deselectAll();
		updateIfNextBuilding();
		if (GetComponent<BuildingInfo>().nbrRessource1 > 0 || GetComponent<BuildingInfo>().nbrRessource2 > 0 || GetComponent<BuildingInfo>().nbrRessource3 > 0)
		{
			GetComponent<BuildingAction>().getRessource();
			if (GetComponent<BuildingInfo>().nbrRessource1 > 0 || GetComponent<BuildingInfo>().nbrRessource2 > 0 || GetComponent<BuildingInfo>().nbrRessource3 > 0)
			{
				// print (GetComponent<BuildingInfo>().nbrRessource1);
				// print (GetComponent<BuildingInfo>().nbrRessource2);
				// print (GetComponent<BuildingInfo>().nbrRessource3);
			}
			else
				return ;
		}
		changeSpritesOrder(true);
		timeBeforeSelect = 0;
		mouseDown = false;
		initMousePos = getMousePos();
		finalPosition = transform.position;
		initialPosition = transform.position;
		oldPosition = transform.position;
		GetComponent<BuildingAction>().fillInfoBuildingPanel();
		GetComponent<BuildingAction>().fillUpgradeBuildingPanel();
		selected = true;
		canvasBuilding.gameObject.SetActive(true);
		if (GameController != null)
		{
			GameController.GetComponent<FinishUpgradeWithGlobes>().building = transform;
			if (GetComponent<BuildingInfo>().upgrade || GetComponent<BuildingInfo>().missileUpgrade)
			{
				//GameController.GetComponent<FinishUpgradeWithGlobes>().building = transform;
				GameController.GetComponent<ClickOnBuildingOnOff>().active(3);
				if (GetComponent<BuildingInfo>().upgrade)
				{
					GameController.GetComponent<FinishUpgradeWithGlobes>().missile = false;
					GameController.GetComponent<ClickOnBuildingOnOff>().C.transform.Find("InstantButton").Find("RawImage").Find("Number").GetComponent<Text>().text = "" + GetComponent<BuildingInfo>().costRessource4UpgradeLeft;
				}
				else
				{
					GameController.GetComponent<FinishUpgradeWithGlobes>().missile = true;
					GameController.GetComponent<ClickOnBuildingOnOff>().C.transform.Find("InstantButton").Find("RawImage").Find("Number").GetComponent<Text>().text = "" + GetComponent<BuildingInfo>().costRessource4UpgradeLeftMissile;
				}
			}
			else if (GetComponent<BuildingInfo>().name == "Attack Whale" || GetComponent<BuildingInfo>().name == "Missile Launcher" || GetComponent<BuildingInfo>().name == "Geo's Lab" || GetComponent<BuildingInfo>().name == "Missile Factory")
				GameController.GetComponent<ClickOnBuildingOnOff>().active(1);
			else
				GameController.GetComponent<ClickOnBuildingOnOff>().active(2);
		}
		if (terrain)
			terrain.GetComponent<TerrainInfo>().chooseTexture(true);
	}

	void OnMouseUp()
	{
		if (mouseCollider.GetComponent<MouseSensitive>().unactiveSensitive)
			return ;
		if (myCamera != null)
			myCamera.GetComponent<Move>().block = false;
		if (!selected)
			return ;
		if (canPlaceHere())
			lastGoodPosition = finalPosition;
		distBool = false;
		mouseDown = false;
	}

	public bool canPlaceHere()
	{
		// if (selected && (!isInTerrain(finalPosition) || !noBuildingHere))
		// 	return (false);
		return (true);
	}

	void calcFinalPosition(float x, float y)
	{
		float distance = -1;
		var v = new Vector2(x, y);
		Vector2 posFound = v;
		int lx = (int)(transform.Find("spriteFloor").localScale.x / 2.0f);
		int ly = (int)(transform.Find("spriteFloor").localScale.y / 2.0f);
		var caseObject = script.GetComponent<GenerateProd>().caseObject;
		var sizeY = script.GetComponent<GenerateProd>().sizeY;
		var sizeX = script.GetComponent<GenerateProd>().sizeX;

		if ((int)transform.Find("spriteFloor").localScale.x % 2 == 0)
		{
			sizeY -= 1;
			sizeX += 1;
			ly -= 1;
		}
		for (int i = ly; i < sizeY - ly; i++)
		{
			for (int j = lx; j < sizeX - lx; j++)
			{
				var tempDist = Vector2.Distance(v, caseObject[i, j].position);
				if (distance == -1 || tempDist < distance)
				{
					if (script.GetComponent<SelectBuildingProd>().canPlaceHere(transform, i, j, caseObject, true))
					{
						transform.GetComponent<BuildingInfo>().oldPosMatrixX = transform.GetComponent<BuildingInfo>().posMatrixX;
						transform.GetComponent<BuildingInfo>().oldPosMatrixY = transform.GetComponent<BuildingInfo>().posMatrixY;
						transform.GetComponent<BuildingInfo>().posMatrixX = j;
						transform.GetComponent<BuildingInfo>().posMatrixY = i;
						posFound = caseObject[i, j].position;
						distance = tempDist;
					}
				}
			}
		}
		finalPosition = posFound;
		if ((int)transform.Find("spriteFloor").localScale.x % 2 == 0)
			finalPosition += new Vector3(-0.5f, 0, 0);
	}

	void OnMouseDrag()
	{
		if (mouseCollider.GetComponent<MouseSensitive>().unactiveSensitive)
			return ;
		if (!selected)
			return ;
		myCamera.GetComponent<Move>().block = true;
		if (getMousePos() == (Vector2)transform.position)
			return ;
		var p = getMousePos() - initMousePos;
		var x = p.x;
		var y = p.y;
		var dist = Vector2.Distance(initMousePos, getMousePos());
		if (dist < 1.0f && !distBool) // if distance drag is close
			return ;
		distBool = true;
		calcFinalPosition(initialPosition.x + x, initialPosition.y + y);
	}

	void moveBuilding()
	{
		if (transform.position == finalPosition)
			return ;
		transform.position = Vector3.Lerp(transform.position, finalPosition, 0.4f);
		if (finalPosition != initialPosition)
			hasMoved = true;
	}

	void arrowGestion()
	{
		transform.Find("spriteFloor").Find("arrowGroupe").gameObject.SetActive(true);
	}

	void terrainGestion()
	{
		if (selected && terrain)
			terrain.GetComponent<TerrainInfo>().chooseTexture(true);
	}

	void menuGestion()
	{
		if (GameController == null)
			return ;
		if (myCamera.GetComponent<Move>().block)
		{
			canvasBuilding.Find("inSelect").gameObject.SetActive(false);
			GameController.GetComponent<ClickOnBuildingOnOff>().desactiveTemp();
		}
		else
		{
			canvasBuilding.Find("inSelect").gameObject.SetActive(true);
			GameController.GetComponent<ClickOnBuildingOnOff>().reactiveTemp();
		}
	}

	public void changeSpritesOrder(bool up)
	{

		// if (up)
		// {
		// 	var sList = GetComponentsInChildren<SpriteRenderer>(true);
		// 	foreach (SpriteRenderer s in sList)
		// 		s.sortingOrder += 5000;
		// }
		// else
		// {
		// 	var sList = GetComponentsInChildren<SpriteRenderer>(true);
		// 	foreach (SpriteRenderer s in sList)
		// 		s.sortingOrder -= 5000;
		// }
		var p = transform.Find("spriteBuilding").transform.position;
		if (up)
			transform.Find("spriteBuilding").transform.position = new Vector3(p.x, p.y, -2);
	}

	void colorSpriteGestion()
	{
		tcolor += Time.deltaTime;
		if (tcolor < 0.02f)
			return ;
		tcolor = 0;
		var col = transform.Find("spriteBuilding").GetComponent<Renderer>().material.color;
		if (col.b >= 1)
			colorUp = false;
		else if (col.b < 0.4f)
			colorUp = true;
		if (colorUp)
			col = new Color(col.r + 0.03f, col.g + 0.03f, col.b + 0.03f, 1.0f);
		else
			col = new Color(col.r - 0.03f, col.g - 0.03f, col.b - 0.03f, 1.0f);
		if (!noBuildingHere)
			col = new Color(0.8f, 0.1f, 0.1f, 0.9f);
		transform.Find("spriteBuilding").GetComponent<Renderer>().material.color = col;
	}

	bool checkIfHud()
	{
		#if UNITY_ANDROID && !UNITY_EDITOR
			if (Input.touchCount > 0)
			{
				if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
					return (true);
			}	
		#endif
		#if UNITY_EDITOR || UNITY_STANDALONE
			if (EventSystem.current.IsPointerOverGameObject())
				return (true);
		#endif
		return (false);
	}

	void canvasLevelUp()
	{
		if (GetComponent<BuildingInfo>().upgrade)
		{
			transform.GetComponent<BuildingPlacementProd>().canvasBuilding.Find("levelUpIcon").gameObject.SetActive(false);
			var timeBetweenStartUpgrade = GetComponent<BuildingInfo>().timeBetweenStartUpgrade;
			var constructionTime = GetComponent<BuildingInfo>().constructionTime;
			var totalInstant = GetComponent<BuildingInfo>().totalInstant;
			GetComponent<BuildingInfo>().costRessource4UpgradeLeft = (int)(totalInstant - (timeBetweenStartUpgrade / (constructionTime / totalInstant)));
			canvasBuilding.Find("topBarUpgradeTime").gameObject.SetActive(true);
			canvasBuilding.Find("inSelect").Find("bottomBarUpgradeTime").gameObject.SetActive(true);
			canvasBuilding.Find("topBarUpgradeTime").Find("Image").GetComponent<Image>().fillAmount = (timeBetweenStartUpgrade / constructionTime);
			canvasBuilding.Find("topBarUpgradeTime").Find("Text").GetComponent<Text>().text = (int)timeBetweenStartUpgrade + " / " + (int)constructionTime;
			canvasBuilding.Find("inSelect").Find("bottomBarUpgradeTime").Find("Image").GetComponent<Image>().fillAmount = (timeBetweenStartUpgrade / constructionTime);
			canvasBuilding.Find("inSelect").Find("bottomBarUpgradeTime").Find("Text").GetComponent<Text>().text = (int)timeBetweenStartUpgrade + " / " + (int)constructionTime;
			GetComponent<BuildingInfo>().timeBetweenStartUpgrade += Time.deltaTime / 2.0f;
			if (timeBetweenStartUpgrade > constructionTime)
			{
				GetComponent<BuildingAction>().finishLevelUp();
				GetComponent<BuildingInfo>().upgrade = false;
			}
			if (selected)
				canvasBuilding.Find("topBarUpgradeTime").gameObject.SetActive(false);
		}
		else if (GetComponent<BuildingInfo>().missileUpgrade)
		{
			transform.GetComponent<BuildingPlacementProd>().canvasBuilding.Find("levelUpIcon").gameObject.SetActive(false);
			var timeBetweenStartUpgrade = GetComponent<BuildingInfo>().timeBetweenStartUpgradeMissile;
			var constructionTime = GetComponent<BuildingInfo>().upgradeTimeMissile;
			var totalInstant = GetComponent<BuildingInfo>().totalInstantMissile;
			GetComponent<BuildingInfo>().costRessource4UpgradeLeftMissile = (int)(totalInstant - (timeBetweenStartUpgrade / (constructionTime / totalInstant)));
			canvasBuilding.Find("topBarUpgradeTime").gameObject.SetActive(true);
			canvasBuilding.Find("inSelect").Find("bottomBarUpgradeTime").gameObject.SetActive(true);
			canvasBuilding.Find("topBarUpgradeTime").Find("Image").GetComponent<Image>().fillAmount = (timeBetweenStartUpgrade / constructionTime);
			canvasBuilding.Find("topBarUpgradeTime").Find("Text").GetComponent<Text>().text = (int)timeBetweenStartUpgrade + " / " + (int)constructionTime;
			canvasBuilding.Find("inSelect").Find("bottomBarUpgradeTime").Find("Image").GetComponent<Image>().fillAmount = (timeBetweenStartUpgrade / constructionTime);
			canvasBuilding.Find("inSelect").Find("bottomBarUpgradeTime").Find("Text").GetComponent<Text>().text = (int)timeBetweenStartUpgrade + " / " + (int)constructionTime;
			GetComponent<BuildingInfo>().timeBetweenStartUpgradeMissile += Time.deltaTime / 2.0f;
			if (timeBetweenStartUpgrade > constructionTime)
			{
				GetComponent<BuildingAction>().finishLevelUpMissile();
				GetComponent<BuildingInfo>().missileUpgrade = false;
			}
			if (selected)
				canvasBuilding.Find("topBarUpgradeTime").gameObject.SetActive(false);
		}
		else
		{
			if (nextBuilding != null)
				transform.GetComponent<BuildingPlacementProd>().canvasBuilding.Find("levelUpIcon").gameObject.SetActive(true);
			else
				transform.GetComponent<BuildingPlacementProd>().canvasBuilding.Find("levelUpIcon").gameObject.SetActive(false);
			canvasBuilding.Find("topBarUpgradeTime").gameObject.SetActive(false);
			canvasBuilding.Find("inSelect").Find("bottomBarUpgradeTime").gameObject.SetActive(false);
		}
	}

	void Update()
	{
		canvasLevelUp();
		if ((GetComponent<BuildingInfo>().ratioRessource1 > 0 || GetComponent<BuildingInfo>().ratioRessource2 > 0 || GetComponent<BuildingInfo>().ratioRessource3 > 0) &&
			(GetComponent<BuildingInfo>().nbrRessource1 > 0 || GetComponent<BuildingInfo>().nbrRessource2 > 0 || GetComponent<BuildingInfo>().nbrRessource3 > 0))
			canvasBuilding.Find("getRessource").gameObject.SetActive(true);
		else
			canvasBuilding.Find("getRessource").gameObject.SetActive(false);
		if (mouseCollider.GetComponent<MouseSensitive>().unactiveSensitive)
			return ;
		moveBuilding();
		if (selected)
		{
			arrowGestion();
			colorSpriteGestion();
			terrainGestion();
			menuGestion();
			// if (Input.GetKeyDown("l"))
			// {
			// 	GetComponent<BuildingAction>().levelUp();
			// }
		}
		else
			transform.Find("spriteBuilding").GetComponent<Renderer>().material.color = new Color (1, 1, 1, 1);
	}

	void OnTriggerStay2D(Collider2D other)
	{
		if (other.tag == "buildingColliderPlacement")
			noBuildingHere = false;
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag == "buildingColliderPlacement")
			noBuildingHere = true;
		transform.Find("spriteBuilding").GetComponent<Renderer>().material.color = new Color (1, 1, 1, 1);
	}
}
