using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SimpleJSON;

public class upgradeMissile : MonoBehaviour
{	
	public void upButton(InfoMissile infoMissileOld, InfoMissile infoMissileNew)
	{
		var infoUser = GameObject.Find("script").GetComponent<InfoUser>();
		if (infoUser.nbrTotalR2 >= infoMissileOld.ressource2CostUpgrade)
		{
			GameObject.Find("script").GetComponent<GenerateProd>().spendRessource(0, (int)infoMissileOld.ressource2CostUpgrade, 0);
			GameObject.Find("script").GetComponent<GenerateProd>().fillInfoUserRessource();
			StartCoroutine(startUpdateMissileController(infoMissileOld, infoMissileNew, infoMissileOld.missileLevelListId, infoMissileNew.missileLevelListId, infoMissileNew.missileListId, true));
			GameObject.Find("GameController").GetComponent<MissileUpgradeOnOff>().desactive();
			GameObject.Find("GameController").GetComponent<GeoLabOnOff>().desactive();
		}
		else
			GameObject.Find("GameController").GetComponent<ConvertGlobes>().displayMissileUpgrade(infoMissileOld, infoMissileNew);
	}

	public void upForced(InfoMissile infoMissileOld, InfoMissile infoMissileNew, int r1, int r2, int r3, int r4)
	{
		GameObject.Find("script").GetComponent<GenerateProd>().spendRessource(r1, r2, r3);
		GameObject.Find("script").GetComponent<GenerateProd>().spendGlobes(r4);
		GameObject.Find("script").GetComponent<GenerateProd>().fillInfoUserRessource();
		StartCoroutine(startUpdateMissileController(infoMissileOld, infoMissileNew, infoMissileOld.missileLevelListId, infoMissileNew.missileLevelListId, infoMissileNew.missileListId, true));
		GameObject.Find("GameController").GetComponent<MissileUpgradeOnOff>().desactive();
		GameObject.Find("GameController").GetComponent<GeoLabOnOff>().desactive();
	}

	public void upButtonInstant(InfoMissile infoMissileOld, InfoMissile infoMissileNew)
	{
		var infoUser = GameObject.Find("script").GetComponent<InfoUser>();
		if (infoUser.nbrTotalR4 >= (infoMissileOld.intantR2Upgrade + infoMissileOld.intantTimeUpgrade))
		{
			GameObject.Find("script").GetComponent<GenerateProd>().spendGlobes((int)(infoMissileOld.intantR2Upgrade + infoMissileOld.intantTimeUpgrade));
			GameObject.Find("script").GetComponent<GenerateProd>().fillInfoUserRessource();
			StartCoroutine(finishUpdateMissileController(infoMissileOld.missileLevelListId, infoMissileNew.missileLevelListId, infoMissileNew.missileListId));
			GameObject.Find("GameController").GetComponent<MissileUpgradeOnOff>().desactive();
			GameObject.Find("GameController").GetComponent<GeoLabOnOff>().desactive();
		}
		else
			print ("pas assez de globes pour update ce missile");
	}

	public void fillInfoInfo(GameObject A, InfoMissile infoMissile)
	{
		var missileTexture = GameObject.Find("script").GetComponent<GenerateProd>().missileTexture;
		var slot = A.transform.Find("Panel");
		slot.Find("missileNameAndLevel").GetComponent<Text>().text = infoMissile.name;
		foreach (var tt in missileTexture)
		{
			if (infoMissile.name == tt.name)
			{
				slot.Find("missileImage").GetComponent<RawImage>().texture = tt;
				break ;
			}
		}
		slot.Find("Text (7)").GetComponent<Text>().text  = "" + infoMissile.damageBuilding;
		slot.Find("Text (6)").GetComponent<Text>().text  = "" + infoMissile.health;
		slot.Find("Text (9)").GetComponent<Text>().text = "" + infoMissile.area;
		slot.Find("Text (8)").GetComponent<Text>().text = "" + infoMissile.speed;
		slot.Find("Text (14)").GetComponent<Text>().text = "" + infoMissile.ressource2Cost;
		slot.Find("Text (10)").GetComponent<Text>().text = "" + infoMissile.prodTime;
		slot.Find("missileDescription").GetComponent<Text>().text = "" + infoMissile.description;
	}

	public void fillInfo(GameObject A, InfoMissile infoMissileOld, InfoMissile infoMissileNew)
	{
		var missileTexture = GameObject.Find("script").GetComponent<GenerateProd>().missileTexture;
		var slot = A.transform.Find("Panel");
		slot.Find("missileNameAndLevel").GetComponent<Text>().text = infoMissileNew.name;
		slot.Find("upgradeTo").GetComponent<Text>().text = "-      Upgrade to lvl " + infoMissileNew.missileLevel;
		foreach (var tt in missileTexture)
		{
			if (infoMissileNew.name == tt.name)
			{
				slot.Find("missileImage").GetComponent<RawImage>().texture = tt;
				break ;
			}
		}
		var infoUser = GameObject.Find("script").GetComponent<InfoUser>();
		slot.Find("Text (7)").GetComponent<Text>().text  = "" + infoMissileOld.damageBuilding;
		slot.Find("Text (12)").GetComponent<Text>().text = "+" + (infoMissileNew.damageBuilding - infoMissileOld.damageBuilding);
		slot.Find("Text (6)").GetComponent<Text>().text  = "" + infoMissileOld.health;
		slot.Find("Text (11)").GetComponent<Text>().text = "+" + (infoMissileNew.health - infoMissileOld.health);
		slot.Find("Text (9)").GetComponent<Text>().text = "" + infoMissileOld.area;
		slot.Find("Text (16)").GetComponent<Text>().text = "+" + (infoMissileNew.area - infoMissileOld.area);
		slot.Find("Text (8)").GetComponent<Text>().text = "" + infoMissileOld.speed;
		slot.Find("Text (15)").GetComponent<Text>().text = "+" + (infoMissileNew.speed - infoMissileOld.speed);
		slot.Find("Text (14)").GetComponent<Text>().text = "" + infoMissileOld.ressource2Cost;
		slot.Find("Text (18)").GetComponent<Text>().text = "+" + (infoMissileNew.ressource2Cost - infoMissileOld.ressource2Cost);
		slot.Find("Text (10)").GetComponent<Text>().text = "" + infoMissileOld.prodTime;
		slot.Find("Text (17)").GetComponent<Text>().text = "+" + (infoMissileNew.prodTime - infoMissileOld.prodTime);
		slot.Find("Text (13)").GetComponent<Text>().text = "+" + infoMissileOld.exp;
		slot.Find("Price Ressource 2").Find("Text").GetComponent<Text>().text = "" + infoMissileOld.ressource2CostUpgrade;
		slot.Find("Price Ressource 4").Find("Text").GetComponent<Text>().text = "" + (infoMissileOld.intantR2Upgrade + infoMissileOld.intantTimeUpgrade);
		slot.Find("Price Ressource 2").Find("Text").GetComponent<Text>().color = Color.white;
		slot.Find("Price Ressource 2").Find("Text").GetComponent<Text>().color = Color.white;
		if (infoUser.nbrTotalR2 < infoMissileOld.ressource2CostUpgrade)
			slot.Find("Price Ressource 2").Find("Text").GetComponent<Text>().color = Color.red;
		if (infoUser.nbrTotalR4 < (infoMissileOld.intantR2Upgrade + infoMissileOld.intantTimeUpgrade))
			slot.Find("Price Ressource 4").Find("Text").GetComponent<Text>().color = Color.red;
		var i1 = infoMissileOld;
		var i2 = infoMissileNew;
		GameObject.Find("GameController").GetComponent<FinishUpgradeWithGlobes>().missile = true;
		slot.Find("Upgrade").Find("Text").GetComponent<Text>().text = "" + (infoMissileOld.upgradeTime / 3600).ToString("0.0") + "h";
		slot.Find("Upgrade").GetComponent<Button>().onClick.RemoveAllListeners();
		slot.Find("Upgrade").GetComponent<Button>().onClick.AddListener(delegate{ upButton(i1, i2); });
		slot.Find("Instant").GetComponent<Button>().onClick.RemoveAllListeners();
		slot.Find("Instant").GetComponent<Button>().onClick.AddListener(delegate{ GameObject.Find("GameController").GetComponent<FinishUpgradeWithGlobes>().finishDisplay(true, i1, i2); });
		//slot.Find("Instant").GetComponent<Button>().onClick.AddListener(delegate{ upButtonInstant(i1, i2); });
	}

	IEnumerator startUpdateMissileController(InfoMissile infoMissileOld, InfoMissile infoMissileNew, int missileLevelListIdOld, int missileLevelListId, int missileListId, bool update)
	{
		var u = (update) ? 1 : 0;
		var idPlayer = GameObject.Find("mainScript").GetComponent<GenerateResource>().idPlayer;
		var url = "http://5.196.68.62/api/missilePlayerConstruct/update/startUpgrade/" + idPlayer + "/" + missileLevelListId + "/" + missileListId + "/" + u;
		url = url.Replace(" ", "%20");
		print (url);
		WWW www = new WWW(url);
		yield return (www);
		var buildings = GameObject.FindGameObjectsWithTag("building");
		foreach (GameObject b in buildings)
		{
			if (b.GetComponent<BuildingInfo>().name == "Geo's Lab")
			{
				if (update)
				{
					b.GetComponent<BuildingInfo>().infoMissileOld = infoMissileOld;
					b.GetComponent<BuildingInfo>().infoMissileNew = infoMissileNew;
					b.GetComponent<BuildingInfo>().missileUpgrade = true;
					b.GetComponent<BuildingInfo>().timeBetweenStartUpgradeMissile = 0;
					b.GetComponent<BuildingInfo>().missileName = infoMissileOld.name;
					b.GetComponent<BuildingInfo>().totalInstantMissile = infoMissileOld.intantR2Upgrade + infoMissileOld.intantTimeUpgrade;
					b.GetComponent<BuildingInfo>().upgradeTimeMissile = infoMissileOld.upgradeTime;
					b.GetComponent<BuildingInfo>().costRessource4UpgradeLeftMissile = (int)(infoMissileOld.intantR2Upgrade + infoMissileOld.intantTimeUpgrade);
				}
				else
					b.GetComponent<BuildingInfo>().missileUpgrade = false;
				GameObject.Find("mouseCollider").GetComponent<MouseSensitive>().unselectAll();
				break ;
			}
		}
	}

	public void finishUpdateMissileControllerPublic(int missileLevelListIdOld, int missileLevelListId, int missileListId)
	{
		StartCoroutine(finishUpdateMissileController(missileLevelListIdOld, missileLevelListId, missileListId));
	}

	public void cancelUpdateMissileControllerPublic(InfoMissile infoMissileOld, InfoMissile infoMissileNew)
	{
		StartCoroutine(startUpdateMissileController(infoMissileOld, infoMissileNew, infoMissileOld.missileLevelListId, infoMissileNew.missileLevelListId, infoMissileNew.missileListId, false));
	}

	IEnumerator finishUpdateMissileController(int missileLevelListIdOld, int missileLevelListId, int missileListId)
	{
		var idPlayer = GameObject.Find("mainScript").GetComponent<GenerateResource>().idPlayer;
		var url = "http://5.196.68.62/api/missilePlayerConstruct/update/" + idPlayer + "/" + missileLevelListId + "/" + missileListId;
		url = url.Replace(" ", "%20");
		print (url);
		WWW www = new WWW(url);
		yield return (www);
		if (www.text != "")
		{
			GameObject.Find("script").GetComponent<GenerateProd>().missilePlayerConstructJson = JSON.Parse(www.text);
			print("upgrade Missile !");
			var mCopy = new MissilesReview();
			var missilesReview = GameObject.Find("script").GetComponent<GenerateProd>().missilesReview;
			foreach	(var m in missilesReview)
			{
				if (m.missile.GetComponent<InfoMissile>().missileLevelListId == missileLevelListIdOld)
				{
					mCopy.construct(mCopy.missile, m.missilePlayerid, m.nbr, m.equipedAtt, m.equipedDef); 
					m.nbr = 0;
					m.missilePlayerid = 0;
					m.equipedAtt = false;
					m.equipedDef = false;
					break ;
				}
			}
			foreach	(var m in missilesReview)
			{
				if (m.missile.GetComponent<InfoMissile>().missileLevelListId == missileLevelListId)
				{
					m.missilePlayerid = mCopy.missilePlayerid;
					m.nbr = mCopy.nbr + 1;
					m.equipedAtt = mCopy.equipedAtt;
					m.equipedDef = mCopy.equipedDef;
				}
			}
			GameObject.Find("script").GetComponent<GenerateProd>().reGenerateUIMissile(missileLevelListIdOld, missileLevelListId);
		}
		else
			print ("le missile n'a pas pu etre ajoute");
	}
}