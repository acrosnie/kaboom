using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine.UI;
using System.IO;
using System.Linq;

public class MissileProdScript : MonoBehaviour
{
	public Sprite spriteDefaultConstructionLine;
	public Transform constructionLine;
	private Transform script;

	void Start()
	{
		script = GameObject.Find("script").transform;
	}

	public IEnumerator addMissile(int missileLevelListId, InfoMissile missileInfo)
	{
		var playerId = PlayerPrefs.GetInt("idPlayer");
		var url = "http://5.196.68.62/api/addMissilePlayer/" + playerId +"/" + missileLevelListId;
		print (url);
		WWW www = new WWW(url);
		yield return (www);
		foreach	(var m in script.GetComponent<GenerateProd>().missilesReview)
		{
			if (m.missile.GetComponent<InfoMissile>().missileLevelListId == missileLevelListId)
			{
				print (m.missile.GetComponent<InfoMissile>().name + "  acheter ! ==> missileLevelListId : " + missileLevelListId);
				m.nbr++;
				script.GetComponent<GenerateProd>().reGenerateUIMissile();
				break ;
			}
		}
	}

	void addInConstructionLine(int missileLevelListId, InfoMissile missileInfo)
	{
		int i = 0;
		foreach (Transform child in constructionLine)
		{
			if (child.GetComponent<Image>().sprite == null || child.GetComponent<Image>().sprite == spriteDefaultConstructionLine)
			{
				PlayerPrefs.SetInt("missileConstructTimestamp" + i, (int)GameObject.Find("mainScript").GetComponent<GenerateResource>().timestamp);
				PlayerPrefs.SetInt("missileConstructMissileLevelListId" + i, missileLevelListId);
				child.GetComponent<Image>().sprite = missileInfo.sprite;
				child.GetComponent<Image>().fillAmount = 0;
				child.GetComponent<InConstructionLine>().i = i;
				child.GetComponent<InConstructionLine>().missileConstructTimestamp = PlayerPrefs.GetInt("missileConstructTimestamp" + i);
				child.GetComponent<InConstructionLine>().ready = true;
				child.GetComponent<InConstructionLine>().timeMax = (int)missileInfo.prodTime;
				child.GetComponent<InConstructionLine>().spriteDefault = spriteDefaultConstructionLine;
				child.GetComponent<InConstructionLine>().missileInfo = missileInfo;
				child.GetComponent<InConstructionLine>().missileLevelListId = missileLevelListId;
				child.Find("StopButton").gameObject.SetActive(true);
				child.Find("TimeLeft").gameObject.SetActive(true);
				child.Find("TimeLeft").GetComponent<Text>().text = "" + (int)missileInfo.prodTime;
				script.GetComponent<GenerateProd>().spendRessource((int)missileInfo.ressource1Cost, (int)missileInfo.ressource2Cost, (int)missileInfo.ressource3Cost);
				GameObject.Find("script").GetComponent<MissileProdScript>().majCostTotalInstantBuildMissile();
				break ;
			}
			i++;
		}
	}

	void addInConstructionLineForced(int missileLevelListId, InfoMissile missileInfo, int r1, int r2, int r3, int r4)
	{
		int i = 0;
		foreach (Transform child in constructionLine)
		{
			if (child.GetComponent<Image>().sprite == null || child.GetComponent<Image>().sprite == spriteDefaultConstructionLine)
			{
				PlayerPrefs.SetInt("missileConstructTimestamp" + i, (int)GameObject.Find("mainScript").GetComponent<GenerateResource>().timestamp);
				PlayerPrefs.SetInt("missileConstructMissileLevelListId" + i, missileLevelListId);
				child.GetComponent<Image>().sprite = missileInfo.sprite;
				child.GetComponent<Image>().fillAmount = 0;
				child.GetComponent<InConstructionLine>().i = i;
				child.GetComponent<InConstructionLine>().missileConstructTimestamp = PlayerPrefs.GetInt("missileConstructTimestamp" + i);
				child.GetComponent<InConstructionLine>().ready = true;
				child.GetComponent<InConstructionLine>().timeMax = (int)missileInfo.prodTime;
				child.GetComponent<InConstructionLine>().spriteDefault = spriteDefaultConstructionLine;
				child.GetComponent<InConstructionLine>().missileInfo = missileInfo;
				child.GetComponent<InConstructionLine>().missileLevelListId = missileLevelListId;
				child.Find("StopButton").gameObject.SetActive(true);
				child.Find("TimeLeft").gameObject.SetActive(true);
				child.Find("TimeLeft").GetComponent<Text>().text = "" + (int)missileInfo.prodTime;
				script.GetComponent<GenerateProd>().spendRessource(r1, r2, r3);
				script.GetComponent<GenerateProd>().spendGlobes(r4);
				GameObject.Find("script").GetComponent<MissileProdScript>().majCostTotalInstantBuildMissile();
				break ;
			}
			i++;
		}
	}

	public void generateMissileConstructionLine()
	{
		int i = 0;
		foreach (Transform child in constructionLine)
		{
			var m = PlayerPrefs.GetInt("missileConstructMissileLevelListId" + i);
			if (m > 0)
			{
				var missileInfo = getMissileInfo(m);
				child.GetComponent<Image>().sprite = missileInfo.sprite;
				child.GetComponent<Image>().fillAmount = 0;
				child.GetComponent<InConstructionLine>().i = i;
				child.GetComponent<InConstructionLine>().ready = true;
				child.GetComponent<InConstructionLine>().timeMax = (int)missileInfo.prodTime;
				child.GetComponent<InConstructionLine>().spriteDefault = spriteDefaultConstructionLine;
				child.GetComponent<InConstructionLine>().missileInfo = missileInfo;
				child.GetComponent<InConstructionLine>().missileLevelListId = m;
				child.Find("StopButton").gameObject.SetActive(true);
				child.Find("TimeLeft").gameObject.SetActive(true);
				child.Find("TimeLeft").GetComponent<Text>().text = "" + (int)missileInfo.prodTime;
				GameObject.Find("script").GetComponent<MissileProdScript>().majCostTotalInstantBuildMissile();
			}
			i++;
		}
	}

	public void constructMissileGlobes(int missileLevelListId, int r1, int r2, int r3, int r4)
	{
		var infoUser = script.GetComponent<InfoUser>();
		var missileInfo = getMissileInfo(missileLevelListId);
		addInConstructionLineForced(missileLevelListId, missileInfo, r1, r2, r3, r4);
	}

	public void constructMissile(int missileLevelListId, string name)
	{
		var infoUser = script.GetComponent<InfoUser>();
		var missileInfo = getMissileInfo(missileLevelListId);
		if (missileInfo == null)
		{
			print ("missile " + name + " not exist");
			return ;
		}
		// si on a assez de ressource
		if (infoUser.nbrTotalR1 >= missileInfo.ressource1Cost && infoUser.nbrTotalR2 >= missileInfo.ressource2Cost && infoUser.nbrTotalR3 >= missileInfo.ressource3Cost)
			addInConstructionLine(missileLevelListId, missileInfo);
		else
			GameObject.Find("GameController").GetComponent<ConvertGlobes>().displayMissile(missileInfo);
	}

	public bool hasMissileId(int id)
	{
		foreach	(var m in script.GetComponent<GenerateProd>().missilesReview)
		{
			if (m.missile.GetComponent<InfoMissile>().missileLevelListId == id && m.nbr > 0)
				return (true);
		}
		return (false);
	}

	public int nbrMissileId(int id)
	{
		foreach	(var m in script.GetComponent<GenerateProd>().missilesReview)
		{
			if (m.missile.GetComponent<InfoMissile>().missileLevelListId == id)
				return (m.nbr);
		}
		return (-1);
	}

	public InfoMissile getMissileInfo(int id)
	{
		foreach	(var m in script.GetComponent<GenerateProd>().missilesReview)
		{
			if (m.missile.GetComponent<InfoMissile>().missileLevelListId == id)
				return (m.missile.GetComponent<InfoMissile>());
		}
		return (null);
	}

	public bool isEquiped(int missileLevelListId, bool att)
	{
		var missilesReview = script.GetComponent<GenerateProd>().missilesReview;
		foreach (var m in missilesReview)
		{
			if (missileLevelListId == m.missile.GetComponent<InfoMissile>().missileLevelListId)
			{
				if (att)
					return (m.equipedAtt);
				else
					return (m.equipedDef);
			}
		}
		return (false);
	}

	public int majCostTotalInstantBuildMissile()
	{
		var ressourceLeft = 0;
		int i = 0;
		foreach (Transform child in constructionLine)
		{
			var m = PlayerPrefs.GetInt("missileConstructMissileLevelListId" + i);
			if (m > 0)
			{
				var missileInfo = GameObject.Find("script").GetComponent<MissileProdScript>().getMissileInfo(m);
				ressourceLeft += (int)(missileInfo.intantR2Prod + missileInfo.intantTimeProd);
			}
			i++;
		}
		constructionLine.parent.Find("buttonInstant").Find("value").GetComponent<Text>().text = "" + ressourceLeft;
		if (ressourceLeft <= 0)
			constructionLine.parent.Find("buttonInstant").gameObject.SetActive(false);
		else
			constructionLine.parent.Find("buttonInstant").gameObject.SetActive(true);
		return (ressourceLeft);
	}
}