using UnityEngine;
using System.Collections;

public class TerrainInfo : MonoBehaviour
{
	public int nbrCase;
	public float sizeCase;
	public Color startColor;
	public Color finalColor;
	public bool up;
	public Transform s;

	public void Start()
	{
		StartCoroutine(changeGrid());
		s = GameObject.Find("map").transform.Find("terrain").Find("grid").transform;
	}

	public void chooseTexture(bool grid)
	{
		up = grid;
	}

	IEnumerator changeGrid()
	{
		while (true)
		{
			yield return new WaitForSeconds(0.1f);
			var speedAnim = 0.1f;
			if (up)
			{
				var lerpedColor = Color.Lerp(s.GetComponent<Renderer>().material.color, finalColor, speedAnim);
				s.GetComponent<Renderer>().material.color = lerpedColor;
			}
			else
			{
				var lerpedColor = Color.Lerp(s.GetComponent<Renderer>().material.color, startColor, speedAnim);
				s.GetComponent<Renderer>().material.color = lerpedColor;
			}
		}
	}
}
