using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class EquipedMissile : MonoBehaviour
{
	private Transform script;
	public bool equiping;

	void Start()
	{
		script = GameObject.Find("script").transform;
	}

	IEnumerator changeEquiped(bool equipedBool, int missileLevelListId, bool att)
	{
		var equipedInt = (equipedBool) ? 0 : 1;
		var attInt = (att) ? 1 : 0;
		var playerId = PlayerPrefs.GetInt("idPlayer");
		var url = "http://5.196.68.62/edit/edit/missile/equiped/player/"+playerId+"/"+missileLevelListId+"/" + equipedInt + "/" + attInt;
		WWW www = new WWW(url);
		yield return (www);
		var e = transform.parent.parent.parent.Find("LineEquipedScroll").Find("Selection").transform;
		int i = 0;
		Texture[] textureList = new Texture[e.childCount];
		foreach (Transform a in e)
		{
			if (equipedBool == false)
			{
				if (a != null && a.GetComponent<RawImage>().texture == null)
				{
					a.GetComponent<RawImage>().texture = transform.Find("Image").GetComponent<RawImage>().texture;
					break ;
				}
			}
			else if (equipedBool == true)
			{
				if (a != null && a.GetComponent<RawImage>().texture == transform.Find("Image").GetComponent<RawImage>().texture)
				{
					a.GetComponent<RawImage>().texture = null;
					break ;
				}
			}
		}

		var missilesReview = script.GetComponent<GenerateProd>().missilesReview;
		foreach (var m in missilesReview)
		{
			if (missileLevelListId == m.missile.GetComponent<InfoMissile>().missileLevelListId)
			{
				if (att)
					m.equipedAtt = !equipedBool;
				else
					m.equipedDef = !equipedBool;
				break ;
			}
		}
		//reorder
		foreach (Transform a in e)
		{
			if (a.GetComponent<RawImage>().texture != null)
			{
				textureList[i] = a.GetComponent<RawImage>().texture;
				i++;
			}
		}
		i = 0;
		foreach (Transform a in e)
		{
			a.GetComponent<RawImage>().texture = textureList[i];
			i++;
		}
		equiping = false;
	}

	IEnumerator changeEquiped2(bool equipedBool, int missileLevelListId, bool att)
	{
		var equipedInt = (equipedBool) ? 0 : 1;
		var attInt = (att) ? 1 : 0;
		var playerId = PlayerPrefs.GetInt("idPlayer");
		var url = "http://5.196.68.62/edit/edit/missile/equiped/player/"+playerId+"/"+missileLevelListId+"/" + equipedInt + "/" + attInt;
		WWW www = new WWW(url);
		yield return (www);
		var e = transform.parent.parent.parent.Find("LineEquipedScroll").Find("Selection").transform;
		int i = 0;
		Texture[] textureList = new Texture[e.childCount];
		GetComponent<RawImage>().texture = null;
		var missilesReview = script.GetComponent<GenerateProd>().missilesReview;
		foreach (var m in missilesReview)
		{
			if (missileLevelListId == m.missile.GetComponent<InfoMissile>().missileLevelListId)
			{
				if (att)
					m.equipedAtt = !equipedBool;
				else
					m.equipedDef = !equipedBool;
				break ;
			}
		}
		// //reorder
		foreach (Transform a in e)
		{
			if (a.GetComponent<RawImage>().texture != null)
			{
				textureList[i] = a.GetComponent<RawImage>().texture;
				i++;
			}
		}
		i = 0;
		foreach (Transform a in e)
		{
			a.GetComponent<RawImage>().texture = textureList[i];
			i++;
		}
		equiping = false;
	}

	public void equiped()
	{
		if (equiping)
			return ;
		var id = GetComponent<SmallInfoUiMissile>().missileLevelListId;
		bool att = GetComponent<SmallInfoUiMissile>().att;
		var equipedBool = script.GetComponent<MissileProdScript>().isEquiped(id, att);
		equiping = true;
		StartCoroutine(changeEquiped(equipedBool, id, att));
	}

	public void desEquipedLine(bool att)
	{
		if (equiping)
			return ;
		equiping = true;
		Transform e = null;
		if (att)
			e = transform.parent.parent.parent.Find("AttackLineScroll").Find("Selection").transform;
		else
			e = transform.parent.parent.parent.Find("DefenceLineScroll").Find("Selection").transform;
		foreach (Transform a in e)
		{
			if (a.Find("Image").GetComponent<RawImage>().texture == GetComponent<RawImage>().texture)
			{
				var id = a.GetComponent<SmallInfoUiMissile>().missileLevelListId;
				var equipedBool = script.GetComponent<MissileProdScript>().isEquiped(id, att);
				StartCoroutine(changeEquiped2(equipedBool, id, att));
				break ;
			}
		}
	}
}