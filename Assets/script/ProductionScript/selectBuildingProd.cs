using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SelectBuildingProd : MonoBehaviour
{
	public Transform[] building;
	public Transform arrow;
	public bool haveBuildingSelected;
	public Text textGenerate;
	private Vector3 positionNewBuilding;
	private Transform terrain;
	private InfoUser InfoUser;
	private Transform script;

	void Start()
	{
		script = GameObject.Find("script").transform;
		InfoUser = script.GetComponent<InfoUser>();
		terrain = GameObject.Find("terrain").transform;
	}

	//return position look at camera on MouseSensitive plane
	Vector3 getPositionCenterCamera(Transform g)
	{

		var v = Camera.main.transform.position;
		v = new Vector3(v.x, v.y, 0);
		return (calcFinalPosition(v.x, v.y, g));
	}

	Vector2 calcFinalPosition(float x, float y, Transform g)
	{
		if (script == null)
			script = GameObject.Find("script").transform;
		float distance = -1;
		var v = new Vector2(x, y);
		Vector2 posFound = v;
		int lx = (int)g.Find("spriteFloor").localScale.x / 2;
		int ly = (int)g.Find("spriteFloor").localScale.y / 2;
		var caseObject = script.GetComponent<GenerateProd>().caseObject;
		for (int i = ly; i < script.GetComponent<GenerateProd>().sizeY - ly; i++)
		{
			for (int j = lx; j < script.GetComponent<GenerateProd>().sizeX - lx; j++)
			{
				var tempDist = Vector2.Distance(v, caseObject[i, j].position);
				if (distance == -1 || tempDist < distance)
				{
					if (g.GetComponent<BuildingInfo>().att || (g.GetComponent<BuildingInfo>().def && caseObject[i, j].isDefense))
					{
						posFound = caseObject[i, j].position;
						distance = tempDist;
					}
				}
			}
		}
		return (posFound);
	}

	// bool isInpositionMatrixArray(Vector2 p, bool[,] positionMapMatrix, Vector3[,] isoMapMatrix)
	// {
	// 	for (int i = 0; i < script.GetComponent<GenerateProd>().sizeX; i++)
	// 	{
	// 		for (int j = 0; j < script.GetComponent<GenerateProd>().sizeY; j++)
	// 		{
	// 			if (isoMapMatrix[i, j].x == p.x && isoMapMatrix[i, j].y == p.y)
	// 			{
	// 				if (positionMapMatrix[i, j])
	// 					return (true);
	// 			}
	// 		}
	// 	}
	// 	return (false);
	// }

	public bool canPlaceHere(Transform g, int y, int x, Case[,] caseObject, bool byPassIsEmpty = false)
	{
		int sx = (int)(g.Find("spriteFloor").transform.localScale.x / 2.0f);
		int sy = (int)(g.Find("spriteFloor").transform.localScale.y / 2.0f);
		int x2Base = x - sx;
		int y2Base = y - sy;
		int y2 = y2Base;
		int x2 = x2Base;
		while (y2 < y2Base + (int)g.Find("spriteFloor").transform.localScale.y)
		{
			x2 = x2Base;
			while (x2 < x2Base + (int)g.Find("spriteFloor").transform.localScale.x)
			{
				if (y2 < 0 || y2 >= script.GetComponent<GenerateProd>().sizeY || x2 < 0 || x2 >= script.GetComponent<GenerateProd>().sizeX)
				{

				}
				else
				{
					if ((!byPassIsEmpty && !caseObject[y2, x2].isEmpty) || (g.GetComponent<BuildingInfo>().def && !caseObject[y2, x2].isDefense) || (g.GetComponent<BuildingInfo>().att && caseObject[y2, x2].isDefense))
						return (false);
				}
				x2++;
			}
			y2++;
		}
		return (true);
	}

	void findPlacement(Vector3 pos, Transform g)
	{
		var decallage = new Vector2(0, 0);
		if (g.Find("spriteFloor").localScale.x % 2 == 0)
			decallage += new Vector2(-0.5f, 0);
		// if (g.Find("spriteFloor").localScale.y % 2 == 0)
		// 	decallage += new Vector2(0, -0.5f);	
		var caseObject = script.GetComponent<GenerateProd>().caseObject;
		int x;
		int y;
		float distance = -1;
		Vector2 posFound = pos;

		y = 0;
		while (y < script.GetComponent<GenerateProd>().sizeY)
		{
			x = 0;
			while (x < script.GetComponent<GenerateProd>().sizeX)
				{
				if ((g.GetComponent<BuildingInfo>().att || (g.GetComponent<BuildingInfo>().def && caseObject[y, x].isDefense)) && canPlaceHere(g, y, x, caseObject))
				{
					var tempDist = Vector2.Distance(pos, caseObject[y, x].position);
					if (distance == -1 || tempDist < distance)
					{
						g.GetComponent<BuildingInfo>().oldPosMatrixX = g.GetComponent<BuildingInfo>().posMatrixX;
						g.GetComponent<BuildingInfo>().oldPosMatrixY = g.GetComponent<BuildingInfo>().posMatrixY;
						g.GetComponent<BuildingInfo>().posMatrixX = x;
						g.GetComponent<BuildingInfo>().posMatrixY = y;
						posFound = caseObject[y, x].position;
						distance = tempDist;
					}
				}
				x++;
			}
			y++;
		}
		positionNewBuilding = posFound + decallage;
	}

	// get building number array with name
	int getBuildingByName(string name, int level)
	{
		for (int i = 0; i < building.Length; i++)
		{
			if (building[i].GetComponent<BuildingInfo>().name == name && building[i].GetComponent<BuildingInfo>().level == level)
				return (i);
		}
		return (-1);
	}

	public Transform createSelectableBuilding(int i, string name = "", int level = 0, bool isNewBuilding = false, bool forcePlacement = false)
	{
		if (name != "" && level > 0) // if name && level
		{
			i = getBuildingByName(name, level);
			if (i < 0)
			{
				//textGenerate.text = "Error : Building not found.";
				Debug.Log ("Error : Building " + name + " Level " + level + " not found.");
				return (null);
			}
		}
		var buildingSelected = Instantiate(building[i], building[i].position, building[i].rotation) as Transform; // create building
		if (isNewBuilding)
		{
			buildingSelected.position = getPositionCenterCamera(buildingSelected); // set position (center camera)
			positionNewBuilding = new Vector3(-99, -99, -99);
			findPlacement(buildingSelected.position, buildingSelected); // find placement empty
			buildingSelected.position = positionNewBuilding; // positionNewBuilding have been changed in findPlacement();
			buildingSelected.GetComponent<BuildingPlacementProd>().oldPosition = positionNewBuilding;
		}
		// if (forcePlacement)
		// 	findPlacement(buildingSelected.position, buildingSelected);
		if (buildingSelected.position.x == -99) // if can't place building, destroy it
		{
			Debug.Log ("Error in create building");
			Destroy(buildingSelected.gameObject);
			return (null);
		}
		buildingSelected.GetComponent<BuildingInfo>().idHash = Random.Range(1000, 10000000); // set id 
		buildingSelected.GetComponent<BuildingPlacementProd>().newBuilding = isNewBuilding;
		if (isNewBuilding)
			buildingSelected.GetComponent<BuildingPlacementProd>().checkForSelect(); // select this building
		buildingSelected.gameObject.SetActive(true);
		buildingSelected.GetComponent<BuildingPlacementProd>().loadVar();
		return (buildingSelected);
	}
	
	//create building in standalone with pad number (0, 1, 2, ...)
	void createWithKey()
	{
		if (haveBuildingSelected)
			return ;
		for (int i = 0; i < building.Length; i++)
		{
			if (i >= 9)
				return ;
			if (Input.GetKeyDown("" + (i + 1)))
			{
				if (createSelectableBuilding(i, "", 0, true) != null)
					haveBuildingSelected = true;
			}
		}
	}

	void Update ()
	{
		#if UNITY_EDITOR || UNITY_STANDALONE
			createWithKey(); // create building in standalone
		#endif
	}
}
 