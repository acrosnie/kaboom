using UnityEngine;
using System.Collections;

public class ButtonDisplayMenuBuilding : MonoBehaviour
{
	public GameObject[] buttonsList;
	public GameObject menuSelectBuilding;
	public GameObject mouseCollider;
	public GameObject cameraTarget;

	void unactiveButton()
	{
		int i = -1;

		while (++i < buttonsList.Length)
		{
			buttonsList[i].SetActive(false);
		}
	}

	public void displayMenuBuilding()
	{
		mouseCollider.GetComponent<MouseSensitive>().unselectAll();
		mouseCollider.GetComponent<MouseSensitive>().unactiveSensitive = true;
		menuSelectBuilding.SetActive(true);
		cameraTarget.GetComponent<Move>().block = true;
		unactiveButton();
	}

	public void clickTrigger()
	{
		displayMenuBuilding();
	}
}
