using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Move : MonoBehaviour
{
	public bool block;
	public bool isMove;
	public bool reallyMove;
	public float perspectiveZoomSpeed;
	private float xAxisDir;
	private float yAxisDir;
	private Transform terrain;
	private float tTurn;
	private float timeC;
	private Vector3 dest;
	private bool limiteLeft;
	private bool limiteRight;
	private bool limiteTop;
	private bool limiteBottom;
	private Vector3 velocity = Vector3.zero;
	private Camera _camera;
	public float MoveSensitivity = 2.0f;
	public float inertiaDuration = 1.0f;
	public float maxZoom, minZoom = 0.0f;
	private float scrollVelocity = 0.0f;
	private float timeTouchPhaseEnded;
	private Vector3 scrollDirection = Vector3.zero;
	private Vector3 oldPos;
	public Transform textDebug;
	private Rect rect = new Rect(-0.3f, -0.3f, 1.6f, 1.6f);
	private Vector2 delta;
	private Rigidbody2D rb;
	private Vector3 desiredVelocity;
	private float lastSqrMag;
	private Transform script;

	void Start()
	{
		script = GameObject.Find("script").transform;
		rb = GetComponent<Rigidbody2D>();
		_camera = Camera.main;
		dest = transform.position;
		#if UNITY_EDITOR || UNITY_STANDALONE
			perspectiveZoomSpeed *= 10;
		#endif
		terrain = GameObject.Find("terrain").transform;
		lastSqrMag = Mathf.Infinity;
	}

	void zoomInMobile()
	{
		Touch touchZero = Input.GetTouch(0);
		Touch touchOne = Input.GetTouch(1);
		Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
		Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
		float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
		float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
		float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
		if (deltaMagnitudeDiff > 1.0f)
			_camera.orthographicSize += perspectiveZoomSpeed * Time.deltaTime;
		else if (deltaMagnitudeDiff < -1.0f)
			_camera.orthographicSize -= perspectiveZoomSpeed * Time.deltaTime;
		// if (_camera.orthographicSize < minZoom)
		// 	_camera.orthographicSize = minZoom;
		// if (_camera.orthographicSize > maxZoom)
		// 	_camera.orthographicSize = maxZoom;
	}

	void touchMove()
	{
		#if UNITY_ANDROID// && !UNITY_EDITOR
			Touch[] touches = Input.touches;
			terrain.Find("mouseCollider").GetComponent<MouseSensitive>().unactiveSensitive = false;
			if (touches.Length < 1) // if no touch
			{
				if (scrollVelocity != 0.0f) // if velocity
				{
					float t = (Time.time - timeTouchPhaseEnded) / inertiaDuration;
					float frameVelocity = Mathf.Lerp (scrollVelocity, 0.0f, t);
					defineTranslateCam(scrollDirection.normalized * (frameVelocity * 0.0001f)); // Move camera...
					if (t >= 1.0f)
						scrollVelocity = 0.0f;
				}
			}
			else if (touches.Length > 0) // if touch
			{
				if (touches.Length == 1)
				{
					if (touches[0].phase == TouchPhase.Began)
						scrollVelocity = 0.0f;
					else if (touches[0].phase == TouchPhase.Moved) // if drag, calcul velocity
					{
						delta = touches[0].deltaPosition;
						var v = new Vector3(-delta.x, -delta.y, 0);
						var pTemp = transform.position;
						defineTranslateCam(v * MoveSensitivity); // Move camera
						scrollDirection = v.normalized;
						scrollVelocity = v.magnitude / touches[0].deltaTime;
						if (scrollVelocity <= 400) // 280
						{
							transform.position = pTemp;
							desiredVelocity = Vector3.zero;
							scrollVelocity = 0;
						}
					}
					else if (touches[0].phase == TouchPhase.Ended)
					{
						block = false;
						timeTouchPhaseEnded = Time.time;
					}
				}
			else if (touches.Length >= 2)
				zoomInMobile();
			}
		#endif
	}

	// calcul translate and define velocity
	void defineTranslateCam(Vector3 n)
	{
		transform.Translate(n);
		// var MoveDirection = transform.TransformDirection(n);
		// desiredVelocity = MoveDirection;
	}

	//Move in unity editor with arrow
	void standaloneMove()
	{
		#if UNITY_EDITOR || UNITY_STANDALONE
			if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey("d")) //RIGHT
			{
				timeC = 0;
				defineTranslateCam(new Vector3(1, 0, 0) * MoveSensitivity * 12);
			}
			else if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey("a")) // LEFT
			{
				timeC = 0;
				defineTranslateCam(new Vector3(-1, 0, 0) * MoveSensitivity * 12);
			}
			else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey("s")) // DOWN
			{
				timeC = 0;
				defineTranslateCam(new Vector3(0, -1, 0) * MoveSensitivity * 12);
			}
			else if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey("w")) // UP
			{
				timeC = 0;
				defineTranslateCam(new Vector3(0, 1, 0) * MoveSensitivity * 12);
			}
			if (Input.GetAxis("Mouse ScrollWheel") < 0)
			{
				_camera.orthographicSize += perspectiveZoomSpeed * Time.deltaTime;
			}
			if (Input.GetAxis("Mouse ScrollWheel") > 0)
			{
				_camera.orthographicSize -= perspectiveZoomSpeed * Time.deltaTime;
			}	
		#endif
	}

	void Update()
	{
		if (block)
		{
			//rb.velocity = desiredVelocity;
			return ;
		}
		oldPos = _camera.transform.position;
		standaloneMove();
		touchMove();
		//rb.velocity = desiredVelocity;
	}

	void LateUpdate()
	{
		transform.position = new Vector3(
			Mathf.Clamp(transform.position.x, -20, 20), 
			Mathf.Clamp(transform.position.y, -20, 20), transform.position.z
		);
		_camera.orthographicSize = Mathf.Clamp(_camera.orthographicSize, minZoom, maxZoom);
	}

	public void setVelocity(Vector3 v)
	{
		defineTranslateCam(v);
	}
}
