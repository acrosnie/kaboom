using UnityEngine;
using System.Collections;

public class ImgFond : MonoBehaviour
{
	public GameObject[] buttonsList;
	public GameObject menuSelectBuilding;
	public GameObject cameraTarget;
	public GameObject mouseCollider;

	// in menu , if click on img (invisible), hide all menu
	void activeButton()
	{
		int i = -1;

		while (++i < buttonsList.Length)
		{
			buttonsList[i].SetActive(true);
		}
	}

	public void clickOnImg()
	{
		activeButton();
		cameraTarget.GetComponent<Move>().block = false;
		mouseCollider.GetComponent<MouseSensitive>().unactiveSensitive = false;
		menuSelectBuilding.SetActive(false);
	}
}
