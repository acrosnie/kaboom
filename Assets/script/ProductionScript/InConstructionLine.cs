using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class InConstructionLine : MonoBehaviour
{
	public bool ready;
	public float timeMax;
	public Sprite spriteDefault;
	public int missileLevelListId;
	public InfoMissile missileInfo;
	public int i;
	public int missileConstructTimestamp;
	private float t;
	private GenerateResource g;

	void Start()
	{
		g = GameObject.Find("mainScript").GetComponent<GenerateResource>();
	}

	void Update()
	{
		if (!ready)
			return ;
		t = (int)g.timestamp - missileConstructTimestamp;
		GetComponent<Image>().fillAmount = (1.0f / timeMax) * t;
		transform.Find("TimeLeft").GetComponent<Text>().text = "" + ((int)(timeMax - t));
		var a = new LocationInfo();
		if (t >= timeMax)
		{
			GetComponent<Image>().fillAmount = 1;
			StartCoroutine(GameObject.Find("script").GetComponent<MissileProdScript>().addMissile(missileLevelListId, missileInfo));
			ready = false;
			t = 0;
			GetComponent<Image>().sprite = spriteDefault;
			transform.Find("StopButton").gameObject.SetActive(false);
			transform.Find("TimeLeft").gameObject.SetActive(false);
			PlayerPrefs.SetInt("missileConstructTimestamp" + i, 0);
			PlayerPrefs.SetInt("missileConstructMissileLevelListId" + i, 0);
		}
	}

	public void cancelAction()
	{
		GetComponent<Image>().fillAmount = 1;
		GetComponent<Image>().sprite = spriteDefault;
		ready = false;
		timeMax = 0;
		missileLevelListId = 0;
		t = 0;
		GameObject.Find("script").GetComponent<GenerateProd>().recupRessource((int)missileInfo.ressource1Cost, (int)missileInfo.ressource2Cost, (int)missileInfo.ressource3Cost);
		missileInfo = null;
		transform.Find("StopButton").gameObject.SetActive(false);
		transform.Find("TimeLeft").gameObject.SetActive(false);
		PlayerPrefs.SetInt("missileConstructTimestamp" + i, 0);
		PlayerPrefs.SetInt("missileConstructMissileLevelListId" + i, 0);
		GameObject.Find("script").GetComponent<MissileProdScript>().majCostTotalInstantBuildMissile();
	}
}