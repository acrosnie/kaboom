<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once("config.php");

main();

function check()
{
	if ((isset($_GET['getInfoUser']) || isset($_GET['updateInfoUser'])) && isset($_GET['pseudo']))
	{
		if (strlen($_GET['pseudo']) > 50)
			return (false);
		if (!userExist($_GET['pseudo']))
			return (false);
		return (true);
	}
	return (false);
}

function userExist($pseudo)
{
	$req = $GLOBALS["bdd"]->prepare('SELECT id FROM user WHERE pseudo = ?');
	$req->execute(array($pseudo));
	$result = $req->fetchAll(PDO::FETCH_ASSOC);
	if (!isset($result[0]) || count($result[0]) == 0)
		return (false);
	else
		return ($result[0]['id']);
}

function getInfoUser($pseudo)
{
	$req = $GLOBALS["bdd"]->prepare('SELECT * FROM user WHERE pseudo = ?');
	$req->execute(array($pseudo));
	$result = $req->fetchAll(PDO::FETCH_ASSOC);
	$result[0]['dateNow'] = date('m/d/Y h:i:s a', time());
	$result[0]['differenceInSeconds'] = strtotime($result[0]['dateNow']) - strtotime($result[0]['dateLastConnection']);
	echo json_encode($result);
}

function updateInfoUser($pseudo)
{
	$req = $GLOBALS["bdd"]->prepare('UPDATE user SET DateLastConnection = NOW() WHERE pseudo = ?');
	$req->execute(array($pseudo));
	// print ("done");
}

function getImg()
{
	$req = $GLOBALS["bdd"]->prepare('SELECT img FROM buildingInfoLevel');
	$req->execute();
	$result = $req->fetchAll(PDO::FETCH_ASSOC);
	echo json_encode($result);
}

function main()
{
	if (isset($_GET['getInfoUser']))
	{
		if (check())
			getInfoUser($_GET['pseudo']);
	}
	else if (isset($_GET['updateInfoUser']))
	{
		if (check())
			updateInfoUser($_GET['pseudo']);
	}
	else if (isset($_GET['img']))
		getImg();
}

?>