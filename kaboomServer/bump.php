<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once("config.php");
//http://5.196.68.62/bump.php?lat=90.03&&lng=92.36&&ip=5.196.68.62
main();

function distance($lat1, $lon1, $lat2, $lon2, $unit)
{
	$theta = $lon1 - $lon2;
	$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	$dist = acos($dist);
	$dist = rad2deg($dist);
	$miles = $dist * 60 * 1.1515;
	$unit = strtoupper($unit);

	if ($unit == "K")
		return ($miles * 1.609344);
	else if ($unit == "N")
		return ($miles * 0.8684);
	else
		return $miles;
}

function verif()
{
	if (!isset($_GET['search']))
	{
		if (!is_numeric($_GET['lat']) || !is_numeric($_GET['lng']) || !is_numeric($_GET['id']))
			return (false);
	}
	if ($_GET['id'] < 1000000000 || $_GET['id'] > 2000000000)
		return (false);
	return (true);
}

function insert()
{
	$date = date('Y-m-d H:i:s');
	$array = array(
		"lat" => $_GET['lat'], "lng" => $_GET['lng'], "idPlayer" => $_GET['id'],
		"ip" => $_SERVER['REMOTE_ADDR'], "dateCreate" => $date , "dateUpdate" => $date
	);
	$req = $GLOBALS["bdd"]->prepare('INSERT INTO bump(lat, lng, idPlayer, ip, dateCreate, dateUpdate) VALUES(:lat, :lng, :idPlayer, :ip, :dateCreate, :dateUpdate)');
	$req->execute($array);
}

function selectUser($ip, $id)
{
	$req = $GLOBALS["bdd"]->prepare('SELECT * FROM bump WHERE ip = ? AND idPlayer = ?');
	$req->execute(array($ip, $id));
	$result = $req->fetchAll(PDO::FETCH_ASSOC);
	if (!isset($result[0]) || count($result[0]) == 0)
		return (null);
	else
		return ($result[0]);	
}

function selectAll()
{
	$req = $GLOBALS["bdd"]->prepare('SELECT * FROM bump');
	$req->execute();
	$result = $req->fetchAll(PDO::FETCH_ASSOC);
	if (!isset($result[0]) || count($result[0]) == 0)
		return (array());
	else
		return ($result);	
}

function checkIfExist()
{
	$req = $GLOBALS["bdd"]->prepare('SELECT * FROM bump WHERE ip = ? AND idPlayer = ?');
	$req->execute(array($_SERVER['REMOTE_ADDR'], $_GET['id']));
	$result = $req->fetchAll(PDO::FETCH_ASSOC);
	if (!isset($result[0]) || count($result[0]) == 0)
		return (0);
	else if ($result[0]["lat"] != $_GET['lat'] || $result[0]["lng"] != $_GET['lng'] || $result[0]["idPlayer"] != $_GET['id'])
		return (1);
	return (2);
}

function userExist($ip, $id)
{
	if (selectUser($ip, $id) == null)
		return (false);
	return (true);
}

function updateUser()
{
	$req = $GLOBALS["bdd"]->prepare('UPDATE bump SET lat = :lat, lng = :lng, idPlayer = :idPlayer WHERE ip = :ip AND idPlayer = :id');
	$req->execute(array(
		'lat' => $_GET['lat'],
		'lng' => $_GET['lng'],
		'idPlayer' => $_GET['id'],
		'ip' => $_SERVER['REMOTE_ADDR']
	));
}

function deleteOld()
{
	$select = selectAll();
	foreach ($select as $key => $value)
	{
		$timeDiff = abs(strtotime("now") - strtotime($value['dateCreate']));
		if ($timeDiff > 150)
		{
			$req = $GLOBALS["bdd"]->prepare('DELETE FROM bump WHERE ip = ? AND idPlayer = ?');
			$req->execute(array($value['ip'], $value['idPlayer']));
		}
	}
}

function addToDb()
{
	$r = checkIfExist();
	if ($r == 0)
		insert();
	else if ($r == 1)
		updateUser();
	print("success");
}

function askBump()
{
	if (verif())
		addToDb();
}

function printArray($array)
{
	echo "<pre>";
	print_r($array);
	echo "</pre>";
}

function updateUsersMatch($user, $usertmp, $dist)
{
	$req = $GLOBALS["bdd"]->prepare('UPDATE bump SET role = :role, idMatch = :idMatch, ipMatch = :ipMatch, distance = :distance WHERE ip = :ip AND idPlayer = :idPlayer');
	$req->execute(array(
		'role' => 1,
		'idMatch' => $usertmp['idPlayer'],
		'ipMatch' => $usertmp['ip'],
		'distance' => $dist,
		'ip' => $user['ip'],
		'idPlayer' => $user['idPlayer']
	));
	$req = $GLOBALS["bdd"]->prepare('UPDATE bump SET role = :role, idMatch = :idMatch, ipMatch = :ipMatch, distance = :distance WHERE ip = :ip AND idPlayer = :idPlayer');
	$req->execute(array(
		'role' => 2,
		'idMatch' => $user['idPlayer'],
		'ipMatch' => $user['ip'],
		'distance' => $dist,
		'ip' => $usertmp['ip'],
		'idPlayer' => $usertmp['idPlayer']
	));
}

function searchMatch($user)
{
	$select = selectAll();
	$usertmp = null;
	$distance = -1;
	foreach ($select as $key => $value)
	{
		if ($value['idPlayer'] != $user['idPlayer'])
		{
			$dist = distance($user['lat'], $user['lng'], $value['lat'], $value['lng'], 'k');
			$timeDiff = abs(strtotime($user['dateCreate']) - strtotime($value['dateCreate']));

			if ($dist > 0.05 || $timeDiff > 10) // 0.05 km // si les 2 players sont trop eloigne on ignore; si il y a plus de X sec d'intervalle on ignore
				continue ;
			if ($dist < $distance || $distance == -1)
			{
				$distance = $dist;
				$usertmp = $value;
			}
		}
	}
	if (isset($usertmp)) // un utilisateur a ete trouve !
	{
		updateUsersMatch($user, $usertmp, $distance);
		getInfo($user);
	}
	else
		print("nobody");
}

function getInfo($user)
{
	$user = selectUser($user['ip'], $user['idPlayer']);
	//print($user['role'].";".$user['idMatch'].";".$user['ipMatch']);
	print($user['role']);
}

function lookAtUser()
{
	$user = selectUser($_SERVER['REMOTE_ADDR'], $_GET['id']);
	if (isset($user))
	{
		if ($user['role'] == 0 && $user['idMatch'] == 0)
			searchMatch($user);
		else
			getInfo($user);
	}
	else
		print ("error");
}

function searchBump()
{
	if (verif())
		lookAtUser();
	else
		print ("error");
}

function updateRoom()
{
	$user = selectUser($_SERVER['REMOTE_ADDR'], $_GET['id']);
	if (isset($user))
	{
		$user2 = selectUser($user['ipMatch'], $user['idMatch']);
		$req = $GLOBALS["bdd"]->prepare('UPDATE bump SET nameRoom = :nameRoom WHERE ip = :ip AND idPlayer = :idPlayer');
		$req->execute(array(
			'nameRoom' => $_GET['nameRoom'],
			'ip' => $user['ip'],
			'idPlayer' => $user['idPlayer']
		));
		$req = $GLOBALS["bdd"]->prepare('UPDATE bump SET nameRoom = :nameRoom WHERE ip = :ip AND idPlayer = :idPlayer');
		$req->execute(array(
			'nameRoom' => $_GET['nameRoom'],
			'ip' => $user2['ip'],
			'idPlayer' => $user2['idPlayer']
		));
	}
}

function getRoomName()
{
	$user = selectUser($_SERVER['REMOTE_ADDR'], $_GET['id']);
	if (isset($user))
	{
		if ($user['nameRoom'] != "")
			print($user['nameRoom']);
		else
			print ("noRoom");
	}
}

function main()
{
	if (isset($_GET['lat']) && isset($_GET['lng']) && isset($_GET['id']))
	{
		deleteOld();
		askBump();
	}
	else if (isset($_GET['id']) && isset($_GET['search']))
		searchBump();
	else if (isset($_GET['updateRoom']) && isset($_GET['nameRoom']) && isset($_GET['id']))
		updateRoom();
	else if (isset($_GET['infoRoom']) && isset($_GET['id']))
		getRoomName();
	return (0);
}
?>