<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once("config.php");

main();

function displayArray($array)
{
	echo "<pre>";
	var_dump($array);
	echo "</pre>";
}

function buildingExist($name)
{
	$req = $GLOBALS["bdd"]->prepare('SELECT id FROM buildingList WHERE name = ?');
	$req->execute(array($name));
	$result = $req->fetchAll(PDO::FETCH_ASSOC);
	if (!isset($result[0]) || count($result[0]) == 0)
		return (false);
	else
		return ($result[0]['id']);
}

function entryExist($idBuilding, $level)
{
	$req = $GLOBALS["bdd"]->prepare('SELECT id FROM buildingInfoLevel WHERE IdBuilding = ? AND level = ?');
	$req->execute(array($idBuilding, $level));
	$result = $req->fetchAll(PDO::FETCH_ASSOC);
	if (!isset($result[0]) || count($result[0]) == 0)
		return (false);
	else
		return ($result[0]['id']);
}

function insertEntry($array)
{
	$req = $GLOBALS["bdd"]->prepare(
			'INSERT INTO buildingInfoLevel
			(IdBuilding, Level, Health, NukeSoda, GalacticFizz, Time, CapacityMissiles, CapacityNukeSoda, CapacityGalacticFizz, ProdRateNukeSoda, ProdRateGalacticFizz, FiringRate, RessourcesSaved, IEMtime, ShieldCapacity)
			VALUES(:IdBuilding, :Level, :Health, :NukeSoda, :GalacticFizz, :Time, :CapacityMissiles, :CapacityNukeSoda, :CapacityGalacticFizz, :ProdRateNukeSoda, :ProdRateGalacticFizz, :FiringRate, :RessourcesSaved, :IEMtime, :ShieldCapacity)'
	);
	$req->execute($array);
}

function updateEntry($array)
{
	$req = $GLOBALS["bdd"]->prepare('UPDATE buildingInfoLevel SET Health = :Health, NukeSoda = :NukeSoda, GalacticFizz = :GalacticFizz, Time = :Time, CapacityMissiles = :CapacityMissiles, CapacityNukeSoda = :CapacityNukeSoda,
			CapacityGalacticFizz = :CapacityGalacticFizz, ProdRateNukeSoda = :ProdRateNukeSoda, ProdRateGalacticFizz = :ProdRateGalacticFizz, FiringRate = :FiringRate, RessourcesSaved = :RessourcesSaved, IEMtime = :IEMtime, ShieldCapacity = :ShieldCapacity
			WHERE IdBuilding = :IdBuilding AND Level = :Level');
	$req->execute($array);
}

function createArray($csv)
{
	$row = 1;
	$arrayCsv = array();
	$arrayCsvChamp = array();

	if (($handle = fopen($csv, "r")) !== FALSE)
	{
		while (($data = fgetcsv($handle, 1000, ";")) !== FALSE)
		{
			$num = count($data);
			for ($c=0; $c < $num; $c++)
			{
				if ($row == 1)
				{
					//echo $data[$c]." <br />";
					$arrayCsvChamp[$c] = $data[$c]; // on cree l'array
					if ($data[$c] == "Name")
						$arrayCsvChamp[$c] = "IdBuilding";	
				}
				else
				{
					$arrayCsv[$row][$arrayCsvChamp[$c]] = $data[$c];
					if ($arrayCsvChamp[$c] == "IdBuilding")
						$arrayCsv[$row][$arrayCsvChamp[$c]] = buildingExist($data[$c]);
				}
			}
			$row++;
		}
	}
	else
		return (null);
	print_r($arrayCsv);
	return ($arrayCsv);
}

function fillDatabase($csv)
{
	$arrayCsv = createArray($csv);
	// foreach ($arrayCsv as $key => $value)
	// {
	// 	if (!entryExist($value["IdBuilding"], $value["Level"]))
	// 		insertEntry($value);
	// 	// else
	// 	// 	updateEntry($value);
	// }
	echo "DataBase Modified Success !";
}

function readAndDisplayCsv($csv)
{
	$row = 1;
	if (($handle = fopen($csv, "r")) !== FALSE)
	{
		while (($data = fgetcsv($handle, 1000, ";")) !== FALSE)
		{
			$num = count($data);
			echo "<p> $num champs à la ligne $row: <br /></p>\n";
			$row++;
			for ($c=0; $c < $num; $c++)
			{
				echo $data[$c] . "<br />\n";
			}
		}
		fclose($handle);
	}
}

function check()
{
	if (isset($_POST["submit"]))
	{

		if (isset($_FILES["file"]))
		{
			if ($_FILES["file"]["error"] > 0)
				echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
			else
			{
				//Store file in directory "upload" with the name of "uploaded_file.txt"
				$storagename = "csvBuilding.csv";
				move_uploaded_file($_FILES["file"]["tmp_name"], "upload/" . $storagename);
				fillDatabase("upload/csvBuilding.csv");
			}
		}
	}
}

function main()
{
	check();
}

?>
<h1>CSV LEVEL BUY BUILDING</h1>
<table width="600">
<form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" enctype="multipart/form-data">

<tr>
<td width="20%">Select file</td>
<td width="80%"><input type="file" name="file" id="file" /></td>
</tr>

<tr>
<td>Submit</td>
<td><input type="submit" name="submit" /></td>
</tr>

</form>
</table>
