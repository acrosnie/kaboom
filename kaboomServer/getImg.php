<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once("config.php");

function getImg($level, $id)
{
	$req = $GLOBALS["bdd"]->prepare('SELECT img FROM buildingInfoLevel WHERE Level = ? AND IdBuilding = ?');
	$req->execute(array($level, $id));
	$result = $req->fetchAll(PDO::FETCH_ASSOC);
	return ($result);
}

if (isset($_GET['name']) && isset($_GET['level']))
{
	$name = explode("@", $_GET['name']);
	$level = $_GET['level'];
	$a = getImg($level, $name[0]);
	if (!isset($a[0]))
		echo "NoImage.png";
	else
		echo $a[0]['img'];
}


?>