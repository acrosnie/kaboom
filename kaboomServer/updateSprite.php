<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once("config.php");

main();

function getBuildingList()
{
	$req = $GLOBALS["bdd"]->prepare('SELECT * FROM buildingList');
	$req->execute(array());
	$result = $req->fetchAll(PDO::FETCH_ASSOC);
	return ($result);
}

function getImgBuilding()
{
	$req = $GLOBALS["bdd"]->prepare('SELECT DISTINCT img FROM buildingInfoLevel');
	$req->execute(array());
	$result = $req->fetchAll(PDO::FETCH_ASSOC);
	return ($result);
}

function selectBuildingHtml()
{
	$building = "";
	$list = getBuildingList();
	foreach ($list as $key => $value) 
	{
		$building .= '<option value="'.$value["id"].'@'.$value["name"].'">'.$value["name"].'</option>';
	}
	return ($building);
}

function selectLevelHtml()
{
	$level = "";
	for ($i=1; $i < 30; $i++)
	{ 
		$level .= '<option value="'.$i.'">Level '.$i.'</option>';
	}
	return ($level);
}

function selectImageExist()
{
	$images = '<option value="">Choose</option>';;
	$list = getImgBuilding();
	foreach ($list as $key => $value) 
	{
		$images .= '<option value="'.$value["img"].'">'.$value["img"].'</option>';
	}
	return ($images);
}

function check()
{

}

function main()
{
	check();
}
$l = selectLevelHtml();
$b = selectBuildingHtml();
$ii = selectImageExist();
require_once("imgUploadScript.php");
?>

<table width="600">
<form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" enctype="multipart/form-data">

<tr>
<td>Choose building : </td>
<td>
<select name="name" id="selectbox1">
<?php
	echo $b;
?>
</select>
</td>
</tr>
<tr>
<td>Choose level : </td>
<td>
<select name="level" id="selectbox2">
<?php
	echo $l;
?>
</select>
</td>
</tr>
<tr>
<td>Choose existing Image : </td>
<td>
<select name="imageExist" id="selectbox3">
<?php
	echo $ii;
?>
</select>
</td>
</tr>
<tr>
<td width="20%">Select image</td>
<td width="80%"><input type="file" name="fileToUpload" accept="image/*"></td>
</tr>
<tr>
<td>Valider</td>
<td><input type="submit" name="submitImg" /></td>
</tr>

</form>
</table>
<div id = "imgContainer">
	<img class="imageFill" src="sprite/ajax-loader.gif" style="display:inline;"/>
</div>
<div id = "imgContainer2">
	<img class="imageChoose" src="sprite/ajax-loader.gif" style="display:inline;margin-left: 60%;"/>
</div>
<script type="text/javascript" src="jquery.min.js"></script>
<script type="text/javascript">
$('#selectbox1, #selectbox2').change(function()
{
	$("img.imageChoose").attr('src', 'sprite/ajax-loader.gif');
	$("img.imageFill").attr('src', 'sprite/ajax-loader.gif');
    var data = "";
    var name = $("#selectbox1").val();
    var level = $("#selectbox2").val();
    $.ajax({
        type:"GET",
        url : "/getImg.php",
        data: {name: name , level : level},
        async: false,
        success : function(response)
        {
        	$("img.imageFill").attr('src', 'sprite/' + response);
        },
        error: function() {
            alert('Error occured');
        }
    });
});
$('#selectbox3').change(function()
{
	$("img.imageChoose").attr('src', 'sprite/' + $("#selectbox3").val());
	if ($("#selectbox3").val() == "")
		$("img.imageChoose").attr('src', 'sprite/ajax-loader.gif');
	$("img.imageFill").attr('src', 'sprite/ajax-loader.gif');
});
</script>