<?php

function getImg($level, $id)
{
	$req = $GLOBALS["bdd"]->prepare('SELECT id FROM buildingInfoLevel WHERE Level = ? AND IdBuilding = ?');
	$req->execute(array($level, $id));
	$result = $req->fetchAll(PDO::FETCH_ASSOC);
	return ($result);
}

function updateDataBaseImg($img, $level, $id)
{
	$a = getImg($level, $id);
	if (!isset($a[0]))
	{
		echo "Veuillez d'abord ajouter ce batiment dans le csv !!!!";
		return ;
	}

	$req = $GLOBALS["bdd"]->prepare('UPDATE `buildingInfoLevel` SET`img`= ? WHERE Level = ? AND IdBuilding = ?');
	$req->execute(array($img, $level, $id));
	print ("Batiment Update");
}

if (isset($_POST['imageExist']) && !empty($_POST['imageExist']) && isset($_POST["name"]) && isset($_POST["level"]))
{
	$qq = explode("@", $_POST['name']);
	updateDataBaseImg($_POST['imageExist'], $_POST['level'], $qq[0]);
}
else
{
	$error = false;
	if (!isset($_POST["submitImg"]) || !isset($_POST["level"]) || !isset($_POST["name"]) || !!isset($_FILES))
		$error = true;
	if ($error == false && (!is_numeric($_POST["level"]) || $_POST["level"] < 1 || $_POST["level"] > 30))
		$error = true;
	function upload_img()
	{
		$target_dir = "sprite/";
		$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
		$uploadOk = 1;
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
		// Check if image file is a actual image or fake image
		if (isset($_POST["submitImg"]) && isset($_FILES["fileToUpload"]))
		{
			$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
			if ($check !== false)
			{
				echo "Image type - " . $check["mime"] . ".";
				$uploadOk = 1;
			}
			else
			{
				echo "This File is not an image.";
				$uploadOk = 0;
			}
		}
		// Check if file already exists
		if (file_exists($target_file))
		{	
			echo "Sorry, file already exists.";
			$uploadOk = 0;
		}
		// Check file size
		if ($_FILES["fileToUpload"]["size"] > 500000)
		{
			echo "Sorry, your file is too large.";
			$uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "png")
		{
			echo "Sorry, only PNG files are allowed.";
			$uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0)
		{
			echo "Sorry, your file was not uploaded.";
		// if everything is ok, try to upload file
		}
		else
		{
			$n = explode("@", $_POST["name"]);
			$name = "S_".$n[1]."_Lvl_".$_POST["level"].".png";
		    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_dir ."/". $name))
				echo "The file ". basename($name). " has been uploaded.";
		    else
				echo "Sorry, there was an error uploading your file.";
		}
	}

	if (!$error) // upload img
	{
		upload_img();
	}
	else if ($_POST)
		print("Error");
}
?>