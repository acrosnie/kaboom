<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once("config.php");

main();

function check()
{
	if (isset($_GET['addBuilding']) && isset($_GET['name']) && isset($_GET['x']) && isset($_GET['y']) && isset($_GET['z']) && isset($_GET['level']) && isset($_GET['health']) && isset($_GET['pseudo']) && isset($_GET['isCity']) && isset($_GET['isProd']))
	{

		if (!isset($_GET['nbrNukeNsoda']) || !isset($_GET['maxNukeNsoda']) || !isset($_GET['ratioSoda']) || !isset($_GET['nbrKbs']) || !isset($_GET['maxKbs']) || !isset($_GET['ratioKbs']) || !isset($_GET['nbrMissile']) || !isset($_GET['maxMissile']) || !isset($_GET['ratioMissile']))
			return (false);
		if (strlen($_GET['name']) > 50)
			return (false);
		if (strlen($_GET['pseudo']) > 50)
			return (false);
		if (!is_numeric($_GET['nbrNukeNsoda']) || !is_numeric($_GET['maxNukeNsoda']) || !is_numeric($_GET['ratioSoda']) || !is_numeric($_GET['x']) || !is_numeric($_GET['y']) || !is_numeric($_GET['z']) || !is_numeric($_GET['level']) || !is_numeric($_GET['health']))
			return (false);
		if (!userExist($_GET['pseudo']))
			return (false);
		return (true);
	}
	else if (isset($_GET['getBuilding']) && isset($_GET['pseudo']) && isset($_GET['isCity']) && isset($_GET['isProd']))
	{
		if (strlen($_GET['pseudo']) > 50)
			return (false);
		if (!userExist($_GET['pseudo']))
			return (false);

		return (true);
	}
	else if (isset($_GET['updateBuilding']) && isset($_GET['id']) && isset($_GET['name']) && isset($_GET['x']) && isset($_GET['y']) && isset($_GET['z']) && isset($_GET['level']) && isset($_GET['health']) && isset($_GET['pseudo']))
	{
		// if (!isset($_GET['nbrNukeNsoda']) || !isset($_GET['maxNukeNsoda']) || !isset($_GET['ratioSoda']) || !isset($_GET['nbrKbs']) || !isset($_GET['maxKbs']) || !isset($_GET['ratioKbs']))
		// 	return (false);
		if (strlen($_GET['name']) > 50)
			return (false);
		if (strlen($_GET['pseudo']) > 50)
			return (false);
		if (!is_numeric($_GET['id']) || !is_numeric($_GET['x']) || !is_numeric($_GET['y']) || !is_numeric($_GET['z']) || !is_numeric($_GET['level']) || !is_numeric($_GET['health']))
			return (false);
		if (!userExist($_GET['pseudo']))
			return (false);
		if (!buildingExist($_GET['id'], userExist($_GET['pseudo'])))
			return (false);
		return (true);
	}
	else if (isset($_GET['removeBuilding']) && isset($_GET['id']) && isset($_GET['pseudo']))
	{
		if (!userExist($_GET['pseudo']))
			return (false);
		if (!buildingExist($_GET['id'], userExist($_GET['pseudo'])))
			return (false);
		return (true);
	}
	return (false);
}

function userExist($pseudo)
{
	$req = $GLOBALS["bdd"]->prepare('SELECT id FROM user WHERE pseudo = ?');
	$req->execute(array($pseudo));
	$result = $req->fetchAll(PDO::FETCH_ASSOC);
	if (!isset($result[0]) || count($result[0]) == 0)
		return (false);
	else
		return ($result[0]['id']);
}

function buildingExist($id, $pseudo)
{
	$req = $GLOBALS["bdd"]->prepare('SELECT id FROM building WHERE id = ? AND playerId = ?');
	$req->execute(array($id, $pseudo));
	$result = $req->fetchAll(PDO::FETCH_ASSOC);
	if (!isset($result[0]) || count($result[0]) == 0)
		return (false);
	else
		return ($result[0]['id']);
}

function findIdBuildingList($name)
{
	$req = $GLOBALS["bdd"]->prepare('SELECT id FROM buildingList WHERE name = ?');
	$req->execute(array($name));
	$result = $req->fetchAll(PDO::FETCH_ASSOC);
	if (!isset($result[0]) || count($result[0]) == 0)
		return (-1);
	else
		return ($result[0]['id']);
}

function newBuilding($name, $x, $y, $z, $level, $playerId, $health, $isCity, $isProd, $nbrNukeNsoda, $maxNukeNsoda, $ratioSoda, $nbrKbs, $maxKbs, $ratioKbs, $nbrMissile, $maxMissile, $ratioMissile)
{
	$idBuilding = findIdBuildingList($name);
	if ($idBuilding < 0)
		return ;
	$date = date('Y-m-d H:i:s');
	$array = array(
		"name" => $name, "x" => $x, "y" => $y, "z" => $z, "level" => $level,
		"playerId" => $playerId, "health" => $health , "dateCreate" => $date, "isCity" => $isCity, "isProd" => $isProd, "idBuilding" => $idBuilding,
		"nbrNukeNsoda" => $nbrNukeNsoda, "maxNukeNsoda" => $maxNukeNsoda, "ratioSoda" => $ratioSoda,
		"nbrKbs" => $nbrKbs, "maxKbs" => $maxKbs, "ratioKbs" => $ratioKbs,
		"nbrMissile" => $nbrMissile, "maxMissile" => $maxMissile, "ratioMissile" => $ratioMissile
	);
	$req = $GLOBALS["bdd"]->prepare('INSERT INTO building(name, x, y, z, level, playerId, health, dateCreate, isCity, isProd, idBuilding, nbrNukeNsoda, maxNukeNsoda, ratioSoda, nbrKbs, maxKbs, ratioKbs, nbrMissile, maxMissile, ratioMissile, lastUpdate)
			VALUES(:name, :x, :y, :z, :level, :playerId, :health, :dateCreate, :isCity, :isProd, :idBuilding, :nbrNukeNsoda, :maxNukeNsoda, :ratioSoda, :nbrKbs, :maxKbs, :ratioKbs, :nbrMissile, :maxMissile, :ratioMissile, NOW())');
	$req->execute($array);
	print ($GLOBALS["bdd"]->lastInsertId());
}

function updateBuilding()
{
	$arr = array(
		'name' => $_GET['name'],
		'x' => $_GET['x'],
		'y' => $_GET['y'],
		'z' => $_GET['z'],
		'level' => $_GET['level'],
		'health' => $_GET['health'],
		'id' => $_GET['id']
	);
	$arr['nbrNukeNsoda'] = $_GET['nbrNukeNsoda'];
	$arr['maxNukeNsoda'] = $_GET['maxNukeNsoda'];
	$arr['ratioSoda'] = $_GET['ratioSoda'];
	$arr['nbrKbs'] = $_GET['nbrKbs'];
	$arr['maxKbs'] = $_GET['maxKbs'];
	$arr['ratioKbs'] = $_GET['ratioKbs'];
	$arr['nbrMissile'] = $_GET['nbrMissile'];
	$arr['maxMissile'] = $_GET['maxMissile'];
	$arr['ratioMissile'] = $_GET['ratioMissile'];
	$req = $GLOBALS["bdd"]->prepare('UPDATE building SET lastUpdate = NOW(), name = :name, x = :x, y = :y, z = :z, level = :level, health = :health,
			nbrNukeNsoda = :nbrNukeNsoda, maxNukeNsoda = :maxNukeNsoda, ratioSoda = :ratioSoda,
			nbrKbs = :nbrKbs, maxKbs = :maxKbs, ratioKbs = :ratioKbs,
			nbrMissile = :nbrMissile, maxMissile = :maxMissile, ratioMissile = :ratioMissile
			WHERE id = :id');
	$req->execute($arr);
}

function removeBuilding($id)
{
	$req = $GLOBALS["bdd"]->prepare('DELETE FROM building WHERE id = ?');
	$req->execute(array($id));
}

function getBuilding($playerId, $isCity, $isProd)
{
	$req = $GLOBALS["bdd"]->prepare('SELECT * FROM building WHERE playerId = ? AND isCity = ? AND isProd = ?');
	$req->execute(array($playerId, $isCity, $isProd));
	$result = $req->fetchAll(PDO::FETCH_ASSOC);
	for ($i = 0; $i < count($result); $i++)
	{ 
		$result[$i]['dateNow'] = date('m/d/Y h:i:s a', time());
		$result[$i]['differenceInSeconds'] = strtotime($result[$i]['dateNow']) - strtotime($result[$i]['lastUpdate']);
	}
	if (count($result) > 0)
		echo json_encode($result);
}

function getBuildingList()
{
	$req = $GLOBALS["bdd"]->prepare('SELECT * FROM buildingList');
	$req->execute(array());
	$result = $req->fetchAll(PDO::FETCH_ASSOC);
	echo json_encode($result);
}


function getBuildingListInfolevel()
{
	$req = $GLOBALS["bdd"]->prepare('SELECT * FROM `buildingInfoLevel`INNER JOIN `buildingList`ON `buildingInfoLevel`.`idBuilding` = `buildingList`.`id`');
	$req->execute(array());
	$result = $req->fetchAll(PDO::FETCH_ASSOC);
	echo json_encode($result);
}

function main()
{
	if (isset($_GET['addBuilding']))
	{
		if (check())
		{
			newBuilding($_GET['name'], $_GET['x'], $_GET['y'], $_GET['z'], $_GET['level'], userExist($_GET['pseudo']), $_GET['health'], $_GET['isCity'], $_GET['isProd'],
				$_GET['nbrNukeNsoda'], $_GET['maxNukeNsoda'], $_GET['ratioSoda'],
				$_GET['nbrKbs'], $_GET['maxKbs'], $_GET['ratioKbs'],
				$_GET['nbrMissile'], $_GET['maxMissile'], $_GET['ratioMissile']
			);
		}
		else
			print ("banal error");
	}
	else if (isset($_GET['getBuilding']))
	{
		if (check())
			getBuilding(userExist($_GET['pseudo']), $_GET['isCity'], $_GET['isProd']);
	}
	else if (isset($_GET['updateBuilding']))
	{
		if (check())
			updateBuilding();
	}
	else if (isset($_GET['removeBuilding']))
	{
		if (check())
			removeBuilding($_GET['id']);
	}
	else if (isset($_GET['buildingList']))
		getBuildingList();
	else if (isset($_GET['buildingListInfolevel']))
		getBuildingListInfolevel();
	return (0);
}
?>